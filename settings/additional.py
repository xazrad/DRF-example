import datetime
import os

import dj_database_url
from celery.schedules import crontab


# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': dj_database_url.config()
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

DATE_FORMAT = 'Y-m-d'

DATETIME_FORMAT = 'Y-m-d H:i:s'

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'PAGE_SIZE': 25,
    # 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination'
}

JWT_AUTH = {
    'JWT_ALLOW_REFRESH': True,
    'JWT_GET_USER_SECRET_KEY': 'user.utils.user_jwt_secret_key',
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=365),
    'JWT_PAYLOAD_HANDLER': 'rest_framework_jwt.utils.jwt_payload_handler',
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'rest_framework_jwt.utils.jwt_response_payload_handler',
    'JWT_AUTH_HEADER_PREFIX': 'Bearer',
}

AUTH_USER_MODEL = 'user.User'

AUTHENTICATION_BACKENDS = [
    'user.backends.CustomModelBackend',
    'user.backends.CustomRemoteUserBackend',
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[{server_time}] {levelname} {message}',
            'style': '{',
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'django.server': {
            'handlers': ['django.server'],  # only in development use
            'level': 'ERROR',
            'propagate': False,
        },
    }
}


SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {}
}

CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_CREDENTIALS = True

# CELERY
CELERY_BROKER_URL = os.environ.get('BROKER_URL')

CELERY_TIMEZONE = 'Europe/Berlin'

CELERY_RESULT_PERSISTENT = True

CELERY_TASK_IGNORE_RESULT = True

CELERY_BEAT_SCHEDULE = {
    'prepare_money_transfer_file': {
        'task': 'flexvoucher.tasks.base.prepare_money_transfer_file',
        'schedule': crontab(minute=15, hour=0),  # in time in CELERY_TIMEZONE = 'Europe/Berlin'
        'args': ()
    },
    'prepare_generate_statements': {
        'task': 'flexvoucher.tasks.base.prepare_generate_statements',
        'schedule': crontab(minute=15, hour=1, day_of_month=1),  # in time in CELERY_TIMEZONE = 'Europe/Berlin'
        'args': ()
    },
    'generate_DLP_statement': {
        'task': 'flexvoucher.tasks.base.generate_DLP_statement',
        'schedule': crontab(minute=30, hour=1, day_of_month=1),  # in time in CELERY_TIMEZONE = 'Europe/Berlin'
        'args': ()
    },
    'consistency_execute_chain_tasks': {
        'task': 'flexvoucher.tasks.data_consistency.consistency_execute_chain_tasks',
        'schedule': crontab(minute=10, hour=23),  # in time in CELERY_TIMEZONE = 'Europe/Berlin'
        'args': ()
    },
}

JET_CHANGE_FORM_SIBLING_LINKS = False

JET_SIDE_MENU_COMPACT = False
# Add this to your settings.py to customize applications and models list
# use command for generate MENU ITEMS
# python manage.py jet_side_menu_items_example
JET_SIDE_MENU_ITEMS = [
    {
        'app_label': 'auth',
        'permissions': 'fix',  # bug
        'items': [
            {'name': 'group'},
        ]
    },
    {
        'app_label': 'flexvoucher',
        'permissions': 'fix',  # bug
        'items': [
            {'name': 'applicationparameter'},
            {'name': 'consistencytaskresults'},
            {'name': 'consistencytask'},
            {'name': 'dlpfiles'},
            {'name': 'feedback'},
            {'name': 'invoicescan'},
            {'name': 'moneytransfers'},
            {'name': 'moneytransferfile'},
            {'name': 'moneytransfertype'},
            {'name': 'pointsloads'},
            {'name': 'pointsloadstatus'},
            {'name': 'pointsloadtype'},
            {'name': 'transactiontype'},
            {'name': 'transaction'},
            {'name': 'voucherstatus'},
            {'name': 'vouchertype'},
            {'name': 'voucher'},
        ]
    },
    {
        'app_label': 'user',
        'permissions': 'fix',  # bug
        'items': [
            {'name': 'user'},
        ]
    },
    {
        'label': 'Reports',
        'items': [
            {
                'label': 'DLP', 'url': {'type': 'reverse', 'name': 'report:DLP_view'},
                'permissions': ['report.can_read_reports']
            },
            {
                'label': 'Vouchers', 'url': {'type': 'reverse', 'name': 'report:voucher_view'},
                'permissions': ['report.can_read_reports']
            },
            {
                'label': 'Transactions', 'url': {'type': 'reverse', 'name': 'report:transaction_view'},
                'permissions': ['report.can_read_reports']
            },
        ]
    },
    {
        'label': 'Technical tasks',
        'items': [
            {
                'label': 'Clean test users data', 'url': {'type': 'reverse', 'name': 'report:clean_test_data_view'},
                'permissions': 'fix'
            },
            {
                'label': 'Staff users sessions', 'url': {'type': 'reverse', 'name': 'report:user_sessions_view'},
                'permissions': 'fix'
            },
        ]
    },
]

C_DLP = 0.076891

C_TRANS = 0.1

C_RATION = 240

# for money transfer files
DEBITOR_NAME = ''

DEBITOR_IBAN = ''

DEBITOR_AGENT_BIC = ''

DATA_UPLOAD_MAX_MEMORY_SIZE = 52428800

JET_INDEX_DASHBOARD = 'flexvoucher.dashboard.CustomIndexDashboard'

CSRF_COOKIE_NAME = "csrftoken"

FILE_UPLOAD_PERMISSIONS = 0o644
