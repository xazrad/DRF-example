from .additional import *
from .base import *

DEBUG = True

SMTP_HOST = ''

SMTP_PORT = 587

SMTP_LOGIN = ''

SMTP_PASSWORD = ''

PREFERED_SCHEME = 'http'

DOMAIN = '127.0.0.1:8000'

DEFAULT_FROM_EMAIL = ''

AMAZON_ACCESS_KEY = ''

AMAZON_SECRET_KEY = ''

AMAZON_ASSOC_TAG = ''

# for develop only !!
MANAGERS = ('xazrad@gmail.com', )

DBL_API_URL = ''
