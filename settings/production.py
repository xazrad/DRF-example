from .additional import *
from .base import *

SECRET_KEY = ''

STATIC_ROOT = '/static/'

MEDIA_ROOT = '/media_app/'

SMTP_HOST = ''

SMTP_PORT = 25

SMTP_LOGIN = ''

SMTP_PASSWORD = ''

PREFERED_SCHEME = 'https'

DOMAIN = ''

DEFAULT_FROM_EMAIL = ''

AMAZON_ACCESS_KEY = ''

AMAZON_SECRET_KEY = ''

AMAZON_ASSOC_TAG = ''

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
