#!/bin/sh
set -e

while ! nc -z rabbitmq 5672; do sleep 5; done

/venv/bin/python manage.py collectstatic --noinput

sleep 5

/venv/bin/python manage.py migrate --noinput

exec "$@"