import json

import pytest

from django.urls import reverse

from flexvoucher.models import ApplicationParameter


def get_jwt_token(user):
    from rest_framework_jwt.settings import api_settings

    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


@pytest.mark.django_db
class TestCreateJwt:
    url = reverse('token:jwt-auth')

    @pytest.mark.parametrize("username,password,status_code", [
        ('superuser@example.com', 'q123q123Az', 200),
        ('admin@example.com', 'q123q123Az', 200),
        ('user@example.com', 'q123q123Az', 200),
        ('user@example.com', 'q123q123Azzz', 400),
        ('user@example.com', '777', 400),
        ('not_confirmed@example.com', 'q123q123Az', 400),
        ('', '', 400),
    ])
    def test_login_local(self, client, monkeypatch, username, password, status_code):
        def mockreturn(name):
            return 'local'
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        r = client.post(self.url, json.dumps({'username': username, 'password': password}),
                        content_type='application/json')
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()['token']
        if status_code == 400:
            assert isinstance(r.json(), dict)

    @pytest.mark.parametrize('username, password, status_code', [
        ('marek.gmyrek@gmail.com', 'Ds3TKsT', 200),
        ('marek.gmyrek@gmail.com', '123', 400),
    ])
    def test_login_dbl(self, client, monkeypatch, username, status_code, password):
        def mockreturn(name):
            return 'dbl'
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        r = client.post(self.url, json.dumps({'username': username, 'password': password}),
                        content_type='application/json')
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()['token']

    @pytest.mark.parametrize("username,password,status_code", [
        ('user@example.com', 'q123q123Az', 200),
        ('user@gmail.com', '777', 400),
        ('', '', 400),
    ])
    def test_login_1(self, client, username, password, status_code):
        r = client.post(self.url, 'username={}&password={}'.format(username, password),
                        content_type='application/x-www-form-urlencoded')
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()['token']
        if status_code == 400:
            assert isinstance(r.json(), dict)


@pytest.mark.django_db
class TestRefreshJwt:
    url = reverse('token:jwt-refresh')

    @pytest.mark.parametrize("user_id,corret", [
        (1, True),
        (2, False),
    ])
    def test_0(self, client, user_id, corret):
        from user.models import User
        user = User.objects.get(pk=user_id)

        if corret:
            token = get_jwt_token(user)
        else:
            token = ''

        r = client.post(self.url, json.dumps({'token': token}),
                        content_type='application/json')
        assert r.status_code == 200 if corret else 400
        if r.status_code == 400:
            assert isinstance(r.json()['token'], list)

    @pytest.mark.parametrize("user_id,corret", [
        (1, True),
        (2, False),
    ])
    def test_1(self, client, user_id, corret):
        from user.models import User
        user = User.objects.get(pk=user_id)

        if corret:
            token = get_jwt_token(user)
        else:
            token = ''

        r = client.post(self.url, 'token={}'.format(token),
                        content_type='application/x-www-form-urlencoded')
        assert r.status_code == 200 if corret else 400
        if r.status_code == 400:
            assert isinstance(r.json(), dict)


@pytest.mark.django_db
@pytest.mark.parametrize("source_users, status_code", [
                         ('local', 400),
                         ('dbl', 400),
                         ])
def test_flow_accountant_user(client, monkeypatch, source_users, status_code):
    def mockreturn(name):
        return source_users
    monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)
    url = reverse('token:jwt-auth')
    r = client.post(url, json.dumps({'username': 'accountant@example.com', 'password': 'q123q123Az'}),
                    content_type='application/json')
    assert r.status_code == status_code
    msg = 'User account is accountant.'
    assert r.json()['non_field_errors'][0] == msg
