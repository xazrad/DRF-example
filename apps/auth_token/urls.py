from django.urls import path

from .views import ObtainJSONWebToken, RefreshJSONWebToken


app_name = 'auth_token'

urlpatterns = [
    path('jwt/', ObtainJSONWebToken.as_view(), name='jwt-auth'),
    path('jwt-refresh/', RefreshJSONWebToken.as_view(), name='jwt-refresh'),
]
