from rest_framework_jwt.views import ObtainJSONWebToken as OriginObtainJSONWebToken
from rest_framework_jwt.views import RefreshJSONWebToken as OriginRefreshJSONWebToken


from .serializers import JSONWebTokenSerializer


class ObtainJSONWebToken(OriginObtainJSONWebToken):
    serializer_class = JSONWebTokenSerializer


class RefreshJSONWebToken(OriginRefreshJSONWebToken):
    pass
