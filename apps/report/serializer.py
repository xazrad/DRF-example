from decimal import Decimal, ROUND_HALF_UP

from rest_framework import serializers

from django.conf import settings

from flexvoucher import models
from flexvoucher.serializers import AccountSerializer


class TransactionTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TransactionType
        fields = ('description', 'points_or_money', 'credit_or_debit')


class TransactionSerializer(serializers.ModelSerializer):
    account = AccountSerializer()
    transaction_type = TransactionTypeSerializer()

    class Meta:
        model = models.Transaction
        fields = '__all__'


class VoucherSerializer(serializers.ModelSerializer):
    account = AccountSerializer(read_only=True)
    commission = serializers.SerializerMethodField()
    trans = serializers.SerializerMethodField()

    def get_commission(self, obj):
        commission = obj.money_amount * Decimal(str(settings.C_DLP))
        return commission.quantize(Decimal('0.01'), rounding=ROUND_HALF_UP)

    def get_trans(self, obj):
        commission = obj.money_amount * Decimal(str(settings.C_TRANS))
        return commission.quantize(Decimal('0.01'), rounding=ROUND_HALF_UP)

    class Meta:
        model = models.Voucher
        exclude = ('date_out', 'voucher_type', 'voucher_status', 'invoice_price')


class VoucherHistorySerializer(serializers.ModelSerializer):
    voucher = VoucherSerializer()

    class Meta:
        model = models.VoucherHistory
        exclude = ('voucher_status', 'transaction')
