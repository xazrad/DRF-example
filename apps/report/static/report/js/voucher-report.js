'use strict';
(function ($) {
  moment.locale('de');
  const BodyWrapper = (sum, props) => {
    return (
      <tbody {...props}>
        <React.Fragment>
          {props.children}
          <tr className="ant-table-row">
            <td />
            <td />
            <td />
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.points_amount ? numeral(sum.points_amount).format('0,0'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.money_amount ? numeral(sum.money_amount).format('0,0.00'): ''}</td>
            <td />
            <td />
            <td />
            <td />
          </tr>
        </React.Fragment>
      </tbody>
    );
  };
  class Result extends React.Component {
    columns = [
      {
        title: 'No',
        dataIndex: 'id',
        render: (text, record, index) => {
          return index + 1;
        }
      },
      {
        title: 'Date',
        dataIndex: 'date_in',
        render: (text, record, index) => {
          return moment(text).format('YYYY-MM-DD HH:mm:ss');
        }
      },
      {
        title: 'voucher',
        render: (text, record, index) => {
          let {id, voucher_uic} = record;
          let href = `/admin/flexvoucher/voucher/${id}/change/`;
          return <a target="_blank" href={href}>{voucher_uic}</a>
        }
      },
      {
        title: 'points',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let amount =  numeral(record.points_amount).format('0,0');
          return { children: amount,
            props: {},
          };
        }
      },
      {
        title: 'EUR',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let amount =  numeral(record.money_amount).format('0,0.00');
          return { children: amount,
            props: {},
          };
        }
      },
      {
        title: 'status',
        dataIndex: 'voucher_status.description'
      },
      {
        title: 'user',
        dataIndex: 'account.owner.email',
        render: (text, record, index) => {
          let {first_name, last_name, id} = record.account.owner;
          let href = `/admin/user/user/${id}/change/`;
          return <a target="_blank" href={href}>{first_name} {last_name}</a>
        }
      },
      {
        title: 'reference',
        dataIndex: 'voucher_reference'
      },
    ];

    render() {
      return (
        <fieldset className="module aligned wide">
          <antd.Table rowKey="id" dataSource={this.props.data} columns={this.columns} loading={this.props.sync}
                      pagination={{ showSizeChanger: true, pageSizeOptions: ['10', '20', '30', '50', '100'] }}
                      components={{ body: { wrapper: BodyWrapper.bind(this, this.props.sum) } }}
          />
        </fieldset>
      )
    }
  }

  class Main extends React.Component {
    state = {
      user: 'all',
      status: 'all',
      data: null,
      sum: null,
      sync: false,
      'day-start': moment().startOf('month').format('YYYY-MM-DD'),
      'day-end': moment().format('YYYY-MM-DD'),
      userSource: [],
      includeTest: 1
    }

    getReport = (e) => {
      let self = this;
      this.setState({
        sync: true
      });
      axios.get('/v1/report/voucher/', {params: {
          status: this.state.status,
          'day-start': this.state['day-start'],
          'day-end': this.state['day-end'],
           user: this.state.user,
          'include-test': this.state.includeTest
        }})
        .then(function (response) {
          let {data, sum} = response.data;
          self.setState({
            data: data,
            sum: sum,
            sync: false,
          })
        })
        .catch(function (error) {
          self.setState({
            sync: false,
          })
        })
    }

    onChangeStatus = (e) => {
      this.setState({
        status: e
      })
    }

    onChangeIncludeTest = (e) => {
      this.setState({
        includeTest: e.target.checked === true ? 1:0
      })
    }

    onRangeDateChange = (moments, momentsStrings) => {
      this.setState({
        'day-start': momentsStrings[0],
        'day-end': momentsStrings[1],
      });
    }

    handleSearchUser = (value) => {
      if (value.length <3 ) {
          this.setState({
            userSource: [],
            user: 'all'
          });
        return
      }

      let self = this;
      axios.get('/v1/user/', {params: {
          search: value
        }})
        .then(function (response) {
          let userSource = response.data.results.map((obj) => {
              let {first_name, last_name, email} = obj;
              return {value: obj.id, text: `${first_name} ${last_name} ${email}`}
          });
          self.setState({
            userSource: userSource
          });
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    onSelectUser = (value) => {
      this.setState({user: value});
    }

    render() {
      let date0 = moment().startOf('month');
      let date1 = moment();
      return (
        <React.Fragment>
          <antd.Form layout="inline" style={{marginBottom: 24}}>
            <antd.Form.Item>
              <antd.DatePicker.RangePicker  defaultValue={[date0, date1]} style={{ width: 240 }} onChange={this.onRangeDateChange}/>
            </antd.Form.Item>
            <antd.Form.Item>
              <antd.Select defaultValue="all" style={{ width: 120 }} onChange={this.onChangeStatus}>
                <antd.Select.Option value="all">All</antd.Select.Option>
                <antd.Select.Option value="1">Created</antd.Select.Option>
                <antd.Select.Option value="2">Cancelled</antd.Select.Option>
                <antd.Select.Option value="3">Expired</antd.Select.Option>
                <antd.Select.Option value="4">Posted</antd.Select.Option>
                <antd.Select.Option value="5">Redeemed</antd.Select.Option>
                <antd.Select.Option value="6">Annulled</antd.Select.Option>
                <antd.Select.Option value="7">Storno</antd.Select.Option>
                <antd.Select.Option value="8">Deleted</antd.Select.Option>
                <antd.Select.Option value="9">Inpayment</antd.Select.Option>
                <antd.Select.Option value="10">Paid</antd.Select.Option>
              </antd.Select>
            </antd.Form.Item>
            <antd.Form.Item>
              <antd.AutoComplete
                dataSource={this.state.userSource}
                style={{ width: 280 }}
                onSelect={this.onSelectUser}
                onSearch={this.handleSearchUser}
                placeholder="Input min. 3 chars"
              />
            </antd.Form.Item>
            <antd.Form.Item style={{ width: 150 }}>
              <antd.Checkbox checked={this.state.includeTest === 1} onChange={this.onChangeIncludeTest}>Test users.</antd.Checkbox>
            </antd.Form.Item>
             <antd.Form.Item>
               <antd.Button
                 onClick={this.getReport}
                 type="primary"
                 htmlType="submit" >
                 Run
               </antd.Button>
             </antd.Form.Item>
          </antd.Form>
          <Result data={this.state.data} sum={this.state.sum} sync={this.state.sync}/>
        </React.Fragment>
      )
    }
  }

  ReactDOM.render(<Main/>, document.getElementById('content-main'));
})(django.jQuery);