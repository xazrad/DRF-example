'use strict';
(function ($) {
  moment.locale('de');

  class Result extends React.Component {
    columns = [
      {
        title: 'No',
        dataIndex: 'index',
      },
      {
        title: 'User',
        dataIndex: 'username',
        filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters,}) => (
          <div className="custom-filter-dropdown">
            <antd.Input
              ref={ele => this.searchInput = ele}
              placeholder="Search name"
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={this.handleSearch(selectedKeys, confirm)}
            />
            <antd.Button type="primary" onClick={this.handleSearch(selectedKeys, confirm)}>Search</antd.Button>
            <antd.Button onClick={this.handleReset(clearFilters)}>Reset</antd.Button>
          </div>
        ),
        filterIcon: filtered => <antd.Icon type="filter-o" style={{ color: filtered ? '#108ee9' : '#aaa' }} />,
        onFilter: (value, record) => record.username.toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => {
              this.searchInput.focus();
            });
          }
        },
        render: (text, record, index) => {
          let {id, username} = record;
          let href = `/admin/user/user/${id}/change/`;
          const { searchText } = this.state;
          return searchText ? (
              <span>
                  {text.split(new RegExp(`(${searchText})`, 'gi')).map((fragment, i) => (
                      fragment.toLowerCase() === searchText.toLowerCase()
                          ? <span key={i} className="highlight">{fragment}</span> : fragment // eslint-disable-line
                  ))}
                  </span>
          ) : <a target="_blank" href={href}>{username}</a>;
        },
      },
      {
        title: 'current',
        dataIndex: 'current',
        render: (text, record, index) => {
          return record.current ? <antd.Icon type="pushpin-o" style={{ color: '#aaa' }} />: '';
        }
      },
      {
        title: 'expire',
        dataIndex: 'expire_date',
        render: (text, record, index) => {
          return moment(text).format('Y-M-D HH:mm:ss');
        }
      },
    ]

    handleSearch = (selectedKeys, confirm) => () => {
      confirm();
      this.setState({ searchText: selectedKeys[0] });
    }

    handleReset = clearFilters => () => {
      clearFilters();
      this.setState({ searchText: '' });
    }

    constructor(props) {
      super(props);
      this.state = {selectedRowKeys: props.selectedRowKeys, searchText: ''};
    };

    componentDidUpdate(prevProps) {
      if (this.props.selectedRowKeys.length == 0 && this.state.selectedRowKeys.length > 0) {
        this.setState({
          selectedRowKeys: []
        });
      }
    };

    onSelectChange = (selectedRowKeys) => {
      this.setState({ selectedRowKeys: selectedRowKeys });
      this.props.onChangeselectedRowKeys(selectedRowKeys);
    };


    render() {
      const { selectedRowKeys } = this.state;
      const rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectChange,
      };
      return (
        <fieldset className="module aligned wide">
          <antd.Table rowKey="id" dataSource={this.props.data} columns={this.columns} loading={this.props.sync}
                      pagination={{ showSizeChanger: true, pageSizeOptions: ['10', '20', '30', '50', '100'] }}
                      rowSelection={rowSelection}
          />
        </fieldset>
      )
    }
  }

  class Main extends React.Component {
    state = {
      sync: true,
      data: null,
      selectedRowKeys: []
    };

    componentDidMount() {
      let self = this;
      axios.get('/v1/report/users-sessions/?action=users',)
        .then(function (response) {
          self.setState({
            sync: false,
            data: response.data
          })
        })
        .catch(function (error) {
          console.log(error);
        })

    };
    onChangeselectedRowKeys = (selectedRowKeys) => {
      this.setState({
        selectedRowKeys: selectedRowKeys
      })
    };
    cleanData = (e) => {
      let self = this;
      self.setState({
        sync: true,
      });
      axios.post('/v1/report/users-sessions/?action=clean', {
          selectsSessionsId: this.state.selectedRowKeys
        })
        .then(function (response) {
          self.setState({
            sync: false,
            data: response.data,
            selectedRowKeys: []
          })
        })
        .catch(function (error) {
          console.log(error);
        })

    }
    render() {
      return (
        <React.Fragment>
          <antd.Form layout="inline" style={{marginBottom: 20}}>
             <antd.Form.Item>
               <antd.Button
                 onClick={this.cleanData}
                 type="primary"
                 htmlType="submit" >
                 Remove
               </antd.Button>
             </antd.Form.Item>
          </antd.Form>
          <Result data={this.state.data} sync={this.state.sync} onChangeselectedRowKeys={this.onChangeselectedRowKeys}
                  selectedRowKeys={this.state.selectedRowKeys}
          />
        </React.Fragment>
      )
    }
  }

  ReactDOM.render(<Main/>, document.getElementById('content-main'));
})(django.jQuery);