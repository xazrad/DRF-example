'use strict';
(function ($) {
  moment.locale('de');
  const BodyWrapper = (sum, props) => {
    return (
      <tbody {...props}>
        <React.Fragment>
          {props.children}
          <tr className="ant-table-row">
            <td />
            <td />
            <td />
            <td />
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.points_amount ? numeral(sum.points_amount).format('0,0'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.money_amount ? numeral(sum.money_amount).format('0,0.00'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.trans ? numeral(sum.trans).format('0,0.00'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.commission ? numeral(sum.commission).format('0,0.00'): ''}</td>
            <td />
            <td />
          </tr>
        </React.Fragment>
      </tbody>
    );
  };
  class Result extends React.Component {
    columns = [
      {
        title: 'No',
        dataIndex: 'id',
        render: (text, record, index) => {
          return index + 1;
        }
      },
      {
        title: 'date',
        width: 150,
        dataIndex: 'voucher.date_in',
        render: (text, record, index) => {
          return moment(text).format('YYYY-MM-DD HH:mm:ss');
        }
      },
      {
        title: 'paid',
        width: 150,
        dataIndex: 'timestamp',
        render: (text, record, index) => {
          return moment(text).format('YYYY-MM-DD HH:mm:ss');
        }
      },
      {
        title: 'voucher',
        width: 90,
        dataIndex: 'voucher.id',
        render: (text, record, index) => {
          let {id, voucher_uic} = record.voucher;
          let href = `/admin/flexvoucher/voucher/${id}/change/`;
          return <a target="_blank" href={href}>{voucher_uic}</a>
        }
      },
      {
        title: 'points',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let amount =  numeral(record.voucher.points_amount).format('0,0');
          return { children: amount,
            props: {},
          };
        }
      },
      {
        title: 'Euro',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let amount =  numeral(record.voucher.money_amount).format('0,0.00');
          return { children: amount,
            props: {},
          };
        }
      },
      {
        title: 'Trans',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let amount =  numeral(record.voucher.trans).format('0,0.00');
          return { children: amount,
            props: {},
          };
        }
      },
      {
        title: 'DLP',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let amount =  numeral(record.voucher.commission).format('0,0.00');
          return { children: amount,
            props: {},
          };
        }
      },
      {
        title: 'user',
        dataIndex: 'voucher.account.owner.email',
        render: (text, record, index) => {
          let {id, first_name, last_name} = record.voucher.account.owner;
          let href = `/admin/user/user/${id}/change/`;
          return <a target="_blank" href={href}>{`${first_name} ${last_name}`}</a>
        }
      },
      {
        title: 'reference',
        dataIndex: 'voucher.voucher_reference'
      },
    ];

    render() {
      return (
        <fieldset className="module aligned wide">
          <antd.Table rowKey="id" dataSource={this.props.data} columns={this.columns} loading={this.props.sync}
                      pagination={{ showSizeChanger: true, pageSizeOptions: ['10', '20', '30', '50', '100'] }}
                      components={{ body: { wrapper: BodyWrapper.bind(this, this.props.sum) } }}
          />
        </fieldset>
      )
    }
  }

  class Main extends React.Component {
    state = {
      data: null,
      sum: null,
      sync: false,
      'day-start': moment().startOf('month').format('YYYY-MM-DD'),
      'day-end': moment().format('YYYY-MM-DD'),
    };

    getReport = (e) => {
      let self = this;
      this.setState({
        sync: true
      });
      axios.get('/v1/report/dlp/', {params: {
          status: this.state.status,
          'day-start': this.state['day-start'],
          'day-end': this.state['day-end'],
        }})
        .then(function (response) {
          let {data, sum} = response.data;
          self.setState({
            data: data,
            sum: sum,
            sync: false,
          })
        })
        .catch(function (error) {
          self.setState({
            sync: false,
          })
        })
    };

    onRangeDateChange = (moments, momentsStrings) => {
      this.setState({
        'day-start': momentsStrings[0],
        'day-end': momentsStrings[1],
      });
    };

    render() {
      let date0 = moment().startOf('month');
      let date1 = moment();
      return (
        <React.Fragment>
          <antd.Form layout="inline" style={{marginBottom: 24}}>
            <antd.Form.Item>
              <antd.DatePicker.RangePicker  defaultValue={[date0, date1]} style={{ width: 240 }} onChange={this.onRangeDateChange}/>
            </antd.Form.Item>
             <antd.Form.Item>
               <antd.Button
                 onClick={this.getReport}
                 type="primary"
                 htmlType="submit" >
                 Run
               </antd.Button>
             </antd.Form.Item>
          </antd.Form>
          <Result data={this.state.data} sum={this.state.sum} sync={this.state.sync}/>
        </React.Fragment>
      )
    }
  }

  ReactDOM.render(<Main/>, document.getElementById('content-main'));
})(django.jQuery);