'use strict';
(function ($) {
  moment.locale('de');
  const BodyWrapper = (sum, props) => {
    console.log(props);
    console.log(sum);
    return (
      <tbody {...props}>
        <React.Fragment>
          {props.children}
          <tr className="ant-table-row">
            <td />
            <td />
            <td />
            <td />
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.points_credit ? numeral(sum.points_credit).format('0,0'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.points_debit ? numeral(sum.points_debit).format('0,0'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.money_credit ? numeral(sum.money_credit).format('0,0.00'): ''}</td>
            <td style={{textAlign: 'right', fontWeight: 'bold'}}>{sum && sum.money_debit ? numeral(sum.money_debit).format('0,0.00'): ''}</td>
            <td />
            <td />
          </tr>
        </React.Fragment>
      </tbody>
    );
  };

  class Result extends React.Component {
    columns = [
      {
        title: 'TID',
        width: 60,
        render: (text, record, index) => {
          let {id} = record;
          let href = `/admin/flexvoucher/transaction/${id}/change/`;
          return <a target="_blank" href={href}>{id}</a>
        }
      },
      {
        title: 'date',
        dataIndex: 'timestamp',
        render: (text, record, index) => {
          return moment(text).format('YYYY-MM-DD HH:mm:ss');
        }
      },
      {
        title: 'user',
        dataIndex: 'account.owner.email',
        render: (text, record, index) => {
          let {first_name, last_name, id} = record.account.owner;
          let href = `/admin/user/user/${id}/change/`;
          return <a target="_blank" href={href}>{first_name} {last_name}</a>
        }
      },
      {
        title: 'operation',
        dataIndex: 'transaction_type.description'
      },
      {
        title: 'Cr points',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let {credit_or_debit, points_or_money} = record.transaction_type;
          if (credit_or_debit === 'credit' && points_or_money === 'points') {
            let amount = record.amount > 0 ? numeral(record.amount).format('0,0'): '';
            return { children: amount,
                      props: {},
            };
          }
        }
      },
      {
        title: 'Dt points',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let {credit_or_debit, points_or_money} = record.transaction_type;
          if (credit_or_debit === 'debit' && points_or_money === 'points') {
            let amount = record.amount > 0 ? numeral(record.amount).format('0,0'): '';
            return { children: amount,
                      props: {},
            };
          }
        }
      },
      {
        title: 'Cr Euro',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let {credit_or_debit, points_or_money} = record.transaction_type;
          if (credit_or_debit === 'credit' && points_or_money === 'money') {
            let amount = record.amount > 0 ? numeral(record.amount).format('0,0.00'): '';
            return { children: amount,
                      props: {},
            };
          }
        }
      },
      {
        title: 'Dt Euro',
        width: 90,
        className: 'column-money',
        render: (text, record, index) => {
          let {credit_or_debit, points_or_money} = record.transaction_type;
          if (credit_or_debit === 'debit' && points_or_money === 'money') {
            let amount = record.amount > 0 ? numeral(record.amount).format('0,0.00'): '';
            return { children: amount,
                      props: {},
            };
          }
        }
      },
      {
        title: 'points',
        width: 90,
        className: 'column-money',
        dataIndex: 'points_balance',
        render: (text, record, index) => {
          return numeral(record.points_balance).format('0,0');
        }
      },
      {
        title: 'Euro',
        align: 'right',
        width: 90,
        className: 'column-money',
        dataIndex: 'money_balance',
        render: (text, record, index) => {
          return numeral(record.money_balance).format('0,0.00');
        }
      }
    ];

    render() {
      return (
        <fieldset className="module aligned wide">
          <antd.Table rowKey="id" dataSource={this.props.data} columns={this.columns} loading={this.props.sync}
            pagination={{ showSizeChanger: true, pageSizeOptions: ['10', '20', '30', '50', '100'] }}
            components={{ body: { wrapper: BodyWrapper.bind(this, this.props.sum) } }}
          />
        </fieldset>
      )
    }
  }

  class Main extends React.Component {
    state = {
      user: 'all',
      type: 'all',
      data: null,
      sum: null,
      sync: false,
      'day-start': moment().startOf('month').format('YYYY-MM-DD'),
      'day-end': moment().format('YYYY-MM-DD'),
      userSource: []
    }

    getReport = (e) => {
      let self = this;
      this.setState({
        sync: true
      });
      axios.get('/v1/report/transaction/', {params: {
          type: this.state.type,
          'day-start': this.state['day-start'],
          'day-end': this.state['day-end'],
          user: this.state.user
        }})
        .then(function (response) {
          let {data, sum} = response.data;
          self.setState({
            data: data,
            sum: sum,
            sync: false,
          })
        })
        .catch(function (error) {
          self.setState({
            sync: false,
          })
        })
    }

    onChangeType = (e) => {
      this.setState({
        type: e
      })
    }

    onRangeDateChange = (moments, momentsStrings) => {
      this.setState({
        'day-start': momentsStrings[0],
        'day-end': momentsStrings[1],
      });
    }

    handleSearchUser = (value) => {
      if (value.length <3 ) {
          this.setState({
            userSource: [],
            user: 'all'
          });
        return
      }

      let self = this;
      axios.get('/v1/user/', {params: {
          search: value
        }})
        .then(function (response) {
          let userSource = response.data.results.map((obj) => {
              let {first_name, last_name, email} = obj;
              return {value: obj.id, text: `${first_name} ${last_name} ${email}`}
          });
          self.setState({
            userSource: userSource
          });
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    onSelectUser = (value) => {
      this.setState({user: value});
    }

    render() {
      let date0 = moment().startOf('month');
      let date1 = moment();
      return (
        <React.Fragment>
          <antd.Form layout="inline" style={{marginBottom: 24}}>
            <antd.Form.Item>
              <antd.DatePicker.RangePicker  defaultValue={[date0, date1]} style={{ width: 240 }} onChange={this.onRangeDateChange}/>
            </antd.Form.Item>
            <antd.Form.Item>
              <antd.Select defaultValue="all" style={{ width: 80 }} onChange={this.onChangeType}>
                <antd.Select.Option value="all">All</antd.Select.Option>
                <antd.Select.Option value="points">Points</antd.Select.Option>
                <antd.Select.Option value="money">Money</antd.Select.Option>
              </antd.Select>
            </antd.Form.Item>
            <antd.Form.Item>
              <antd.AutoComplete
                dataSource={this.state.userSource}
                style={{ width: 280 }}
                onSelect={this.onSelectUser}
                onSearch={this.handleSearchUser}
                placeholder="Input min. 3 chars"
              />
            </antd.Form.Item>
             <antd.Form.Item>
               <antd.Button
                 onClick={this.getReport}
                 type="primary"
                 htmlType="submit" >
                 Run
               </antd.Button>
             </antd.Form.Item>
          </antd.Form>
          <Result data={this.state.data} sum={this.state.sum} sync={this.state.sync}/>
        </React.Fragment>
      )
    }
  }

  ReactDOM.render(<Main/>, document.getElementById('content-main'));
})(django.jQuery);