import json

import pytest

from django.test import TestCase
from django.test.client import Client
from django.urls import reverse


class BaseTestCase(TestCase):
    def setUp(self):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')


class TestTransactionReportView(BaseTestCase):
    url = reverse('report:transaction_view')

    def test_user_transaction(self):
        r = self.c.get(self.url)
        assert r.status_code == 200


@pytest.mark.django_db
class TestTransactionReport:
    url = reverse('report:transaction')

    @pytest.mark.parametrize("user_id, query", [
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01'),
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01&type=points'),
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01&type=money'),
        (1, '?&day-start=2001-01-01&day-end=2019-01-01'),
        (1, ''),
        (3, '?user=1&day-start=2001-01-01&day-end=2019-01-01'),
        (1, '?user=1&day-end=2019-01-01'),
        (1, '?user=1&day-start=01-01-01&day-end=2019-01-01'),
    ])
    def test_user_transaction(self, user_id, query):
        url = self.url + query
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')
        r = self.c.get(url, content_type='application/json')
        assert r.status_code == 200


class TestVoucherReportView(BaseTestCase):
    url = reverse('report:voucher_view')

    def test_user_transaction(self):
        r = self.c.get(self.url)
        assert r.status_code == 200


@pytest.mark.django_db
class TestVoucherReport:
    url = reverse('report:voucher')

    @pytest.mark.parametrize("user_id, query", [
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01&include-test=0'),
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01&include-test=1'),
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01'),
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01&status=2'),
        (1, '?user=1&day-start=2001-01-01&day-end=2019-01-01&status=1'),
        (1, '?&day-start=2001-01-01&day-end=2019-01-01'),
        (1, ''),
        (3, '?user=1&day-start=2001-01-01&day-end=2019-01-01'),
        (1, '?user=1&day-end=2019-01-01'),
        (1, '?user=1&day-start=01-01-01&day-end=2019-01-01'),
    ])
    def test_user_transaction(self, user_id, query):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

        url = self.url + query
        r = self.c.get(url, content_type='application/json')
        assert r.status_code == 200


class TestTestUserCleanView(BaseTestCase):
    url = reverse('report:clean_test_data_view')

    def test_user_transaction(self):
        r = self.c.get(self.url)
        assert r.status_code == 200


@pytest.mark.django_db
class TestTestUserClean:
    url = reverse('report:clean_test_data')

    @pytest.mark.parametrize("user_id, query", [
        (1, '?action=users'),
    ])
    def test_users(self, user_id, query):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

        url = self.url + query
        r = self.c.get(url, content_type='application/json')
        assert r.status_code == 200

    @pytest.mark.parametrize("user_id, query", [
        (1, '?action=clean'),
    ])
    def test_clean(self, user_id, query):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

        url = self.url + query
        data = {'selectsUsersId': [900, 999]}
        r = self.c.post(url, json.dumps(data), content_type='application/json')
        assert r.status_code == 200


class TestUserSessionsView(BaseTestCase):
    url = reverse('report:user_sessions_view')

    def test_user_transaction(self):
        r = self.c.get(self.url)
        assert r.status_code == 200


@pytest.mark.django_db
class TestUserSessions:
    url = reverse('report:user_sessions')

    @pytest.mark.parametrize("user_id, query", [
        (1, '?action=users'),
    ])
    def test_users(self, user_id, query):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

        url = self.url + query
        r = self.c.get(url, content_type='application/json')
        assert r.status_code == 200

    @pytest.mark.parametrize("user_id, query", [
        (1, '?action=clean'),
    ])
    def test_clean(self, user_id, query):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

        url = self.url + query
        data = {'selectsSessionsId': [900, 999]}
        r = self.c.post(url, json.dumps(data), content_type='application/json')
        assert r.status_code == 200


class TestDLPAdminView(BaseTestCase):
    url = reverse('report:DLP_view')

    def test_user_transaction(self):
        r = self.c.get(self.url)
        assert r.status_code == 200


@pytest.mark.django_db
class TestDLP:
    url = reverse('report:DLP')

    @pytest.mark.parametrize("user_id, query", [
        (1, '?day-start=2001-01-01&day-end=2019-01-01'),
        (1, ''),
    ])
    def test_0(self, user_id, query):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

        url = self.url + query
        r = self.c.get(url, content_type='application/json')
        assert r.status_code == 200
