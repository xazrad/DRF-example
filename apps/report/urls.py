from django.urls import path

from . import views

app_name = 'report'

urlpatterns = [
    path('v1/report/transaction/', views.TransactionReportView.as_view(), name='transaction'),
    path('admin/report/transaction/', views.TransactionReportAdminView.as_view(), name='transaction_view'),

    path('v1/report/voucher/', views.VoucherReportView.as_view(), name='voucher'),
    path('admin/report/voucher/', views.VoucherReportAdminView.as_view(), name='voucher_view'),

    path('v1/report/clean-test-data/', views.CleanTestUserView.as_view(), name='clean_test_data'),
    path('admin/report/clean-test-data/', views.CleanTestUserAdminView.as_view(), name='clean_test_data_view'),

    path('v1/report/users-sessions/', views.UserSessionView.as_view(), name='user_sessions'),
    path('admin/report/users-sessions/', views.UserSessionsAdminView.as_view(), name='user_sessions_view'),

    path('v1/report/dlp/', views.DLPView.as_view(), name='DLP'),
    path('admin/report/dlp/', views.DLPAdminView.as_view(), name='DLP_view'),
]
