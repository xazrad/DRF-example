from decimal import Decimal

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import user_passes_test
from django.contrib.sessions.models import Session
from django.db.models import Count
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.utils.timezone import now

from flexvoucher.models import InvoiceScan, PointsLoads, VoucherHistory, Voucher, Transaction
from flexvoucher.serializers import VoucherSerializer
from .authentications import CsrfExemptSessionAuthentication
from .filters import DateFilter, VoucherFilter, TransactionFilter
from .serializer import VoucherHistorySerializer, TransactionSerializer


class BaseReportAdminView(TemplateView):
    site_url = "/"

    @method_decorator(user_passes_test(
        lambda u: u.is_staff,
        login_url='admin:login'
    ))
    def dispatch(self, request, *args, **kwargs):
        return super(BaseReportAdminView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BaseReportAdminView, self).get_context_data(**kwargs)
        context['title'] = 'Transaction Report'
        context['user'] = self.request.user
        context['site_url'] = self.site_url
        context['has_permission'] = True
        return context


class BaseReportView(generics.ListAPIView):
    authentication_classes = (SessionAuthentication, )
    permission_classes = (IsAuthenticated, )  # TODO: check permissions


class TransactionReportView(BaseReportView):
    """
    Transaction for any given user activity on any given period
    """
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    filter_backends = (TransactionFilter, )

    def get_queryset(self):
        query = super(TransactionReportView, self).get_queryset()
        user = self.request.user
        if not user.is_superuser and not user.is_admin and not user.is_accountant:
            query = query.filter(account__owner_id=self.request.user.pk)
        return query

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        points_debit_sum = sum([Decimal(_['amount']) for _ in data
                                if _['transaction_type']['points_or_money'] == 'points' and
                                _['transaction_type']['credit_or_debit'] == 'debit'])
        points_credit_sum = sum([Decimal(_['amount']) for _ in data
                                if _['transaction_type']['points_or_money'] == 'points' and
                                 _['transaction_type']['credit_or_debit'] == 'credit'])
        money_debit_sum = sum([Decimal(_['amount']) for _ in data
                               if _['transaction_type']['points_or_money'] == 'money' and
                               _['transaction_type']['credit_or_debit'] == 'debit'])
        money_credit_sum = sum([Decimal(_['amount']) for _ in data
                                if _['transaction_type']['points_or_money'] == 'money' and
                                _['transaction_type']['credit_or_debit'] == 'credit'])
        resp = {
            'data': data,
            'sum': {
                'points_debit': points_debit_sum,
                'points_credit': points_credit_sum,
                'money_debit': money_debit_sum,
                'money_credit': money_credit_sum,
            }
        }
        return Response(resp)


class TransactionReportAdminView(TemplateView):
    template_name = "report/transaction_report.html"


class VoucherReportAdminView(TemplateView):
    template_name = "report/voucher_report.html"


class VoucherReportView(BaseReportView):
    """
    Vouchers for any given user activity on any given period
    """
    serializer_class = VoucherSerializer
    queryset = Voucher.objects.all()
    filter_backends = (VoucherFilter, )

    def get_queryset(self):
        query = super(VoucherReportView, self).get_queryset()
        user = self.request.user
        if not user.is_superuser and not user.is_admin and not user.is_accountant:
            query = query.filter(account__owner_id=self.request.user.pk)
        return query

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        points_amount_sum = sum([_['points_amount'] for _ in data])
        money_amount_sum = sum([Decimal(_['money_amount']) for _ in data])
        resp = {
            'data': data,
            'sum': {
                'points_amount': points_amount_sum,
                'money_amount': money_amount_sum,
            }
        }
        return Response(resp)


class CleanTestUserView(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated, )  # TODO: check permissions

    def _get_users(self):
        query = get_user_model().objects.filter(account__is_test=True).annotate(Count('account__voucher'),
                                                                                Count('account__pointsloads'))
        data = list()
        for index, obj in enumerate(query, 1):
            row = dict(id=obj.pk,
                       index=index,
                       role='test',
                       username=obj.username,
                       vouchers=obj.account__voucher__count,
                       pointsloads=obj.account__pointsloads__count)
            data.append(row)
        return data

    def _clean_data(self):
        users_id = self.request.data['selectsUsersId']
        [_.delete() for _ in InvoiceScan.objects.filter(invoice_data__voucher__account__owner_id__in=users_id)]
        Voucher.objects.filter(account__owner_id__in=users_id).delete()
        PointsLoads.objects.filter(account__owner_id__in=users_id).delete()

    def get(self, request, *args, **kwargs):
        # request.query_params.get('action')
        data = self._get_users()
        return Response(data)

    def post(self, request, *args, **kwargs):
        # request.query_params.get('action')
        self._clean_data()
        data = self._get_users()
        return Response(data)


class CleanTestUserAdminView(TemplateView):
    template_name = "report/clean_test_user_data.html"


class UserSessionView(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated, )  # TODO: check permissions

    def _get_sessions(self):
        all_sessions = Session.objects.filter(expire_date__gte=now())
        session_key = self.request.session.session_key
        data = list()
        for index, session in enumerate(all_sessions, 1):
            session_data = session.get_decoded()
            row = dict(id=session.pk,
                       index=index,
                       expire_date=session.expire_date,
                       user_pk=session_data.get('_auth_user_id'),
                       current=session.pk == session_key)
            data.append(row)
        user_query = get_user_model().objects.filter(pk__in=set([_['user_pk'] for _ in data])).values('pk', 'username')
        lookup_dict = {str(_['pk']): _['username'] for _ in user_query}
        list(map(lambda x: x.__setitem__('username', lookup_dict.get(x['user_pk'])), data))
        return data

    def _clean_sessions(self):
        sessions_id = self.request.data['selectsSessionsId']
        Session.objects.filter(pk__in=sessions_id).delete()

    def get(self, request, *args, **kwargs):
        # request.query_params.get('action')
        data = self._get_sessions()
        return Response(data)

    def post(self, request, *args, **kwargs):
        # request.query_params.get('action')
        self._clean_sessions()
        data = self._get_sessions()
        return Response(data)


class UserSessionsAdminView(TemplateView):
    template_name = "report/user_sessions.html"


class DLPAdminView(TemplateView):
    template_name = "report/dlp.html"


class DLPView(BaseReportView):
    """ Dienstleistungpauschale Report
    """
    serializer_class = VoucherHistorySerializer
    queryset = VoucherHistory.objects.filter(voucher_status_id=10)  # paid
    filter_backends = (DateFilter, )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        points_amount_sum = sum([_['voucher']['points_amount'] for _ in data])
        money_amount_sum = sum([Decimal(_['voucher']['money_amount']) for _ in data])
        commission_sum = sum([_['voucher']['commission'] for _ in data])
        trans_sum = sum([_['voucher']['trans'] for _ in data])
        resp = {
            'data': data,
            'sum': {
                'points_amount': points_amount_sum,
                'money_amount': money_amount_sum,
                'commission': commission_sum,
                'trans': trans_sum,
            }
        }
        return Response(resp)
