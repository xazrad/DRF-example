from datetime import datetime, time

from rest_framework.compat import coreapi, coreschema
from rest_framework.filters import BaseFilterBackend

from django.utils.dateparse import parse_date
from django.utils.timezone import make_aware


SCHEMA_FIELDS = [
    coreapi.Field(
        name='user',
        required=False,
        location='query',
        schema=coreschema.String(
            default='all',
            description='user id. Default: all'
        )
    ),
    coreapi.Field(
        name='day-start',
        required=False,
        location='query',
        schema=coreschema.String(
            default='today',
            description='Example 2018-12-01. Default: today',
        )
    ),
    coreapi.Field(
        name='day-end',
        required=False,
        location='query',
        schema=coreschema.String(
            default='today',
            description='Example 2018-12-01. Default: today',
        )
    ),
]


class TransactionFilter(BaseFilterBackend):
    search_title = 'Filter.'

    def get_schema_fields(self, view):
        assert coreapi is not None, 'coreapi must be installed to use `get_schema_fields()`'
        assert coreschema is not None, 'coreschema must be installed to use `get_schema_fields()`'
        return SCHEMA_FIELDS + [
            coreapi.Field(
                name='type',
                required=False,
                location='query',
                schema=coreschema.String(
                    default='all',
                    description='Choice: all, points, money. Default: all',
                )
            )
        ]

    def get_search_terms(self, request):
        user_id = request.query_params.get('user', 'all')
        day_start = request.query_params.get('day-start', datetime.today().strftime('%Y-%m-%d'))
        day_end = request.query_params.get('day-end', datetime.today().strftime('%Y-%m-%d'))
        type = request.query_params.get('type', 'all')

        try:
            user_id = 'all' if user_id == 'all' else int(user_id)
            day_start = make_aware(datetime.combine(parse_date(day_start), time.min))
            day_end = make_aware(datetime.combine(parse_date(day_end), time.max))
        except Exception:
            return
        return user_id, day_start, day_end, type

    def filter_queryset(self, request, queryset, view):
        try:
            user_id, day_start, day_end, type = self.get_search_terms(request)
        except Exception:
            return queryset
        queryset = queryset.filter(timestamp__gte=day_start, timestamp__lte=day_end)
        if user_id != 'all':
            queryset = queryset.filter(account__owner_id=user_id)
        if type == 'points':
            queryset = queryset.filter(transaction_type__points_or_money='points')
        if type == 'money':
            queryset = queryset.filter(transaction_type__points_or_money='money')
        return queryset


class VoucherFilter(BaseFilterBackend):
    search_title = 'Filter.'

    def get_schema_fields(self, view):
        assert coreapi is not None, 'coreapi must be installed to use `get_schema_fields()`'
        assert coreschema is not None, 'coreschema must be installed to use `get_schema_fields()`'
        return SCHEMA_FIELDS + [
            coreapi.Field(
                name='status',
                required=False,
                location='query',
                schema=coreschema.String(
                    default='all',
                    description='Choice: all, 1-Created, 2-Cancelled, 3-Expired, 4-Posted, 5-Posted, 6-Annulled. Default: all',
                )
            ),
            coreapi.Field(
                name='include-test',
                required=False,
                location='query',
                schema=coreschema.String(
                    default='all',
                    description='Choice: 1, 0. Default: 1',
                )
            )
        ]

    def get_search_terms(self, request):
        user_id = request.query_params.get('user', 'all')
        day_start = request.query_params.get('day-start', datetime.today().strftime('%Y-%m-%d'))
        day_end = request.query_params.get('day-end', datetime.today().strftime('%Y-%m-%d'))
        status = request.query_params.get('status', 'all')
        include_test = request.query_params.get('include-test', '1')

        try:
            user_id = 'all' if user_id == 'all' else int(user_id)
            day_start = make_aware(datetime.combine(parse_date(day_start), time.min))
            day_end = make_aware(datetime.combine(parse_date(day_end), time.max))
        except Exception:
            return
        return user_id, day_start, day_end, status, include_test

    def filter_queryset(self, request, queryset, view):
        try:
            user_id, day_start, day_end, status, include_test = self.get_search_terms(request)
        except Exception:
            return queryset
        queryset = queryset.filter(date_in__gte=day_start, date_in__lte=day_end)
        if user_id != 'all':
            queryset = queryset.filter(account__owner_id=user_id)
        if status != 'all':
            queryset = queryset.filter(voucher_status=status)
        if include_test != '1':
            queryset = queryset.exclude(account__is_test=True)
        return queryset


class DateFilter(BaseFilterBackend):
    search_title = 'Filter.'

    def get_schema_fields(self, view):
        assert coreapi is not None, 'coreapi must be installed to use `get_schema_fields()`'
        assert coreschema is not None, 'coreschema must be installed to use `get_schema_fields()`'
        return [
            coreapi.Field(
                name='day-start',
                required=False,
                location='query',
                schema=coreschema.String(
                    default='today',
                    description='Example 2018-12-01. Default: today',
                )
            ),
            coreapi.Field(
                name='day-end',
                required=False,
                location='query',
                schema=coreschema.String(
                    default='today',
                    description='Example 2018-12-01. Default: today',
                )
            )
        ]

    def get_search_terms(self, request):
        day_start = request.query_params.get('day-start', datetime.today().strftime('%Y-%m-%d'))
        day_end = request.query_params.get('day-end', datetime.today().strftime('%Y-%m-%d'))

        try:
            day_start = make_aware(datetime.combine(parse_date(day_start), time.min))
            day_end = make_aware(datetime.combine(parse_date(day_end), time.max))
        except Exception:
            return
        return day_start, day_end

    def filter_queryset(self, request, queryset, view):
        try:
            day_start, day_end = self.get_search_terms(request)
        except Exception:
            return queryset
        queryset = queryset.filter(timestamp__gte=day_start, timestamp__lte=day_end)
        return queryset
