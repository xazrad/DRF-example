from rest_framework import permissions

from flexvoucher.models import ApplicationParameter


class UserAdminAccessPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if getattr(view, 'action', 'list') != 'list':
            return True
        if request.user.is_superuser:
            return True
        return request.user.is_admin

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser or request.user.is_admin:
            return True
        return request.user.pk == obj.pk


class UserSourceParamPermission(permissions.BasePermission):

    def _check_param(self):
        return ApplicationParameter.get_param('source_users') == 'local'

    def has_permission(self, request, view):
        return self._check_param()

    def has_object_permission(self, request, view, obj):
        return self._check_param()
