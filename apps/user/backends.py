from schwifty import IBAN

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from flexvoucher.models import ApplicationParameter
from user.dbl_api import dbl_client


UserModel = get_user_model()


class CheckParamMixin:
    def can_authenticate(self):
        return ApplicationParameter.get_param('source_users') == self.source_users


class CustomModelBackend(ModelBackend, CheckParamMixin):
    source_users = 'local'

    def authenticate(self, request, username=None, password=None, **kwargs):
        # check global setting
        if username:
            username = username.lower()
        user = super().authenticate(request, username, password, **kwargs)
        if user is None:
            return
        if user.account.is_test:
            return user
        if not self.can_authenticate():
            return
        return user


class CustomRemoteUserBackend(ModelBackend, CheckParamMixin):
    """
    Authenticates with DBL.
    """
    source_users = 'dbl'

    def authenticate(self, request, username=None, password=None, **kwargs):
        # check global setting
        if not self.can_authenticate():
            return
        if username is None or password is None:
            return
        user_data = dbl_client.auth(username, password)
        if user_data is None:
            return
        defaults = {
            'username': user_data['login'],
            'first_name': user_data['first_name'],
            'last_name': user_data['last_name'],
            'email': user_data['login'],
            'is_active': user_data['is_active'],
            'is_confirmed': True,
            'is_staff': False,
            'is_superuser': False
        }
        user, created = UserModel._default_manager.get_or_create(
            source=self.source_users,
            source_id=user_data['id'],
            defaults=defaults
        )
        if not created:
            user = self.configure_user(user, **defaults)
        if user_data.get('iban'):
            try:
                iban = IBAN(user_data['iban'])
            except ValueError:
                pass
            else:
                user.account.iban = iban.compact
        if user_data.get('tax_id'):
            user.account.tax_id = user_data['tax_id']
        if user_data.get('vat_id'):
            user.account.vat_id = user_data['vat_id']
        user.groups.clear()
        user.set_unusable_password()
        user.save()
        user.account.save(dbl_update=False)
        return user if self.user_can_authenticate(user) else None

    def configure_user(self, user, **kwargs):
        for k, v in kwargs.items():
            setattr(user, k, v)
        return user
