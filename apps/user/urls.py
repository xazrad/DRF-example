from rest_framework.routers import DefaultRouter

from django.urls import path, re_path

from user import views


app_name = 'user'


router = DefaultRouter()
router.register(r'', views.UserViewSet, base_name='user')


urlpatterns = [
    path('upload/', views.UserUploadView.as_view(), name='user_upload'),
    path('deactivate/', views.UserListDeactivate.as_view(), name='user_list_deactivate'),
    re_path(r'invite/(?P<token>[a-z,A-Z,0-9]{2,}-[a-z,A-Z,0-9]{2,}-[a-f,0-9]{2,})/$',
            views.UserInviteView.as_view(), name='signup_invite'),  # accept invite `User`
    path('password-reset/', views.UserPasswordResetRequestView.as_view(), name='password_reset'),  # request reset password
    re_path(r'password-reset/(?P<token>[a-z,A-Z,0-9]{2,}-[a-z,A-Z,0-9]{2,}-[a-f,0-9]{2,})/$',
            views.UserPasswordResetConfirmView.as_view(), name='password_reset_confirm'),  # change password confirm
]

urlpatterns += router.urls
