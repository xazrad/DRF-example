import csv
from decimal import Decimal
from io import StringIO

import pyvat
from rest_framework import serializers
from rest_framework.exceptions import NotFound
from schwifty import IBAN

from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError as DjangoValidationError
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from flexvoucher.models import ApplicationParameter, Feedback
from .dbl_api import dbl_client
from .utils import account_activation_token, check_tax_id, send_email


USER_MODEL = get_user_model()


class UserSerializerModel(serializers.ModelSerializer):
    iban = serializers.CharField(max_length=34, required=False, source='account.iban')
    tax_id = serializers.CharField(max_length=14, required=False, source='account.tax_id')
    vat_id = serializers.CharField(max_length=14, required=False, source='account.vat_id')
    bic = serializers.SerializerMethodField()
    points_balance = serializers.SerializerMethodField(read_only=True)
    money_balance = serializers.DecimalField(max_digits=8, decimal_places=2, read_only=True, source='account.money_balance')
    has_feedback = serializers.SerializerMethodField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(UserSerializerModel, self).__init__(*args, **kwargs)
        if self.context['view'].action == 'list':
            self.fields.pop('iban')
            self.fields.pop('bic')
            self.fields.pop('points_balance')
            self.fields.pop('money_balance')
            self.fields.pop('tax_id')
            self.fields.pop('vat_id')
            self.fields.pop('has_feedback')

    def get_points_balance(self, obj):
        if obj.account.is_test or ApplicationParameter.get_param('source_users') == 'local':
            return obj.account.points_balance
        balance = dbl_client.balance(obj.username)['balance']
        return Decimal(balance)

    def get_has_feedback(self, obj):
        return Feedback.objects.filter(account__owner_id=obj.pk).exists()

    def validate_iban(self, value):
        try:
            iban = IBAN(value)
        except ValueError:
            raise serializers.ValidationError('Ungültiges Format.')
        return iban.compact

    def validate_vat_id(self, value):
        if not value.lower().startswith('de'):
            raise serializers.ValidationError('Ungültiges Format.')
        if not pyvat.is_vat_number_format_valid(value):
            raise serializers.ValidationError('Ungültiges Format.')
        return value

    def validate_tax_id(self, value):
        if not check_tax_id(value):
            raise serializers.ValidationError('Ungültiges Format.')
        return value

    def get_bic(self, obj):
        if obj.account.iban is None:
            return
        try:
            bic = IBAN(obj.account.iban).bic
            data = dict(
                bank_name=bic.bank_name,
                bank_code=bic.bank_code,
                country_code=bic.country_code,
                country_bank_code=bic.country_bank_code,
            )
        except Exception:
            return
        return data

    def validate_email(self, value):
        """
        Check that email is not used
        """
        is_exists = USER_MODEL.objects.filter(username=value)
        if self.instance:
            is_exists = is_exists.exclude(pk=self.instance.pk)

        if is_exists.exists():
            raise serializers.ValidationError("Email is used")
        return value

    def create(self, validated_data):
        """
        Create a new `User` instance, given the validated data.
        User has unusable password for prevent login
        """
        user = USER_MODEL.objects.create_user(
            validated_data['email'],
            email=validated_data['email'],
            is_active=False,
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        return user

    def update(self, instance, validated_data):
        if validated_data.get('account'):
            instance.account.iban = validated_data['account'].get('iban')
            instance.account.tax_id = validated_data['account'].get('tax_id')
            instance.account.vat_id = validated_data['account'].get('vat_id')
            instance.account.save()
        instance.save()
        return instance

    class Meta:
        model = USER_MODEL
        fields = ('id', 'first_name', 'last_name', 'email', 'is_active', 'is_confirmed', 'iban',
                  'bic', 'points_balance', 'money_balance', 'tax_id', 'vat_id', 'has_feedback')


class UserInviteSerializer(serializers.Serializer):
    """
    Validate invite link  and control password
    """
    email = serializers.SerializerMethodField(read_only=True)
    password = serializers.CharField(max_length=128, write_only=True)

    token_generator = account_activation_token

    def _get_user(self, value):
        try:
            user_pk = urlsafe_base64_decode(value.split('-')[0]).decode()
        except Exception:
            return False

        try:
            _user = USER_MODEL.objects.get(pk=user_pk)
        except Exception:
            return False

        token = '-'.join(value.split('-')[1:3])
        is_token_valid = self.token_generator.check_token(_user, token)
        if not is_token_valid:
            return False
        return _user

    def get_email(self, token):
        if self.context.get('user'):
            return self.context['user'].username
        user = self._get_user(token)
        if not user:
            raise NotFound()
        return user.username

    def validate_password(self, value):
        try:
            password_validation.validate_password(value, None)
        except DjangoValidationError as e:
            raise serializers.ValidationError(e.messages)
        return value

    def validate(self, attrs):
        user = self._get_user(self.instance)
        if not user:
            raise NotFound()
        self.context['user'] = user
        return attrs

    def update(self, instance, validated_data):
        user = self.context['user']
        user.set_password(validated_data['password'])
        user.save()
        return instance


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=128)

    def validate_password(self, value):
        try:
            password_validation.validate_password(value, None)
        except DjangoValidationError as e:
            raise serializers.ValidationError(e.messages)
        return value


class UserPasswordResetRequestSerializer(serializers.Serializer):
    """
    Serializer for reset password user
    """

    email = serializers.EmailField(max_length=150)

    def get_users(self, email):
        active_users = USER_MODEL._default_manager.filter(**{
            '%s__iexact' % USER_MODEL.get_email_field_name(): email,
            'is_active': True,
        })
        return (u for u in active_users if u.has_usable_password())

    def create(self, validated_data):
        email = self.validated_data["email"]
        for user in self.get_users(email):
            ctx = dict()
            uid = urlsafe_base64_encode(str(user.id).encode()).decode()
            token = default_token_generator.make_token(user)
            ctx['token'] = '{}-{}'.format(uid, token)
            send_email(user.email, ctx=ctx,
                       subject_template_name='user/subject_reset_subject.txt',
                       template_name='user/email_password_reset.html')
        return validated_data


class UserPasswordResetConfirmSerializer(UserInviteSerializer):
    """
    Change password confirm
    """
    token_generator = default_token_generator


class UserDeactivateListSerializer(serializers.ListSerializer):

    def create(self, validated_data):
        is_superuser = self.context['request'].user.is_superuser
        pk_list = [_['id'] for _ in validated_data]
        query = USER_MODEL.objects.filter(pk__in=pk_list)
        if not is_superuser:
            query = query.exclude(is_superuser=True)
        query.update(is_active=False)
        return validated_data


class UserDeactivateSerializer(serializers.Serializer):
    id = serializers.IntegerField()

    class Meta:
        list_serializer_class = UserDeactivateListSerializer


class UserUploadCSVRowSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=150)
    first_name = serializers.CharField(max_length=30)
    last_name = serializers.CharField(max_length=150)
    status = serializers.CharField(read_only=True)


class CSVFileSerializer(serializers.ListField, serializers.FileField):

    def to_internal_value(self, data):
        if not data:
            raise serializers.ValidationError("Invalid CSV file")
        _data = serializers.FileField.to_internal_value(self, data[0])
        try:
            _data = _data.read().decode()
            r = csv.DictReader(StringIO(_data), quoting=csv.QUOTE_NONE)
            r.fieldnames
        except Exception as exc:
            raise serializers.ValidationError("Invalid CSV file: %s" % exc)
        _data = serializers.ListField.to_internal_value(self=self, data=r)
        return _data

    def to_representation(self, value):
        return serializers.ListField.to_representation(self, value)


class UserUploadCSVSerializer(serializers.Serializer):
    """
    Upload User from CSV file
    """
    file = CSVFileSerializer(child=UserUploadCSVRowSerializer())

    def create(self, validated_data):
        for row in validated_data['file']:
            is_exists = USER_MODEL.objects.filter(username=row['email']).exists()
            if is_exists:
                row['status'] = 'exists'
                continue
            USER_MODEL.objects.create_user(row['email'],
                                           email=row['email'],
                                           is_active=False,
                                           first_name=row['first_name'],
                                           last_name=row['last_name'])

            row['status'] = 'writed'
        return validated_data
