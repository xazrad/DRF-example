from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    email = models.EmailField(_('email address'))
    is_confirmed = models.BooleanField(default=False)
    source = models.CharField(max_length=12, default='local')
    source_id = models.CharField(max_length=60, null=True, default=True, db_index=True)

    @property
    def is_admin(self):
        return self.groups.filter(name='admins').exists()

    @property
    def is_accountant(self):
        return self.groups.filter(name='accountants').exists()

    class Meta:
        ordering = ('email', )
