from django.apps import AppConfig
from django.db.models.signals import pre_save

from .signals import pre_save_user


class UserConfig(AppConfig):
    name = 'user'
    verbose_name = 'User'

    def ready(self):
        from .models import User
        pre_save.connect(pre_save_user, sender=User)
