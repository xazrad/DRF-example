import re
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.crypto import constant_time_compare
from django.utils.http import base36_to_int
from django.template import loader


def send_email(to, ctx, subject_template_name, template_name):
    ctx['protocol'] = settings.PREFERED_SCHEME
    ctx['domain'] = settings.DOMAIN
    ctx['site_name'] = settings.DOMAIN

    html = loader.render_to_string(template_name, ctx)
    subject = loader.render_to_string(subject_template_name, ctx).strip()

    msg_to = to
    if type(to) in [list, tuple]:
        msg_to = ', '.join(to)

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = settings.DEFAULT_FROM_EMAIL
    msg['To'] = msg_to
    msg.attach(MIMEText(html, 'html'))

    s = smtplib.SMTP(settings.SMTP_HOST, settings.SMTP_PORT)
    if settings.SMTP_LOGIN and settings.SMTP_PASSWORD:
        s.login(settings.SMTP_LOGIN, settings.SMTP_PASSWORD)
    s.sendmail(msg['From'], to, msg.as_string())
    s.quit()


def user_jwt_secret_key(user):
    key = '{}{}'.format(user.password, settings.SECRET_KEY)
    return key


def check_tax_id(value):
    regex_list = [
        '^\d{5}/\d{5}$',
        '^\d{3}/\d{3}/\d{5}$',
        '^\d{2}/\d{3}/\d{5}$',
        '^\d{2} \d{3} \d{5}$',
        '^\d{3} \d{3} \d{5}$',
        '^\d{3}/\d{4}/\d{4}$',
    ]
    for regex in regex_list:
        if re.match(regex, value) is not None:
            return True
    return False


class TokenGenerator(PasswordResetTokenGenerator):

    def _make_hash_value(self, user, timestamp):
        login_timestamp = '' if user.last_login is None else user.last_login.replace(microsecond=0, tzinfo=None)
        return str(user.pk) + str(timestamp) + user.password + str(login_timestamp) + 'invite'

    def check_token(self, user, token):
        """
        Check that a password reset token is correct for a given user.
        """
        # Parse the token
        ts_b36, hash = token.split("-")
        try:
            ts = base36_to_int(ts_b36)
        except ValueError:
            return False

        # Check that the timestamp/uid has not been tampered with
        if not constant_time_compare(self._make_token_with_timestamp(user, ts), token):
            return False
        return True


account_activation_token = TokenGenerator()
