import pyvat
from schwifty import IBAN

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _

from flexvoucher.models import Account, Transaction

from . import models
from .utils import check_tax_id


admin.site.unregister(Group)


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = '__all__'

    @property
    def is_tech_user(self):
        if self.cleaned_data['owner'].is_staff or self.cleaned_data['owner'].account.is_test:
            return True
        return False

    def clean_iban(self):
        value = self.cleaned_data['iban']
        if self.is_tech_user:
            return value
        if value is None:
            raise forms.ValidationError('Value is required.')
        try:
            IBAN(value)
        except ValueError:
            raise forms.ValidationError('Invalid value.')
        return value

    def clean_vat_id(self):
        value = self.cleaned_data['vat_id']
        if self.is_tech_user:
            return value
        if value is None:
            raise forms.ValidationError('Value is required.')
        if not value.lower().startswith('de'):
            raise forms.ValidationError('Invalid format.')
        if not pyvat.is_vat_number_format_valid(value):
            raise forms.ValidationError('Invalid format.')
        return value

    def clean_tax_id(self):
        value = self.cleaned_data['tax_id']
        if self.is_tech_user:
            return value
        if value is None:
            raise forms.ValidationError('Value is required.')
        if not check_tax_id(value):
            raise forms.ValidationError('Invalid format.')
        return value


class CustomUserChangeForm(UserChangeForm):

    def clean_is_staff(self):
        value = self.cleaned_data['is_staff']
        # nothing to change
        if value == self.instance.is_staff:
            return value
        if value and not self.instance.is_staff:
            is_has_transactions = Transaction.objects.filter(account=self.instance.account).exists()
            if is_has_transactions:
                raise forms.ValidationError('User has transaction.')
        return value


class AccountInline(admin.StackedInline):
    form = AccountForm
    model = Account
    readonly_fields = ('points_balance', 'money_balance', 'created_at', 'updated_at', 'is_test')


@admin.register(models.User)
class AuthUserAdmin(UserAdmin):
    inlines = (AccountInline, )
    filter_horizontal = ('groups', 'user_permissions',)
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'is_confirmed')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'is_confirmed', 'groups')
    form = CustomUserChangeForm
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_confirmed', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ('permissions',)
    fields = ('name', 'permissions', )
    readonly_fields = ('name', )

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permissions':
            qs = kwargs.get('queryset', db_field.remote_field.model.objects)
            # Avoid a major performance hit resolving permission names which
            # triggers a content_type load:
            kwargs['queryset'] = qs.select_related('content_type')
        return super().formfield_for_manytomany(db_field, request=request, **kwargs)
