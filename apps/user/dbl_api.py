import json
import logging
from urllib import parse as urlparse

import requests

from django.conf import settings


GET = 'GET'
POST = 'POST'


class ApiException(Exception):
    pass


class BaseApi:
    """
    Class for access DBL API
    """
    def __init__(self):
        self.end_point = settings.DBL_API_URL
        self.time_out = 5
        self._log = logging.getLogger('django')

    def _perform_request(self, url, type=GET, params=None):
        if params is None:
            params = {}

        url = urlparse.urljoin(self.end_point, url)
        # lookup table to find out the appropriate requests method,
        # headers and payload type (json or query parameters)
        identity = lambda x: x
        json_dumps = lambda x: json.dumps(x)

        lookup = {
            GET: (requests.get, {}, 'params', identity),
            POST: (requests.post, {'Content-type': 'application/json'}, 'data',
                   json_dumps),
        }

        requests_method, headers, payload, transform = lookup[type]
        kwargs = {'headers': headers, payload: transform(params), 'timeout': self.time_out}

        self._log.info('DBL Request: %s %s %s:%s %s',
                       type, url, payload, params, headers)

        req = requests_method(url, **kwargs)
        self._log.info('DBL Response: %s %s %s', url, req.status_code, req.text)
        return req

    def _get_data(self, url, type=GET, params=None):
        if params is None:
            params = dict()
        req = self._perform_request(url, type, params)
        if req.status_code == 204 or req.status_code == 401:
            return req.status_code, None
        if not req.ok:
            raise Exception('DBL API Exception: %s' % req.status_code)
        if not req.text:
            return req.status_code, None
        data = req.json()
        return req.status_code, data

    def auth(self, username, password):
        """
        :param username:
        :param password:
        :return: None or dictionary
        """
        status_code, data = self._get_data('auth', POST, {'login': username.lower(), 'password': password})
        if status_code == 401:
            return
        return data

    def balance(self, username):
        """
        :param username:
        :return:
        """
        status_code, data = self._get_data('balance', POST, {"login": username})
        if status_code == 401:
            raise ApiException('DBL API Exception: %s' % status_code)
        return data

    def request_point(self, username, points_amount):
        """
        :return: reference_id
        """
        status_code, data = self._get_data('requestpoints', POST, {"login": username,
                                                                   'points_amount': points_amount})
        if status_code == 401:
            return
        return data['reference_id']

    def return_points(self, reference_id, username, points_amount):
        """
        :param reference_id:
        :return:
        """
        status_code, _ = self._get_data('returnpoints', POST, {"login": username,
                                                               'points_amount': points_amount,
                                                               'reference_id': reference_id})
        if status_code == 401:
            raise ApiException('DBL API Exception: %s' % status_code)

    def profile_update(self, username, tax_id, vat, iban):
        status_code, _ = self._get_data('profileupdate', POST, {"login": username,
                                                                "tax_id": tax_id,
                                                                "vat": vat,
                                                                "iban": iban})
        if status_code == 401:
            raise ApiException('DBL API Exception: %s' % status_code)


dbl_client = BaseApi()
