import pytest

from user.dbl_api import dbl_client


@pytest.mark.django_db
class TestApiDBL:

    @pytest.mark.parametrize("username, password, data", [
        ('admin@admin.admin', 'admin', None),
        ('admin@admin', 'admin', None),
        ('', '', None),
        ('marek.gmyrek@gmail.com', 'Ds3TKsT', {'id': '28'}),
    ])
    def test_0_auth(self, username, password, data):
        result = dbl_client.auth(username, password)
        if data is None:
            assert result == data
        else:
            assert all([result[_] for _ in data.keys()])

    @pytest.mark.parametrize("email, status_code", [
        ("marek.gmyrek@gmail.com", 200),
        ("marek.gmyrek@gmail", 400),
        ("", 400),
        ("example@mail.com", 400),
    ])
    def test_1_balance(self, email, status_code):
        from user.dbl_api import ApiException
        if status_code == 200:
            result = dbl_client.balance(email)
            assert result['balance']
        else:
            with pytest.raises(ApiException):
                dbl_client.balance(email)

    @pytest.mark.parametrize("username, amount, data", [
        ("marek.gmyrek@mail.com", 'blahbla', False),
        ("marek.gmyrek@mail.com", 100, False),
        ("marek.gmyrek@gmail.com", "100", True),
        ("marek.gmyrek@gmail.com", 100, True),
    ])
    def test_2_requestpoints(self, request, username, amount, data):
        result = dbl_client.request_point(username, amount)
        if data:
            assert result
            request.config.cache.set("voucher/voucher_reference", result)

    @pytest.mark.parametrize("email, amount, voucher_reference, error", [
        ("marek.gmyrek@gmail.com", 100, "from_cache", False),
        ("marek.gmyrek@gmail.com", "100", "from_cache", True),
        ("marek.gmyrek@gmail.com", 200, "from_cache", True),
        ("marek.gmyrek@mail.com", 100, "from_cache", True),
        ("marek.gmyrek@gmail.com", 100, False, True),
    ])
    def test_return_3_points(self, request, email, amount, voucher_reference, error):
        if voucher_reference == 'from_cache':
            voucher_reference = request.config.cache.get("voucher/voucher_reference", None)
        else:
            voucher_reference = '000-000-000-000'
        if error:
            from user.dbl_api import ApiException
            with pytest.raises(ApiException):
                dbl_client.return_points(voucher_reference, email, amount)
        else:
            dbl_client.return_points(voucher_reference, email, amount)
        assert True

    @pytest.mark.parametrize("usermame, tax_id, vat, iban, error", [
        # partial
        ("marek.gmyrek@gmail.com", None, "DE216930296", "DE89 3704 0044 0532 0130 00", False),
        ("marek.gmyrek@gmail.com", "93815/08152", None, "DE89 3704 0044 0532 0130 00", False),
        ("marek.gmyrek@gmail.com", "93815/08152", "DE216930296", None, False),
        ("marek.gmyrek@gmail.com", None, None, "DE89 3704 0044 0532 0130 00", False),
        ("marek.gmyrek@gmail.com", None, "DE216930296", None, False),
        ("marek.gmyrek@gmail.com", "93815/08152", None, None, False),
        # full
        ("marek.gmyrek@gmail.com", "DE123", "454545", "233e324", True),
        ("marek.gmyrek@mail.com", "DE123", "454545", "233e324", True),
        ("marek.gmyrek@gmail.com", "93815/08152", "DE216930296", "DE89 3704 0044 0532 0130 00", False),
        ("marek.gmyrek@gmail.com", "123/123/12345", "DE216930296", "DE89370400440532013000", False),
        ("marek.gmyrek@gmail.com", "12/123/12345", "DE216930296", "DE89370400440532013000", False),
        ("marek.gmyrek@gmail.com", "75 812 08152", "DE216930296", "DE89370400440532013000", False),  # TODO: fix to False
        ("marek.gmyrek@gmail.com", "075 812 08152", "DE216930296", "DE89370400440532013000", False),
        ("marek.gmyrek@gmail.com", "123/1234/1234", "DE216930296", "DE89370400440532013000", False),
    ])
    def test_profile_update(self, usermame, tax_id, vat, iban, error):
        if error:
            from user.dbl_api import ApiException
            with pytest.raises(ApiException):
                dbl_client.profile_update(username=usermame, tax_id=tax_id, vat=vat, iban=iban)
        else:
            dbl_client.profile_update(username=usermame, tax_id=tax_id, vat=vat, iban=iban)
