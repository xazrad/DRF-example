import json

import pytest

from django.urls import reverse

from flexvoucher.models import ApplicationParameter
from user.models import User


def get_jwt_token(user):
    from rest_framework_jwt.settings import api_settings

    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


@pytest.mark.django_db
class TestUserList:
    url = reverse('user:user-list')

    @pytest.mark.parametrize("token, status_code", [
        ('kjfhjkrfhjrfhjkrhfkhr', 401),
        ('', 401),
    ])
    def test_get_401(self, client, token, status_code):
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        r = client.get(self.url, content_type='application/json', **auth_headers)
        assert r.status_code == status_code
        assert r.json()['detail']

    @pytest.mark.parametrize("user_id, status_code, source_users", [
        (1, 200, 'local'),
        (2, 200, 'local'),
        (3, 403, 'local'),
        (1, 200, 'dbl'),
        (2, 200, 'dbl'),
        (3, 403, 'dbl'),
    ])
    def test_get_200(self, client, monkeypatch, user_id, status_code, source_users):
        from user.models import User

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        user = User.objects.get(pk=user_id)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        r = client.get(self.url, content_type='application/json', **auth_headers)
        assert r.status_code == status_code
        if status_code == 200:
            assert len(r.json()) > 0
        if status_code == 403:
            assert r.json()['detail']


@pytest.mark.django_db
class TestUserDetail:

    @pytest.mark.parametrize("user_id, url_user, status_code, source_users", [
        (1, 1, 200, 'local'),
        (3, 1, 403, 'local'),
        (1, 3, 200, 'local'),
        (2, 3, 200, 'local'),
        (3, 3, 200, 'local'),
        (3, 10, 403, 'dbl'),
        (1, 10, 200, 'dbl'),
        (2, 10, 200, 'dbl'),
        (3, 10, 403, 'dbl'),
    ])
    def test_get_0(self, client, monkeypatch, user_id, url_user, status_code, source_users):
        from user.models import User

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        user = User.objects.get(pk=user_id)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('user:user-detail', kwargs={'pk': url_user})
        r = client.get(url.format(url_user), content_type='application/json', **auth_headers)
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()['id'] == url_user
        else:
            assert r.json()['detail']


@pytest.mark.django_db
class TestUserDelete:

    def test_0(self, client):
        from user.models import User
        user = User.objects.get(pk=1)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('user:user-detail', kwargs={'pk': 1})
        r = client.delete(url, content_type='application/json', **auth_headers)
        assert r.status_code == 405
        assert r.json()['detail']


@pytest.mark.django_db
class TestUserCreate:
    url = reverse('user:user-list')

    @pytest.mark.parametrize("status_code,data, source_users", [
        (400, {'email': 'user@example.com', 'first_name': 'Radik', 'last_name': 'Khaziev', 'group': 'user', 'is_confirmed': True}, 'local'),
        (201, {'email': 'xazrad@example.com', 'first_name': 'Radik', 'last_name': 'Khaziev', 'group': 'user', 'is_confirmed': True}, 'local'),
        (400, {'first_name': 'Radik', 'last_name': 'Khaziev', 'group': 'user', 'is_confirmed': True}, 'local'),
        (401, {'email': 'user@example.com', 'first_name': 'Radik', 'last_name': 'Khaziev', 'group': 'user', 'is_confirmed': True}, 'dbl'),
    ])
    def test_signup(self, client, monkeypatch, status_code, data, source_users):
        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        r = client.post(self.url, json.dumps(data), content_type='application/json')
        assert r.status_code == status_code
        assert isinstance(r.json(), dict)


@pytest.mark.django_db
class TestUserPasswordSet:
    @pytest.mark.parametrize("user_id, url_user, status_code, password, source_users", [
        ('1', '2', 200, 'q123q123QW', 'local'),
        ('1', '2', 400, '123456', 'local'),
        ('3', '1', 403, 'q123q123QW', 'local'),
        ('1', '2', 403, 'q123q123QW', 'dbl'),
    ])
    def test_0(self, client, monkeypatch, user_id, url_user, status_code, password, source_users):

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        user = User.objects.get(pk=user_id)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('user:user-set_password', kwargs={'pk': url_user})
        r = client.post(url, json.dumps({'password': password}),
                        content_type='application/json', **auth_headers)
        assert r.status_code == status_code
        assert isinstance(r.json(), dict)


@pytest.mark.django_db
class TestUserFeedbackSet:

    @pytest.mark.parametrize("user_id, status_code, data", [
        (1, 400, {'comment': '', 'rating': 1, 'phone': '+1458989'}),
        (1, 201, {'comment': """Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
        sunt in culpa qui officia deserunt mollit anim id est laborum.""",
                  'phone': '+14957777777',
                  'rating': 1})
    ])
    def test_set_feedback(self, client, user_id, status_code, data):
        user = User.objects.get(pk=user_id)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('user:user-feedback', kwargs={'pk': user_id})
        r = client.post(url, json.dumps(data), content_type='application/json', **auth_headers)
        assert r.status_code == status_code


@pytest.mark.django_db
class TestUserUpdate:
    @pytest.mark.parametrize("user_id, url_user, status_code, check, data, source_users", [
        ('1', '2', 400, True, {'email': 'admin@example.com', 'first_name': 'Sara',
                               'last_name': 'John', 'group': 'user', 'iban': 'DE12 1203 0000 8445 0026 00',
                               'vat_id': 'DE216930296', 'tax_id': '123/123/12345'}, 'local'),
        ('1', '2', 200, True, {'email': 'admin@example.com', 'first_name': 'Sara',
                               'last_name': 'John', 'group': 'user', 'iban': 'DE89 3704 0044 0532 0130 00',
                               'vat_id': 'DE216930296', 'tax_id': '123/123/12345'}, 'local'),
        ('1', 2, 200, True, {'email': 'admin@example.com', 'first_name': 'Sara',
                             'last_name': 'John', 'iban': 'DE89 3704 0044 0532 0130 00',
                             'vat_id': 'DE216930296', 'tax_id': '123/123/12345'}, 'dbl'),
        ('1', '2', 400, True, {'email': 'admin@example.com', 'first_name': 'Sara',
                               'last_name': 'John', 'group': 'user', 'iban': 'DE89 3704 0044 0532 0130 01'}, 'local'),
        ('1', '2', 200, True, {'email': 'admin@example.com', 'first_name': 'Sara',
                               'last_name': 'John', 'group': 'user', 'is_active': False,
                               'is_confirmed': False}, 'local'),
        ('1', '2', 200, True, {'email': 'admin@example.com', 'first_name': 'Radik',
                               'last_name': 'Khaziev', 'group': 'admins'}, 'local'),
        ('1', '2', 200, True, {'email': 'admin@example.com', 'first_name': 'Radik',
                               'last_name': 'Khaziev', 'group': 'super_user'}, 'local'),
        ('3', '1', 403, True, {'email': 'admin@example.com', 'first_name': 'Radik',
                               'last_name': 'Khaziev', 'group': 'user', 'is_active': False,
                               'is_confirmed': False}, 'local'),
        ('3', '3', 200, False, {'email': 'user@example.com', 'first_name': 'Radik',
                                'last_name': 'Khaziev', 'group': 'user', 'is_active': False,
                                'is_confirmed': False}, 'local'),
        (1, 7, 200, True, {'email': 'not_confirmed2@example.com', 'group': 'user',
                           'is_confirmed': True}, 'local'),
    ])
    def test_0(self, client, monkeypatch, user_id, url_user, status_code, check, data, source_users):
        # test for super_user
        from user.models import User
        from user.dbl_api import dbl_client

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        def mock_profile_update(*args, **kwargs):
            return
        monkeypatch.setattr(dbl_client, 'profile_update', mock_profile_update)
        user = User.objects.get(pk=user_id)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('user:user-detail', kwargs={'pk': url_user})
        r = client.patch(url, json.dumps(data),
                         content_type='application/json', **auth_headers)
        assert r.status_code == status_code
        assert isinstance(r.json(), dict)


@pytest.mark.django_db
class TestUserInviteLink:

    def _get_token(self):
        from django.utils.http import urlsafe_base64_encode
        from user.models import User
        from user.utils import account_activation_token

        instance = User.objects.get(pk=1)

        uid = urlsafe_base64_encode(str(instance.id).encode()).decode()
        token = account_activation_token.make_token(instance)
        return '{}-{}'.format(uid, token)

    @pytest.mark.parametrize("token, is_valid, source_users, status_code", [
        (True, True, 'local', 200),
        (False, False, 'local', 404),
        (True, True, 'dbl', 403),
    ])
    def test_get_0(self, client, monkeypatch, token, is_valid, source_users, status_code):
        _token = 'MQ-50b-0030b67230a79308d2d1'
        if token:
            _token = self._get_token()

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        url = reverse('user:signup_invite', kwargs={'token': _token})
        r = client.get(url)
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()['email']
        else:
            assert isinstance(r.json(), dict)

    @pytest.mark.parametrize("password, status_code, token, source_users", [
        ('qwerty', 400, True, 'local'),
        ('q123q123Az', 200, True, 'local'),
        ('q123q123Az', 404, False, 'local'),
        ('q123q123Az', 403, True, 'dbl'),
    ])
    def test_post_0(self, client, monkeypatch, password, status_code, token, source_users):
        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        _token = 'MQ-50b-0030b67230a79308d2d1'
        if token:
            _token = self._get_token()
        url = reverse('user:signup_invite', kwargs={'token': _token})
        r = client.put(url, 'password={}'.format(password),
                       content_type='application/x-www-form-urlencoded')
        assert r.status_code == status_code
        if r.status_code == 200:
            assert r.json()
        else:
            assert isinstance(r.json(), dict)


@pytest.mark.django_db
class TestUserPasswordResetRequest:

    @pytest.mark.parametrize("source_users, status_code", [
        ('local', 201),
        ('dbl', 403)
    ])
    def test_post_0(self, client, monkeypatch, source_users, status_code):
        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        r = client.post(reverse('user:password_reset'), '{"email": "user@example.com"}', content_type='application/json')
        assert r.status_code == status_code
        assert r.json()


@pytest.mark.django_db
class TestUserPasswordResetConfirm:

    def _get_token(self):
        from django.utils.http import urlsafe_base64_encode
        from user.models import User
        from django.contrib.auth.tokens import default_token_generator

        instance = User.objects.get(pk=1)

        uid = urlsafe_base64_encode(str(instance.id).encode()).decode()
        token = default_token_generator.make_token(instance)
        return '{}-{}'.format(uid, token)

    @pytest.mark.parametrize("source_users, status_code", [
        ('local', 200),
        ('dbl', 403),
    ])
    def test_get_0(self, client, monkeypatch, source_users, status_code):
        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        token = self._get_token()
        url = reverse('user:password_reset_confirm', kwargs={'token': token})
        r = client.get(url, content_type='application/json')
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()

    @pytest.mark.parametrize("password,status_code, source_users", [
        ('qwerty', 400, 'local'),
        ('q123q123Az', 200, 'local'),
        ('q123q123Az', 403, 'dbl'),
    ])
    def test_post_0(self, client, monkeypatch, password, status_code, source_users):
        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        token = self._get_token()
        url = reverse('user:password_reset_confirm', kwargs={'token': token})
        r = client.put(url, 'password={}'.format(password),
                       content_type='application/x-www-form-urlencoded')
        assert r.status_code == status_code
        if status_code == 200:
            assert r.json()

    def test_post_1(self, client):
        token = self._get_token()
        url = reverse('user:password_reset_confirm', kwargs={'token': token})
        r = client.put(url, '{"password": "q123q123Az"}',
                       content_type='application/json')
        assert r.status_code == 200
        assert r.json()


@pytest.mark.django_db
class TestUserDeactivateList:

    @pytest.mark.parametrize("user_id, status_code, source_users", [
        (1, 201, 'local'),
        (2, 201, 'local'),
        (3, 403, 'local'),
        (1, 403, 'dbl'),
    ])
    def test_get_0(self, client, monkeypatch, user_id, status_code, source_users):
        from user.models import User

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        user = User.objects.get(pk=user_id)
        token = get_jwt_token(user)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        r = client.post(reverse('user:user_list_deactivate'), json.dumps([{'id': 1}, {'id': 2}]),
                        content_type='application/json', **auth_headers)
        assert r.status_code == status_code


@pytest.mark.django_db
class TestUserUpload:
    @pytest.mark.parametrize("user_id,status_code, source_users", [
        (None, 401, 'local'),
        (3, 403, 'local'),
        (1, 201, 'local'),
        (1, 403, 'dbl'),
    ])
    def test_post(self, client, monkeypatch, example_csv, user_id, status_code, source_users):
        auth_headers = {}

        def mockreturn(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mockreturn)

        if user_id:
            from user.models import User
            user = User.objects.get(pk=user_id)
            token = get_jwt_token(user)
            auth_headers = {
                'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
            }
        r = client.post(reverse('user:user_upload'), {'file': example_csv}, **auth_headers)
        assert r.status_code == status_code
        if status_code == 201:
            assert r.json()['file'][0]['status'] == 'writed'
            assert r.json()['file'][1]['status'] == 'writed'

    @pytest.mark.parametrize("user_id,status_code", [
        (None, 401),
        (3, 403),
        (1, 201),
    ])
    def test_post_2(self, client, example_csv_incorrect, user_id, status_code):
        auth_headers = {}
        if user_id:
            from user.models import User
            user = User.objects.get(pk=user_id)
            token = get_jwt_token(user)
            auth_headers = {
                'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
            }
        r = client.post(reverse('user:user_upload'), {'file': example_csv_incorrect}, **auth_headers)
        assert r.status_code == status_code
        if status_code == 201:
            assert r.json()['file'] == []
