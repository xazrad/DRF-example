from django.utils.http import urlsafe_base64_encode

from .utils import account_activation_token, send_email


def pre_save_user(sender, instance, **kwargs):
    """
    Signal connected User pre save signal
    for alert invite user
    """
    from flexvoucher.models import ApplicationParameter
    from user.models import User

    if kwargs.get('raw'):
        # from fixtures
        return

    if ApplicationParameter.get_param('source_users') == 'dbl':
        return
    is_need_alert = False
    if instance.id is None and instance.is_confirmed:
        # new instance User
        instance.is_active = True
        is_need_alert = True

    if instance.id and instance.is_confirmed:
        old_value = User.objects.filter(id=instance.id).values_list('is_confirmed', flat=True).first()
        if not old_value:
            # check is_confirmed set first
            instance.is_active = True
            is_need_alert = True

    if instance.id and not instance.is_confirmed:
        old_value = User.objects.filter(id=instance.id).values_list('is_confirmed', flat=True).first()
        if old_value:
            # not allow unconfirmed
            instance.is_confirmed = True

    if is_need_alert:
        # send invite link
        ctx = dict()
        uid = urlsafe_base64_encode(str(instance.id).encode()).decode()
        token = account_activation_token.make_token(instance)
        ctx['token'] = '{}-{}'.format(uid, token)
        send_email(instance.email, ctx, subject_template_name='user/invite_user_subject.txt', template_name='user/email_signup.html')
