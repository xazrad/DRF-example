from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import SessionAuthentication
from rest_framework.filters import SearchFilter
from rest_framework import generics
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from django.contrib.auth import get_user_model

from flexvoucher.serializers import FeedbackSerializer
from .permissions import UserAdminAccessPermission, UserSourceParamPermission
from .serializers import (
    PasswordSerializer, UserDeactivateSerializer, UserInviteSerializer, UserPasswordResetRequestSerializer, UserPasswordResetConfirmSerializer,
    UserSerializerModel, UserUploadCSVSerializer
)


USER_MODEL = get_user_model()


class UserPasswordResetRequestView(generics.CreateAPIView):
    """
    Send email with reset password link

    `Authentication: None`

    `Permissions: None`
    """
    serializer_class = UserPasswordResetRequestSerializer
    permission_classes = (UserSourceParamPermission, )


class UserPasswordResetConfirmView(generics.RetrieveUpdateAPIView):
    """
    get:

    Checks reset password link-hash

    `Authentication: None`

    `Permissions: None`

    put:

    Set password for User with reset-password link-hash

    `Authentication: None`

    `Permissions: None`
    """
    serializer_class = UserPasswordResetConfirmSerializer
    permission_classes = (UserSourceParamPermission, )

    def get_object(self):
        token = self.kwargs['token']
        return token


class UserInviteView(generics.RetrieveUpdateAPIView):
    """
    get:

    Checks invite link-hash

    `Authentication: None`

    `Permissions: None`

    put:

    Set password for User with invite link

    `Authentication: None`

    `Permissions: None`
    """
    serializer_class = UserInviteSerializer
    permission_classes = (UserSourceParamPermission,)

    def get_object(self):
        token = self.kwargs['token']
        return token


class UserListDeactivate(generics.CreateAPIView):
    """
    Deactivate list of Users

    `Authentication: JWT`

    `Permissions`

    * super_user

    * admins (only for admins & user)
    """
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated, UserSourceParamPermission, UserAdminAccessPermission,)
    serializer_class = UserDeactivateSerializer

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs, many=True)


class UserViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    list:

    Returns User's list

    `Authentication: JWT`

    `Permissions`

    * super_user

    * admins

    retrieve:

    Returns User's detail

    `Authentication: JWT`

    `Permissions`

    * super_user

    * admins

    * user (for self)

    create:

    Create User. SignUp method

    `Authentication: None`

    `Permissions: None`

    The created user has a group user, is_active & is_confirmed are false always.
    All is optional params

    update:

    `Authentication: JWT`

    `Permissions`

    * super_user

    * admins

    * user (for self, only address)
    """
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, UserSourceParamPermission, UserAdminAccessPermission, )
    queryset = USER_MODEL.objects.all()
    serializer_class = UserSerializerModel
    pagination_class = PageNumberPagination
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('is_active', 'is_confirmed',)
    search_fields = ('username', 'first_name', 'last_name')

    def get_permissions(self):
        if getattr(self, 'action', None) and self.action == 'create':
            return [UserSourceParamPermission(), ]
        if getattr(self, 'action', None) and self.action in ['update', 'partial_update', 'retrieve', 'list']:
            return [IsAuthenticated(), UserAdminAccessPermission()]
        return super(UserViewSet, self).get_permissions()

    @swagger_auto_schema(
        operation_description="Set user password.",
        request_body=PasswordSerializer,
    )
    @action(methods=['post'], url_path='password', detail=True, url_name='set_password')
    def set_password(self, request, pk):
        user = self.get_object()
        serializer = PasswordSerializer(data=request.data)
        if serializer.is_valid():
            user.set_password(serializer.data['password'])
            user.save()
            return Response({'status': 'ok'})
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        operation_description="Send feedback.",
        request_body=FeedbackSerializer,
    )
    @action(methods=['post'], url_path='feedback', detail=True, url_name='feedback')
    def set_feedback(self, request, pk):
        user = self.get_object()
        serializer = FeedbackSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(account=user.account)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserUploadView(generics.CreateAPIView):
    swagger_schema = None
    parser_classes = (MultiPartParser, )
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated, UserSourceParamPermission, UserAdminAccessPermission, )
    serializer_class = UserUploadCSVSerializer
