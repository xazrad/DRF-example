from jet.dashboard.modules import DashboardModule


class StatisticModule(DashboardModule):
    title = 'Flexvoucher statistic'
    template = 'flexvoucher/modules/statistic.html'

    def init_with_context(self, context):
        self.children = [{'key': 0}]
