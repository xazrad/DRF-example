from .celery import app as celery_app


default_app_config = 'flexvoucher.apps.FlexVoucherConfig'

__all__ = ['celery_app']
