from functools import update_wrapper

from django import forms
from django.contrib import admin
from django.contrib.admin.utils import unquote
from django.core.exceptions import PermissionDenied
from django.forms.models import ModelForm
from django.http.response import FileResponse, HttpResponse
from django.utils.safestring import mark_safe
from django.urls import reverse

from flexvoucher import models
from flexvoucher import tasks
from flexvoucher.signals import loglevel_reload
from .common import ReadOnlyMixin


class LogLevelForm(forms.ModelForm):
    choices = (
        ('NOTSET', 'NOTSET'),
        ('DEBUG', 'DEBUG'),
        ('INFO', 'INFO'),
        ('WARNING', 'WARNING'),
        ('ERROR', 'ERROR'),
        ('CRITICAL', 'CRITICAL'),
    )

    value = forms.ChoiceField(choices=choices)

    def __init__(self, *args, **kwargs):
        super(LogLevelForm, self).__init__(*args, **kwargs)

    class Meta:
        model = models.ApplicationParameter
        fields = '__all__'


class ConsistencyTaskResultsInline(ReadOnlyMixin, admin.TabularInline):
    model = models.ConsistencyTaskResultsTask
    readonly_fields = ('consistencytask', 'is_successful', 'description',)


@admin.register(models.ApplicationParameter)
class ApplicationParameterAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'description')
    fields = ('name', 'value', 'description')

    def get_form(self, request, obj=None, change=False, **kwargs):
        self.form = ModelForm
        if obj.name == 'loglevel':
            self.form = LogLevelForm
        return super().get_form(request, obj, change, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return None

    def has_add_permission(self, request):
        return

    def save_model(self, request, obj, form, change):
        if obj.name == 'loglevel':
            loglevel_reload.send(sender=self.__class__, loglevel=obj.value)
        obj.save()

    def get_readonly_fields(self, request, obj=None):
        if obj.name == 'loglevel':
            return ('name', 'description')
        return self.list_display


@admin.register(models.Feedback)
class FeedbackAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'voucher', 'phone', 'rating', 'timestamp')
    readonly_fields = ('id', 'voucher', 'account', 'phone', 'rating', 'timestamp', 'comment', 'voucher_link')

    def voucher_link(self, obj=None):
        url = reverse('admin:flexvoucher_voucher_change', args=(obj.voucher_id, ))
        return mark_safe('<a href="%s" target="_blank">Voucher</a>' % url)
    voucher_link.allow_tags = True
    voucher_link.short_description = 'Voucher'


@admin.register(models.ConsistencyTask)
class ConsistencyTaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active', 'description',)
    # inlines = (CustomPeriodicTaskResultsAdmin, )
    readonly_fields = ('name', 'description', )
    actions = ['run_tasks', ]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return obj is None

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions.pop('delete_selected')
        return actions

    def run_tasks(self, request, queryset):
        self.message_user(request, 'Task queued')
        task_list = list(queryset.order_by('order_id').values_list('pk', flat=True))
        tasks.consistency_execute_chain_tasks.delay(task_list)
    run_tasks.short_description = "Run tasks"


@admin.register(models.ConsistencyTaskResults)
class ConsistencyTaskResultsAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'is_successful', 'start_at', 'end_at')
    readonly_fields = ('id', 'start_at', 'end_at', 'is_successful')
    inlines = (ConsistencyTaskResultsInline, )

    def is_successful(self, obj=None):
        if obj is None:
            return True
        _has_errors = obj.consistencytaskresultstask_set.filter(is_successful=False).exists()
        return not _has_errors
    is_successful.boolean = True
    is_successful.short_description = 'is successful'


@admin.register(models.DLPFiles)
class DLPFilesAdmin(admin.ModelAdmin):
    list_display = ('number', 'period', 'date_from', 'date_to', 'timestamp')
    exclude = ('file', )
    readonly_fields = ('number', 'period', 'file_link', 'date_from', 'date_to', 'timestamp')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def file_link(self, obj=None):
        return mark_safe('<a href="download/">File</a>')
    file_link.allow_tags = True
    file_link.short_description = 'File'

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = super().get_urls()
        urlpatterns.insert(0, path('<path:object_id>/change/download/',
                                   wrap(self.download_file), name='%s_%s_download' % info),)
        return urlpatterns

    def download_file(self, request, object_id, extra_context=None):
        obj = self.get_object(request, unquote(object_id), None)

        if not self.has_view_or_change_permission(request, obj):
            raise PermissionDenied
        response = HttpResponse(obj.file, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s' % obj.file.name.split('/')[-1]
        return response
