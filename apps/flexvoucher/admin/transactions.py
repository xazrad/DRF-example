from jet import filters

from django.contrib import admin

from flexvoucher import models
from .inlines import PointsLoadHistoryline, VoucherHistoryInline
from .common import ReadOnlyMixin


@admin.register(models.TransactionType)
class TransactionTypeAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'points_or_money', 'credit_or_debit', 'enabled', 'description')
    readonly_fields = ('id', 'points_or_money', 'credit_or_debit', 'enabled', 'description')


@admin.register(models.Transaction)
class TransactionAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('transaction_type', 'account', 'amount', 'timestamp')
    readonly_fields = ('transaction_type', 'account', 'amount', 'timestamp', 'points_balance', 'money_balance')
    inlines = (VoucherHistoryInline, PointsLoadHistoryline,)
    list_filter = ('transaction_type', ('timestamp', filters.DateRangeFilter),)
