from functools import update_wrapper

from django import forms
from django.contrib import admin
from django.contrib.admin.utils import unquote
from django.http.response import HttpResponse
from django.urls import reverse
from django.utils.safestring import mark_safe

from flexvoucher import models
from .inlines import MoneyTransferHistoryInline
from .common import ReadOnlyMixin


class MoneyTransferForm(forms.ModelForm):
    class Meta:
        model = models.MoneyTransfers
        fields = '__all__'

    def clean_money_status(self):
        value = self.cleaned_data['money_status']
        if self.instance.money_status in [2, 3]:
            raise forms.ValidationError('Forbidden change record with status %s.' % self.instance.money_status.description)
        if self.instance.money_status_id == value.pk:
            raise forms.ValidationError('Value has not changed.')
        return value


class MoneyTransferInline(ReadOnlyMixin, admin.TabularInline):
    model = models.MoneyTransfers
    readonly_fields = ('id', 'account', 'money_amount', 'timestamp',)
    fields = ('id', 'account', 'money_amount', 'timestamp',)


class WhoDownloadedInline(ReadOnlyMixin, admin.TabularInline):
    model = models.MoneyTransferFileDownload
    readonly_fields = ('user', 'timestamp')


@admin.register(models.MoneyTransferType)
class MoneyTransferTypeAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'description', 'enabled',)
    readonly_fields = ('description', 'enabled',)


@admin.register(models.MoneyTransferFile)
class MoneyTransferFile(admin.ModelAdmin):
    list_display = ('id', 'is_blocked', 'timestamp')
    fields = ('file_link', 'timestamp', 'is_blocked')
    readonly_fields = ('timestamp', 'file_link')
    inlines = (MoneyTransferInline, WhoDownloadedInline,)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return self.readonly_fields
        return self.readonly_fields + ('is_blocked', )

    def file_link(self, obj=None):
        if obj.is_blocked:
            return 'File is blocked.'
        return mark_safe('<a href="download/">File</a>')
    file_link.allow_tags = True
    file_link.short_description = 'File'

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = super().get_urls()
        urlpatterns.insert(0, path('<path:object_id>/change/download/',
                                   wrap(self.download_file), name='%s_%s_download' % info),)
        return urlpatterns

    def download_file(self, request, object_id, extra_context=None):
        from flexvoucher.models import MoneyTransferFileDownload

        obj = self.get_object(request, unquote(object_id), None)

        if not self.has_view_or_change_permission(request, obj):
            pass
            # raise PermissionDenied
        response = HttpResponse()
        if obj.is_blocked:
            return response
        obj.is_blocked = True
        obj.save()
        MoneyTransferFileDownload.objects.create(
            user=request.user,
            moneytransferfile=obj
        )
        response = HttpResponse(obj.file, content_type='text/xml')
        response['Content-Disposition'] = 'attachment; filename=%s' % obj.file.name.split('/')[-1]
        return response


@admin.register(models.MoneyTransfers)
class MoneyTransferAdmin(admin.ModelAdmin):
    list_display = ('id', 'link_voucher', 'money_amount', 'money_status', 'link_account', 'money_type', 'timestamp')
    form = MoneyTransferForm
    fields = ('link_money_tranfer_file', 'link_account', 'link_voucher', 'money_type',
              'money_status', 'money_amount', 'timestamp')
    readonly_fields = ('link_money_tranfer_file', 'link_account', 'link_voucher',
                       'money_type', 'money_amount', 'timestamp')
    inlines = (MoneyTransferHistoryInline, )
    list_filter = ('money_status', )

    def link_account(self, obj=None):
        url = reverse('admin:user_user_change', args=(obj.account.owner_id,))
        return mark_safe('<a href="%s" target="_blank">%s</a>' % (url, obj.account))
    link_account.allow_tags = True
    link_account.short_description = 'Account'

    def link_voucher(self, obj=None):
        url = reverse('admin:flexvoucher_voucher_change', args=(obj.voucher_id,))
        return mark_safe('<a href="%s" target="_blank">%s</a>' % (url, obj.voucher.voucher_uic))
    link_voucher.allow_tags = True
    link_voucher.short_description = 'Voucher'

    def link_money_tranfer_file(self, obj=None):
        url = reverse('admin:flexvoucher_moneytransferfile_change', args=(obj.money_tranfer_file_id,))
        return mark_safe('<a href="%s" target="_blank">%s</a>' % (url, obj.money_tranfer_file))
    link_money_tranfer_file.allow_tags = True
    link_money_tranfer_file.short_description = 'Money transfer file'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
