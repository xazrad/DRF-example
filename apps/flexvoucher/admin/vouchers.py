from functools import update_wrapper

from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.contrib.admin.utils import unquote
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.safestring import mark_safe

from flexvoucher import models
from .inlines import VoucherHistoryInline
from .common import ReadOnlyMixin


class FeedbackInline(ReadOnlyMixin, admin.StackedInline):
    model = models.Feedback
    readonly_fields = ('id', 'link_account', 'voucher', 'phone', 'rating', 'timestamp', 'comment',)
    exclude = ('account', )

    def link_account(self, obj=None):
        url = reverse('admin:user_user_change', args=(obj.account.owner_id,))
        return mark_safe('<a href="%s" target="_blank">%s</a>' % (url, obj.account))
    link_account.allow_tags = True
    link_account.short_description = 'Account'


class InvoiceDataInline(admin.StackedInline):
    model = models.InvoiceData
    readonly_fields = ('price', 'control_link')
    fields = ('price', 'control_link')

    def has_delete_permission(self, request, obj=None):
        return False

    def control_link(self, obj):
        return mark_safe('</script><div id="control-link"><div>')
    control_link.allow_tags = True
    control_link.short_description = 'Scans'

    class Media:
        css = {
            'all': (
                'flexvoucher/js/invoice-scan/1.1e1fc647.chunk.css',
                'flexvoucher/js/invoice-scan/main.83a5b83e.chunk.css',
            )
        }
        js = (
            'flexvoucher/js/invoice-scan/1.16182cf1.chunk.js',
            'flexvoucher/js/invoice-scan/main.a2a5555a.chunk.js',
            'flexvoucher/js/invoice-scan/runtime~main.229c360f.js',
        )


class MetaVoucherInline(ReadOnlyMixin, admin.TabularInline):
    model = models.VoucherMeta
    readonly_fields = ('meta_key', 'meta_value',)


@admin.register(models.Voucher)
class VoucherAdmin(ReadOnlyMixin, admin.ModelAdmin):
    inlines = (InvoiceDataInline, VoucherHistoryInline, FeedbackInline, MetaVoucherInline)
    list_display = ('account', 'voucher_type', 'voucher_uic', 'voucher_status', 'points_amount', 'date_in', 'date_out')
    search_fields = ('voucher_uic', )
    readonly_fields = ('voucher_reference', 'voucher_uic', 'invoice_price', 'date_in', 'date_out')
    exclude = ('account',)
    change_form_template = 'flexvoucher/admin/vouсher/change_form.html'
    delete_confirmation_template = 'flexvoucher/admin/vouсher/delete_confirmation.html'

    def get_readonly_fields(self, request, obj=None):
        """
        Hook for specifying custom readonly fields.
        """
        # if obj is None:
        #     return self.readonly_fields
        return ('voucher_status', 'link_account', 'voucher_type', 'points_amount', 'money_amount') + self.readonly_fields

    def link_account(self, obj=None):
        url = reverse('admin:user_user_change', args=(obj.account.owner_id,))
        return mark_safe('<a href="%s" target="_blank">%s</a>' % (url, obj.account))
    link_account.allow_tags = True
    link_account.short_description = 'Account'

    def response_storno(self, request, obj, is_return_dbl):
        obj_display = obj.voucher_uic
        opts = self.model._meta
        # check money balance
        if obj.money_amount > obj.account.money_balance:
            self.message_user(request, 'Account has no enough money balance', messages.ERROR)
            url = reverse(
                'admin:%s_%s_change' % (opts.app_label, opts.model_name),
                args=(obj.id, ),
                current_app=self.admin_site.name,
            )
            return HttpResponseRedirect(url)
        # storno
        obj.voucher_status_id = 7
        obj.save(force_update=True, update_fields=['voucher_status_id'])
        # deleted
        obj.voucher_status_id = 8
        obj.save(force_update=True, update_fields=['voucher_status_id'], is_return_dbl=is_return_dbl)
        self.message_user(
            request,
            'The %(name)s "%(obj)s" was storned successfully.' % {
                'name': opts.verbose_name,
                'obj': obj_display,
            },
            messages.SUCCESS,
        )

        post_url = reverse(
            'admin:%s_%s_changelist' % (opts.app_label, opts.model_name),
            current_app=self.admin_site.name,
        )
        preserved_filters = self.get_preserved_filters(request)
        post_url = add_preserved_filters(
            {'preserved_filters': preserved_filters, 'opts': opts}, post_url
        )
        return HttpResponseRedirect(post_url)

    def storno_voucher_view(self, request, object_id, extra_context=None):
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))
        if obj is None:
            return self._get_obj_does_not_exist_redirect(request, opts, object_id)
        if obj.voucher_status_id != 5:
            self.message_user(request, 'Invalid operation. Voucher not in redeemed status.', messages.INFO)
            url = reverse(
                'admin:%s_%s_change' % (opts.app_label, opts.model_name),
                args=(obj.id, ),
                current_app=self.admin_site.name,
            )
            return HttpResponseRedirect(url)
        if not request.user.is_superuser:
            raise PermissionDenied

        if request.POST:
            is_return_dbl = request.POST.get('is_return_dbl') == 'on'
            return self.response_storno(request, obj, is_return_dbl)

        object_name = str(opts.verbose_name)
        context = {
            **self.admin_site.each_context(request),
            'title': "Are you sure?",
            'object_name': object_name,
            'object': obj,
            'opts': opts,
            'app_label': app_label,
            **(extra_context or {}),
        }

        return self.render_delete_form(request, context)

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = super().get_urls()
        urlpatterns.insert(0, path('<path:object_id>/storno/',
                                   wrap(self.storno_voucher_view), name='%s_%s_storno' % info),)
        return urlpatterns


@admin.register(models.InvoiceScan)
class InvoiceScanAdmin(admin.ModelAdmin):
    list_filter = ('invoice_data', 'deleted')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.VoucherType)
class VoucherTypeAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'description', 'validity_points_in_hours', 'enabled')
    readonly_fields = ('description', 'validity_points_in_hours', 'enabled')


@admin.register(models.VoucherStatus)
class VoucherStatusAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'transaction_type', 'enabled', 'description')
    readonly_fields = ('transaction_type', 'enabled', 'description')
