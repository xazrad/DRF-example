from django import forms
from django.contrib import admin

from flexvoucher import models
from .inlines import PointsLoadHistoryline
from .common import ReadOnlyMixin


class PointsLoadForm(forms.ModelForm):
    class Meta:
        model = models.PointsLoads
        fields = '__all__'

    def clean_account(self):
        value = self.cleaned_data['account']
        if value.is_test:
            raise forms.ValidationError('Account for technical needs only.')
        return value

    def clean_points_status(self):
        value = self.cleaned_data['points_status']
        # new object
        if self.instance.pk is None and value.pk not in [1, 2]:
            raise forms.ValidationError('Incorrect value.')
        # approved  or cancelled
        if self.instance.points_status_id in [2, 3]:
            raise forms.ValidationError('Forbidden change record with status %s.' % self.instance.points_status.description)

        if self.instance.points_status_id == value.pk:
            raise forms.ValidationError('Value has not changed.')
        return value


class PointsLoadTransactionTypeInline(ReadOnlyMixin, admin.StackedInline):
    model = models.PointsLoadTransactionType
    readonly_fields = ('pointsload_status', 'transaction_type', 'description')


@admin.register(models.PointsLoadStatus)
class PointsLoadStatusAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'enabled', 'description')
    readonly_fields = ('enabled', 'description')
    inlines = (PointsLoadTransactionTypeInline,)


@admin.register(models.PointsLoadType)
class PointsLoadTypeAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('id', 'enabled', 'description')
    readonly_fields = ('enabled', 'description')
    inlines = (PointsLoadTransactionTypeInline,)


@admin.register(models.PointsLoads)
class PointsLoadsAdmin(admin.ModelAdmin):
    list_display = ('id', 'account', 'pointsload_type', 'points_status', 'points_amount', 'timestamp')
    inlines = (PointsLoadHistoryline, )
    form = PointsLoadForm

    def get_readonly_fields(self, request, obj=None):
        if obj is None:
            return ()
        return ('account', 'pointsload_type', 'points_amount')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def has_add_permission(self, request):
        from flexvoucher.models import ApplicationParameter

        return ApplicationParameter.get_param('source_users') == 'local'
