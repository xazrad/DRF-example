from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from flexvoucher import models

from .common import ReadOnlyMixin


class VoucherHistoryInline(ReadOnlyMixin, admin.TabularInline):
    model = models.VoucherHistory
    readonly_fields = ('voucher', 'voucher_status', 'transaction', 'timestamp', 'link_transaction', 'link_voucher')

    def link_transaction(self, obj=None):
        if obj.transaction_id is None:
            return
        url = reverse('admin:flexvoucher_transaction_change', args=(obj.transaction_id,))
        return mark_safe('<a href="%s" target="_blank">Transactions</a>' % url)
    link_transaction.allow_tags = True
    link_transaction.short_description = 'Transaction'

    def link_voucher(self, obj=None):
        url = reverse('admin:flexvoucher_voucher_change', args=(obj.voucher_id,))
        return mark_safe('<a href="%s" target="_blank">Voucher</a>' % url)
    link_voucher.allow_tags = True
    link_voucher.short_description = 'Voucher'


class PointsLoadHistoryline(ReadOnlyMixin, admin.TabularInline):
    model = models.PointsLoadHistory
    readonly_fields = ('transaction', 'points_load', 'points_status', 'timestamp', 'link_transaction')

    def link_transaction(self, obj=None):
        if obj.transaction_id is None:
            return
        url = reverse('admin:flexvoucher_transaction_change', args=(obj.obj.transaction_id,))
        return mark_safe('<a href="%s" target="_blank">Transactions</a>' % url)

    link_transaction.allow_tags = True
    link_transaction.short_description = 'Transaction'


class MoneyTransferHistoryInline(ReadOnlyMixin, admin.TabularInline):
    model = models.MoneyTransferHistory
    readonly_fields = ('money_transfer', 'money_status', 'timestamp',)
    fields = ('money_transfer', 'money_status', 'timestamp',)
