import random
import time
from string import digits, ascii_lowercase

from lxml import etree

from django.conf import settings
from django.db.models import Sum
from django.utils.timezone import now


def generate_voucher_uic():
    source = '%s%s' % (ascii_lowercase, digits)
    return ''.join([random.choice(source) for _ in range(8)])


def generate_money_transfer_file(voucher_query):
    SMAP = {None: 'urn:iso:std:iso:20022:tech:xsd:pain.001.001.03',
            "xsi": 'http://www.w3.org/2001/XMLSchema-instance'}

    total_amount = voucher_query.aggregate(money_amount=Sum('money_amount'))['money_amount']
    count_transfers = voucher_query.count()
    create_time = now().strftime('%Y-%m-%dT%H:%M:%S')
    message_id = 'FV' + str(time.time()).replace('.', '')

    Document = etree.Element("Document", nsmap=SMAP)
    CstmrCdtTrfInitn = etree.SubElement(Document, "CstmrCdtTrfInitn")

    GrpHdr = etree.SubElement(CstmrCdtTrfInitn, "GrpHdr")
    etree.SubElement(GrpHdr, "MsgId").text = message_id
    etree.SubElement(GrpHdr, "CreDtTm").text = create_time
    etree.SubElement(GrpHdr, "NbOfTxs").text = str(count_transfers)
    etree.SubElement(GrpHdr, "CtrlSum").text = str(total_amount)
    InitgPty = etree.SubElement(GrpHdr, "InitgPty")
    etree.SubElement(InitgPty, "Nm").text = settings.DEBITOR_NAME

    for voucher in voucher_query:
        PmtInf = etree.SubElement(CstmrCdtTrfInitn, "PmtInf")
        etree.SubElement(PmtInf, 'PmtInfId').text = '%s-%s' % (message_id, voucher.pk)
        etree.SubElement(PmtInf, 'PmtMtd').text = 'TRF'
        PmtTpInf = etree.SubElement(PmtInf, 'PmtTpInf')
        SvcLvl = etree.SubElement(PmtTpInf, 'SvcLvl')
        etree.SubElement(SvcLvl, 'Cd').text = 'SEPA'

        etree.SubElement(PmtInf, 'ReqdExctnDt').text = now().strftime('%Y-%m-%d')

        Dbtr = etree.SubElement(PmtInf, 'Dbtr')
        etree.SubElement(Dbtr, 'Nm').text = settings.DEBITOR_NAME

        DbtrAcct = etree.SubElement(PmtInf, 'DbtrAcct')
        Id = etree.SubElement(DbtrAcct, 'Id')
        etree.SubElement(Id, 'IBAN').text = settings.DEBITOR_IBAN

        DbtrAgt = etree.SubElement(PmtInf, 'DbtrAgt')
        FinInstnId = etree.SubElement(DbtrAgt, 'FinInstnId')
        etree.SubElement(FinInstnId, 'BIC').text = settings.DEBITOR_AGENT_BIC

        etree.SubElement(PmtInf, 'ChrgBr').text = 'SLEV'

        CdtTrfTxInf = etree.SubElement(PmtInf, 'CdtTrfTxInf')
        PmtId = etree.SubElement(CdtTrfTxInf, 'PmtId')
        etree.SubElement(PmtId, 'EndToEndId').text = str(voucher.account_id)

        Amt = etree.SubElement(CdtTrfTxInf, 'Amt')
        InstdAmt = etree.SubElement(Amt, 'InstdAmt')
        InstdAmt.set('Ccy', 'EUR')
        InstdAmt.text = str(voucher.money_amount)

        Cdtr = etree.SubElement(CdtTrfTxInf, 'Cdtr')
        etree.SubElement(Cdtr, 'Nm').text = '%s %s' % (voucher.account.owner.first_name, voucher.account.owner.last_name)

        CdtrAcct = etree.SubElement(CdtTrfTxInf, 'CdtrAcct')
        Id = etree.SubElement(CdtrAcct, 'Id')
        etree.SubElement(Id, 'IBAN').text = voucher.account.iban

        RmtInf = etree.SubElement(CdtTrfTxInf, 'RmtInf')
        etree.SubElement(RmtInf, 'Ustrd').text = 'FLEXVOUCHER {} VOM {}'.format(voucher.voucher_uic, voucher.date_in.strftime('%Y-%m-%d'))
    return etree.tostring(Document, pretty_print=True)
