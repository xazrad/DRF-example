import tempfile
import os
from decimal import Decimal, ROUND_HALF_UP
from datetime import datetime, time, timedelta
from pathlib import Path

import pdfkit
from celery import group
from celery.utils.log import get_task_logger

from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import Sum
from django.db.transaction import atomic
from django.template import loader
from django.utils.dateparse import parse_datetime
from django.utils.timezone import make_aware, now, localdate

from flexvoucher.celery import app
from flexvoucher.utils import generate_money_transfer_file


logger = get_task_logger(__name__)


@app.task
def check_status_voucher(voucher_id):
    from flexvoucher.models import Voucher

    voucher = Voucher.objects.get(pk=voucher_id)
    # only created
    if voucher.voucher_status_id == 1:
        # check as expired
        voucher.voucher_status_id = 3
        with atomic():
            voucher.save()


@app.task
def prepare_money_transfer_file():
    from flexvoucher.models import ApplicationParameter, MoneyTransfers, MoneyTransferFile, Voucher

    if ApplicationParameter.get_param('immediately_money_tranfer') == 'true':
        return
    # select redeemed vouchers
    voucher_query = Voucher.objects.filter(voucher_status_id=5,
                                           account__is_test=False).select_related('account__owner', 'account')
    if not voucher_query.exists():
        return
    file = generate_money_transfer_file(voucher_query)
    with atomic():
        moneytrans_obj = MoneyTransferFile()
        moneytrans_obj.save()
        moneytrans_obj.file = ContentFile(file.decode(), '%s-%s.xml' % (now().strftime('%Y-%m-%d'),
                                                                        moneytrans_obj.pk,))
        moneytrans_obj.save()

        moneyobj_list = [MoneyTransfers(account_id=_.account_id,
                                        money_tranfer_file=moneytrans_obj,
                                        voucher_id=_.pk,
                                        money_type_id=1,  # Daily Transfer via ASDAG bank
                                        money_status_id=1,  # Transfered to Bank
                                        money_amount=_.money_amount) for _ in voucher_query]
        list(map(lambda x: x.save(), moneyobj_list))


@app.task
def statement_generation(account_id, start_date, end_date):
    from flexvoucher.models import Statement, Transaction
    start_date = make_aware(parse_datetime(start_date))
    end_date = make_aware(parse_datetime(end_date))
    query = Transaction.objects.filter(timestamp__gte=start_date,
                                       timestamp__lte=end_date,
                                       account_id=account_id).order_by('timestamp')
    # total credit point
    # Rewarding points/ Return points
    total_points_credit = query.filter(transaction_type_id__in=[1, 4]).aggregate(Sum('amount'))['amount__sum'] or 0
    # total debit point
    # Redeeming points by Voucher/ Annul points/ Redeeming points by Order
    total_points_debit = query.filter(transaction_type_id__in=[2, 3, 7]).aggregate(Sum('amount'))['amount__sum'] or 0
    # total credit money
    # Credit money by Voucher redeem/  Money back - correction
    total_money_credit = query.filter(transaction_type_id__in=[5, 8]).aggregate(Sum('amount'))['amount__sum'] or 0
    # total debit money
    # Money transfer to Bank account
    total_money_debit = query.filter(transaction_type_id=6).aggregate(Sum('amount'))['amount__sum'] or 0
    first_trx_id = None if query.first() is None else query.first().pk
    last_trx_id = None if query.last() is None else query.last().pk
    # previous statement
    prev_statement = Statement.objects.filter(account_id=account_id, period__lt=start_date.date()).order_by('-period').first()
    if prev_statement is None:
        init_points_amount = 0
        init_money_amount = 0
    else:
        init_points_amount = prev_statement.end_points_amount
        init_money_amount = prev_statement.end_money_amount

    end_points_amount = init_points_amount + total_points_credit - total_points_debit
    end_money_amount = init_money_amount + total_money_credit - total_money_debit
    obj = Statement(
        account_id=account_id,
        init_points_amount=init_points_amount,
        init_money_amount=init_money_amount,
        end_points_amount=end_points_amount,
        end_money_amount=end_money_amount,
        total_points_credit=total_points_credit,
        total_points_debit=total_points_debit,
        total_money_credit=total_money_credit,
        total_money_debit=total_money_debit,
        first_trx_id=first_trx_id,
        last_trx_id=last_trx_id,
        period=end_date.date(),
    )
    obj.save()
    return obj.pk


@app.task
def prepare_generate_statements():
    from flexvoucher.models import Account
    # define start/end date
    yesterday = localdate() - timedelta(days=1)
    end_date = datetime.combine(yesterday, time.max)
    start_date = datetime.combine(yesterday.replace(day=1), time.min)
    account_id_list = Account.objects.filter(owner__is_active=True, owner__is_staff=False).values_list('id', flat=True)
    tasks_list = list()
    for account_id in account_id_list:
        tasks_list.append(statement_generation.si(account_id, start_date, end_date))
    if tasks_list:
        group(*tasks_list).delay()


@app.task
def generate_DLP_statement(period=None):
    from flexvoucher.models import DLPFiles, VoucherHistory

    if period is None:
        # define start/end date
        yesterday = localdate() - timedelta(days=1)
        start_date = datetime.combine(yesterday.replace(day=1), time.min)
        end_date = datetime.combine(yesterday, time.max)
    else:
        start_date = parse_datetime(period[0])
        end_date = parse_datetime(period[1])
    start_date = make_aware(start_date)
    end_date = make_aware(end_date)

    query = VoucherHistory.objects.filter(voucher_status_id=10,
                                          timestamp__gte=start_date,
                                          timestamp__lte=end_date)
    query_aggregate = query.aggregate(points_amount_sum=Sum('voucher__points_amount'),
                                      money_amount_sum=Sum('voucher__money_amount'))
    # avoid None values
    points_amount_sum = query_aggregate['points_amount_sum'] or 0
    money_amount_sum = query_aggregate['money_amount_sum'] or 0
    query_rows = list(query.values('timestamp', 'voucher__points_amount', 'voucher__date_in',
                                   'voucher__money_amount', 'voucher__voucher_reference',
                                   'voucher__voucher_uic', 'voucher__account__owner__first_name',
                                   'voucher__account__owner__last_name'))
    # set DLP commission
    list(
        map(
            lambda x: x.__setitem__(
                'dlp',
                (x['voucher__money_amount'] * Decimal(settings.C_DLP)).quantize(
                    Decimal('0.01'),
                    rounding=ROUND_HALF_UP
                )
            ),
            query_rows
        )
    )
    # Trans
    list(
        map(
            lambda x: x.__setitem__(
                'trans',
                (x['voucher__money_amount'] * Decimal(settings.C_TRANS)).quantize(
                    Decimal('0.01'),
                    rounding=ROUND_HALF_UP
                )
            ),
            query_rows
        )
    )
    dlp_sum = sum([_['dlp'] for _ in query_rows])
    trans_sum = sum([_['trans'] for _ in query_rows])
    ctx = dict(
        start_date=start_date,
        end_date=end_date,
        query_rows=query_rows,
        points_amount_sum=points_amount_sum,
        money_amount_sum=money_amount_sum,
        dlp_sum=dlp_sum,
        trans_sum=trans_sum
    )
    body_html = loader.render_to_string("flexvoucher/statement/table.html", ctx)
    footer = Path(Path(__file__).parent.parent, 'templates/flexvoucher/statement/footer.html')
    header = Path(Path(__file__).parent.parent, 'templates/flexvoucher/statement/header.html')

    options = {
        'header-html': str(header),
        'footer-html': str(footer),
        'footer-line': '',
        'orientation': 'Landscape',
    }
    period = localdate(end_date)
    number = f'{period.month:02}/{period.year:02}'
    with tempfile.NamedTemporaryFile(suffix='.html', delete=False) as header_html:
        ctx_header = {
            'number': number,
            'statement_date': period.strftime('%Y-%m-%d'),
            'statement_start': start_date.strftime('%Y-%m-%d'),
            'statement_end': end_date.strftime('%Y-%m-%d'),
        }
        options['header-html'] = header_html.name
        header_html.write(loader.render_to_string("flexvoucher/statement/header.html", ctx_header).encode('utf-8'))

    out_file = pdfkit.from_string(body_html, output_path=False, options=options)
    DLPFiles.objects.create(
        number=number,
        file=ContentFile(out_file, '1.pdf'),
        period=period,
        date_from=start_date,
        date_to=end_date,
    )
    os.remove(options['header-html'])
