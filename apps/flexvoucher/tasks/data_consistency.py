import json
from datetime import date, datetime, timedelta
from decimal import Decimal

from celery import chain
from celery.utils.log import get_task_logger
from raven.contrib.django.raven_compat.models import client as raven_client

from django.db.models import Q
from django.utils.timezone import now

from flexvoucher.celery import app


logger = get_task_logger(__name__)


def json_serial(obj):
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    if isinstance(obj, date):
        return obj.strftime('%Y-%m-%d')
    if isinstance(obj, Decimal):
        return float(obj)


@app.task(bind=True, consistency_task_id=1)
def consistency_money_points_balance(self, task_result_id, is_end=False):
    """ All money and points balances for every user should be >= 0 at any time.
        id = 1
    """
    from flexvoucher.models import Account, ConsistencyTaskResults, ConsistencyTaskResultsTask

    query = Account.objects.filter(Q(money_balance__lt=0) | Q(points_balance__lt=0))
    kwargs = dict()
    if query.exists():
        extra = dict(extra=list(query.values('pk', 'owner__username', 'points_balance', 'money_balance')))
        kwargs['is_successful'] = False
        kwargs['description'] = json.dumps(extra, default=json_serial)
        raven_client.captureMessage(self.name, extra=extra)
    ConsistencyTaskResultsTask.objects.create(consistencytaskresult_id=task_result_id,
                                              consistencytask_id=self.consistency_task_id, **kwargs)
    if is_end:
        ConsistencyTaskResults.objects.filter(pk=task_result_id).update(end_at=now())


@app.task(bind=True, consistency_task_id=2)
def consistency_vouchers_status(self, task_result_id, is_end=False):
    """ Every voucher older than 24 h must be in one of end statuses
        Cancelled, Expired, Redeemed, Annulled, Posted
        id = 2
    """
    from flexvoucher.models import ConsistencyTaskResults, ConsistencyTaskResultsTask, Voucher

    check_time = now() - timedelta(days=1)
    query = Voucher.objects.filter(date_in__lt=check_time).filter(voucher_status_id=1)
    kwargs = dict()
    if query.exists():
        extra = dict(extra=list(query.values('pk', 'account', 'voucher_status', 'date_in')))
        kwargs['is_successful'] = False
        kwargs['description'] = json.dumps(extra, default=json_serial)
        raven_client.captureMessage(self.name, extra=extra)
    ConsistencyTaskResultsTask.objects.create(consistencytaskresult_id=task_result_id,
                                              consistencytask_id=self.consistency_task_id, **kwargs)
    if is_end:
        ConsistencyTaskResults.objects.filter(pk=task_result_id).update(end_at=now())


@app.task(bind=True)
def consistency_execute_chain_tasks(self, task_ids=None):
    from flexvoucher.models import ConsistencyTask, ConsistencyTaskResults

    if task_ids is None:
        task_ids = list(ConsistencyTask.objects.filter(is_active=True).order_by('order_id').values_list('pk', flat=True))
    tasks_filtered = [_[1] for _ in self._app.tasks.items() if getattr(_[1], 'consistency_task_id', None)]
    tasks_filtered = list(filter(lambda x: x.consistency_task_id in task_ids, tasks_filtered))
    tasks_filtered = sorted(tasks_filtered, key=lambda x: task_ids.index(x.consistency_task_id))

    obj = ConsistencyTaskResults.objects.create()
    task_list = list()
    for ind, task in enumerate(tasks_filtered, 1):
        is_end = True if ind == len(tasks_filtered) else False
        task_list.append(task.si(obj.pk, is_end=is_end))
    chain(*task_list).apply_async()
