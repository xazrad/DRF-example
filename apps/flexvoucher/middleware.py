import logging

import jwt
from rest_framework.authentication import get_authorization_header
from rest_framework_jwt.settings import api_settings

from django.utils.encoding import smart_text


logger = logging.getLogger('django')


class FlexvoucherLogging:

    def __init__(self, get_response):
        self.get_response = get_response

    def _get_client(self, request):
        if request.user.is_authenticated:
            return request.user.username

    def _get_jwt_client(self, request):
        auth = get_authorization_header(request).split()
        auth_header_prefix = api_settings.JWT_AUTH_HEADER_PREFIX.lower()
        if not auth:
            return
        if smart_text(auth[0].lower()) != auth_header_prefix:
            return

        if len(auth) == 1 or len(auth) > 2:
            return
        try:
            payload = api_settings.JWT_DECODE_HANDLER(auth[1])
        except jwt.PyJWTError:
            return
        return payload['username']

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        username = self._get_jwt_client(request) or self._get_client(request) or 'Unknown'
        if request.META.get('CONTENT_TYPE') and request.META['CONTENT_TYPE'].startswith('multipart/form-data'):
            body_splitted = request.body.split(b'\r\n')
            body = [_ for _ in enumerate(body_splitted) if _[1].startswith(b'Content-Disposition: form-data;')]
            values = [body_splitted[_[0] + 2].decode() for _ in body]
            keys = [_[1].decode().replace('Content-Disposition: form-data;', "") for _ in body]
            body = dict(zip(keys, values))
        else:
            body = request.body.decode()
        template_log = 'Request {method} {path}, USERNAME: {username}'
        if body:
            template_log = 'Request {method} {path}, USERNAME: {username} BODY: {body}'
        full_path = request.get_full_path()
        if full_path.startswith('/v') and not full_path.startswith('/v1/report/'):
            logger.info(template_log.format(
                method=request.method,
                path=request.get_full_path(),
                username=username, body=body))
        response = self.get_response(request)
        # Code to be executed for each request/response after
        # the view is called.
        if response.get('Content-Type') == 'application/json' and full_path.startswith('/v') \
                and not full_path.startswith('/v1/report/'):
            status_code, content = response.status_code, response.content and response.content.decode()
            template_log = 'Response {method} {path}, USERNAME: {username} STATUS_CODE: {status_code} BODY: {content}'
            logger.info(template_log.format(
                method=request.method,
                path=request.get_full_path(),
                status_code=status_code,
                content=content,
                username=username, body=body))
        return response
