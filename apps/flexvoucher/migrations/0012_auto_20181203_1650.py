# Generated by Django 2.1.3 on 2018-12-03 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flexvoucher', '0011_auto_20181203_0839'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voucher',
            name='voucher_reference',
            field=models.CharField(max_length=60, null=True),
        ),
    ]
