import logging
import logging.config
import threading

import pika

from django.apps import AppConfig
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, pre_save

from .signals import loglevel_reload, post_save_voucher, post_save_user


logger = logging.getLogger('django')


class Propagator:
    def __init__(self):
        self.connection = self.channel = self.exchange_name = None
        self.amqp_thread = None
        self.exchange_name = 'logging_propagator'
        self.url = settings.CELERY_BROKER_URL

    def setup(self, callback):
        self.connection = pika.BlockingConnection(pika.URLParameters(self.url))
        channel = self.connection.channel()
        channel.exchange_declare(exchange=self.exchange_name, exchange_type='fanout')

        queue = channel.queue_declare(exclusive=True)
        queue_name = queue.method.queue
        channel.queue_bind(exchange=self.exchange_name, queue=queue_name)

        channel.basic_consume(callback, queue=queue_name, no_ack=True)
        self.amqp_thread = threading.Thread(name='Propagator Listener', target=channel.start_consuming)
        self.amqp_thread.daemon = True
        self.amqp_thread.start()

    def teardown(self):
        self.connection.ioloop.stop()
        self.amqp_thread = None
        self.connection = self.channel = self.exchange_name = None

    def propagate(self, sender, loglevel, **kwargs):
        connection = pika.BlockingConnection(pika.URLParameters(self.url))
        channel = connection.channel()
        channel.exchange_declare(exchange=self.exchange_name, exchange_type='fanout')
        channel.basic_publish(
            exchange=self.exchange_name,
            routing_key='',
            body=loglevel
        )


class FlexVoucherConfig(AppConfig):
    name = 'flexvoucher'
    verbose_name = 'FlexVoucher'

    def __init__(self, *args, **kwargs):
        self.propagator = None
        super(FlexVoucherConfig, self).__init__(*args, **kwargs)

    def _setup_log_level(self, *args, **kwargs):
        from flexvoucher.models import ApplicationParameter
        if args:
            # from rabbit
            loglevel = args[3].decode()
        else:
            # read from db
            try:
                loglevel = ApplicationParameter.get_param('loglevel')
                getattr(logging, loglevel)
            except Exception:
                loglevel = 'INFO'
        config = settings.LOGGING
        config['handlers']['console']['level'] = loglevel
        logging.config.dictConfig(config)

    def setup_propagator(self):
        if self.propagator is not None:
            self.propagator.teardown()
        self.propagator = Propagator()
        self.propagator.setup(self._setup_log_level)

    def ready(self):
        from flexvoucher.models import Account, Voucher

        USER_MODEL = get_user_model()

        self._setup_log_level()
        self.setup_propagator()
        post_save.connect(post_save_user, sender=USER_MODEL)
        post_save.connect(post_save_voucher, sender=Voucher)
        loglevel_reload.connect(self.propagator.propagate, sender=None)
