from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard
from jet.utils import get_admin_site_name

from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from .dashboard_modules import StatisticModule


class CustomIndexDashboard(Dashboard):
    columns = 2

    def init_with_context(self, context):
        self.available_children.append(modules.LinkList)
        self.available_children.append(StatisticModule)

        self.children.append(modules.AppList(
            _('Applications'),
            exclude=('auth.*', 'user.*'),
            column=0,
            order=0
        ))

        self.children.append(modules.LinkList(
            'Reports',
            draggable=True,
            deletable=False,
            collapsible=True,
            children=[
                ['DLP', reverse('report:DLP_view')],
                ['Vouchers', reverse('report:voucher_view')],
                ['Transactions', reverse('report:transaction_view')],
            ],
            column=0,
            order=1
        ))

        self.children.append(modules.LinkList(
            'Technical tasks',
            draggable=True,
            deletable=False,
            collapsible=True,
            children=[
                ['Clean test users data', reverse('report:clean_test_data_view')],
                ['Staff users sessions', reverse('report:user_sessions_view')],
            ],
            column=0,
            order=2
        ))

        self.children.append(StatisticModule(
            'Flexvoucher statistic',
            column=1,
            order=1
        ))

        self.children.append(modules.ModelList(
            _("Authentication and Authorization"),
            models=('auth.*', 'user.*'),
            column=1,
            order=2
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            10,
            column=1,
            order=3
        ))
