import pytest


@pytest.mark.django_db
class TestListCategory:

    def test_get(self, client):
        r = client.get('/v1/category/')
        assert r.status_code == 200
        assert len(r.json()) > 0


@pytest.mark.django_db
class TestItemSearch:

    @pytest.mark.parametrize("ItemPage,Keywords,SearchIndex,is_valid", [
        (1, 'golf', '', False),
        (1, 'golf', 'All', True),
        (1, 'kink', 'Shoes', True),
        (0, 'golf', 'HomeGarden', False),
    ])
    def test_get_0(self, client, ItemPage, Keywords, SearchIndex, is_valid):
        data = {
            'ItemPage': ItemPage,
            'Keywords': Keywords,
            'SearchIndex': SearchIndex,
        }
        if not SearchIndex:
            data.pop('SearchIndex')
        r = client.get('/v1/item/', data=data)
        if not SearchIndex:
            assert r.status_code == 400
        else:
            assert r.status_code == 200
            assert r.json()['Request']['IsValid'] == is_valid


@pytest.mark.django_db
class TestItemLookUp:

    @pytest.mark.parametrize("item_id, errors", [
        ('B075LXNGFH', False),
        ('B01LSUYLVE', False),
        ('000', True),
    ])
    def test_0(self, client, item_id, errors):
        r = client.get('/v1/item/{}/'.format(item_id))
        assert r.status_code == 200
        assert bool(r.json()['Request'].get('Errors')) == errors
