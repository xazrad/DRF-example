import json

import pytest

from django.urls import reverse

from flexvoucher.models import ApplicationParameter, PointsLoads, Transaction, Voucher
from user.dbl_api import dbl_client
from user.models import User


def get_jwt_token(user_id):
    from rest_framework_jwt.settings import api_settings

    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    user = User.objects.get(pk=user_id)

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


@pytest.mark.django_db
class TestVoucherList:
    url = reverse('voucher-list')

    @pytest.mark.parametrize("user_id, status_code, count_voucher, q_filter", [
        (None, 401, 0, None),
        (1, 200, 10, None),  # superuser
        (1, 200, 1, '?voucher_status=2'),  # superuser
        (2, 200, 10, None),  # admin
        (3, 200, 3, None),
    ])
    def test_0(self, client, user_id, status_code, count_voucher, q_filter):
        auth_headers = dict()
        if user_id:
            token = get_jwt_token(user_id)
            auth_headers = {'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),}
        url = self.url
        if q_filter:
            url = url + q_filter
        r = client.get(url, content_type='application/json', **auth_headers)
        if status_code == 200:
            assert len(r.json()['results']) == count_voucher
        assert r.status_code == status_code


@pytest.mark.django_db
class TestVoucherDetail:

    @pytest.mark.parametrize("user_id, status_code, voucher_id", [
        (None, 401, 1),
        (1, 200, 1),
        (3, 200, 3),
        (3, 404, 1),
    ])
    def test_0(self, client, user_id, status_code, voucher_id):
        auth_headers = dict()
        if user_id:
            token = get_jwt_token(user_id)

            auth_headers = {
                'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
                'HTTP_X_FORWARDED_PROTO': 'https',
                'test': 'test'
            }
        url = reverse('voucher-detail', kwargs={'pk': voucher_id})
        r = client.get(url, content_type='application/json', **auth_headers)
        if status_code == 200:
            assert r.json()
        assert r.status_code == status_code


@pytest.mark.django_db
class TestVoucherCreate:
    url = reverse('voucher-list')

    @pytest.mark.parametrize("user_id, status_code, source_users, data", [
        (3, 201, 'local', {'points_amount': '120', 'money_amount': '0.5', 'invoice_price': 1000, 'meta': {'a': 1}}),
        (1, 400, 'local', {'points_amount': '32035', 'money_amount': 133.48, 'invoice_price': 133.48}),
        (1, 400, 'local', {'points_amount': '1200', 'money_amount': '0.5', 'invoice_price': 1000}),
        (3, 400, 'local', {'points_amount': '120', 'money_amount': '0.5', 'invoice_price': 0.1}),
        (3, 400, 'local', {'points_amount': '120', 'money_amount': '2'}),
        (3, 400, 'local', {'points_amount': 'abc', 'money_amount': '0.5'}),
        (None, 401, 'local', {'points_amount': '120', 'money_amount': '0.5'}),
        (100, 201, 'dbl', {'points_amount': '120', 'money_amount': '0.5', 'invoice_price': 1000}),
        (None, 401, 'dbl', {'points_amount': '120', 'money_amount': '0.5'}),
    ])
    def test_0(self, client, monkeypatch, mocker, user_id, status_code, source_users, data):
        def mock_get_param(name):
            return source_users
        monkeypatch.setattr(ApplicationParameter, 'get_param', mock_get_param)

        def mock_request_point(*args, **kwargs):
            return '0000'
        monkeypatch.setattr(dbl_client, 'request_point', mock_request_point)
        mocker.spy(dbl_client, 'request_point')

        def mock_balance(*args, **kwargs):
            return {'balance': 1000000}
        monkeypatch.setattr(dbl_client, 'balance', mock_balance)
        mocker.spy(dbl_client, 'balance')

        auth_headers = dict()
        user = None
        if user_id:
            token = get_jwt_token(user_id)
            auth_headers = {
                'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
            }
            user = User.objects.get(pk=user_id)
        r = client.post(self.url, json.dumps(data), content_type='application/json', **auth_headers)
        assert status_code == r.status_code
        if status_code != 201:
            return
        assert r.json()['id']
        is_called = 1 if source_users == 'dbl' else False
        assert dbl_client.request_point.called == is_called
        assert dbl_client.balance.called == is_called
        # check balance
        user.refresh_from_db()
        assert user.account.points_balance == 0.0
        # assert user.account.money_balance == 1.0
        voucher_obj = Voucher.objects.get(id=r.json()['id'])
        assert voucher_obj.voucher_status_id == 1
        assert voucher_obj.voucher_history.count() == 1
        assert voucher_obj.voucher_history.first().transaction.transaction_type_id == 2
        count_transaction = 1 if source_users == 'local' else 2
        if user.account.is_test:
            count_transaction = 0
        assert Transaction.objects.filter(account=user.account).count() == count_transaction
        if source_users == 'dbl':
            # check transaction
            load_obj = PointsLoads.objects.filter(account=user.account).first()
            assert load_obj.account_id == user.account.pk
            assert load_obj.points_amount == 120


@pytest.mark.django_db
class TestVoucherUpdateLocal:

    @pytest.mark.parametrize("user_id, status_code, voucher_id, voucher_status_id, immediately_money_tranfer", [
        (1, 200, 1, 4, 'false'),  # set posted
        (1, 200, 6, 4, 'true'),  # set posted
        (1, 400, 1, 5, 'false'),  # set redeem
        (1, 200, 1, 2, 'false'),  # set cancelled
        (1, 400, 5, 50, 'false'),  # invalid status
        (1, 400, 1, '3', 'false'),  # set voucher status expired
        (2, 400, 2, 2, 'false'),  # voucher is already cancelled
        (3, 404, 1, 2, 'false'),  # user have no access to voucher
    ])
    def test_0(self, client, monkeypatch, user_id, status_code, voucher_id, voucher_status_id, immediately_money_tranfer):
        def mock_get_param(name):
            if name == 'auto_redeem_voucher':
                return 'true'
            if name == 'source_users':
                return 'local'
            if name == 'immediately_money_tranfer':
                return immediately_money_tranfer
        monkeypatch.setattr(ApplicationParameter, 'get_param', mock_get_param)

        token = get_jwt_token(user_id)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        data = {'voucher_status': {'id': voucher_status_id}}
        url = reverse('voucher-detail', kwargs={'pk': voucher_id})
        r = client.patch(url, json.dumps(data), content_type='application/json', **auth_headers)
        # TODO: check balance
        assert status_code == r.status_code


@pytest.mark.django_db
class TestVoucherUpdateDBL:

    @pytest.mark.parametrize("user_id, status_code, voucher_id, voucher_status_id", [
        (1, 400, 1, 5),  # set redeem
        (1, 200, 1, 4),  # posted
        (1, 200, 1, 2),  # cancelled
        (1, 400, 1, 50),  # invalid status
        (1, 400, 1, '3'),  # set voucher status expired
        (2, 400, 2, 2),  # voucher is already cancelled
        (3, 404, 1, 2),  # user have no access to voucher
    ])
    def test_0(self, client, monkeypatch, user_id, status_code, voucher_id, voucher_status_id):
        def mock_get_param(name):
            if name == 'source_users':
                return 'dbl'
        monkeypatch.setattr(ApplicationParameter, 'get_param', mock_get_param)

        def mock_return_points(*args, **kwargs):
            return
        monkeypatch.setattr(dbl_client, 'return_points', mock_return_points)
        token = get_jwt_token(user_id)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        data = {'voucher_status': {'id': voucher_status_id}}
        url = reverse('voucher-detail', kwargs={'pk': voucher_id})
        r = client.patch(url, json.dumps(data), content_type='application/json', **auth_headers)
        # TODO: check balance
        assert status_code == r.status_code


@pytest.mark.django_db
class TestIvoiceDataPut:

    @pytest.mark.parametrize("status_code, user_id, voucher_id, data", [
        (400, 3, 3, {'shop_address': 'street-street', 'date': '2018-09-30', 'price': '0.1'}),  # Price is not equals vouchers invoice_price
        (200, 3, 3, {'shop_address': 'street-street', 'date': '2018-09-30', 'price': '1000'}),
        (403, 3, 4, {'shop_address': 'street-street', 'date': '2018-09-30', 'price': '1000'}),  # voucher redeemed
        (200, 1, 4, {'shop_address': 'street-street', 'date': '2018-09-30', 'price': '1000'}),
        (404, 3, 300, {'shop_address': 'street-street', 'date': '2018-09-30', 'price': '1000'}),
    ])
    def test_0(self, client, status_code, user_id, voucher_id, data):
        token = get_jwt_token(user_id)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('voucher-invoice_data', kwargs={'pk': voucher_id})
        r = client.put(url, json.dumps(data), content_type='application/json', **auth_headers)
        if r.status_code == 200:
            assert r.json()['shop_address'] == 'street-street'
        assert status_code == r.status_code


@pytest.mark.django_db
class TestScanPost:

    @pytest.mark.parametrize("user_id, status_code, voucher_id", [
        (3, 201, 3),  # is_staff=False, voucher: created & account = 3
        (3, 403, 9999),  # voucher does not exists
        (3, 403, 4),  # is_staff=False, voucher: redeem & account = 3
        (1, 403, 3),  # is_staff=True voucher created & account = 3
        (1, 201, 4),  # is_staff=True voucher redeem & account = 3
        (3, 403, 1),  # is_staff=False, voucher account = 1
        (None, 401, 2),
    ])
    def test_0(self, client, example_scan, example_scan2, user_id, status_code, voucher_id):
        auth_headers = dict()
        if user_id:
            token = get_jwt_token(user_id)
            auth_headers = {
                'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
            }
        url = reverse('voucher-scan-list', kwargs={'voucher_pk': voucher_id})
        data = {'image': [example_scan, example_scan2]}
        r = client.post(url, data, **auth_headers)
        assert status_code == r.status_code


@pytest.mark.django_db
class TestScanDelete:
    @pytest.mark.parametrize("user_id, status_code, voucher_id, scan_id", [
        (3, 204, 3, 3),  # is_staff=False, voucher: created & account = 3
        (3, 403, 9999, 3),  # voucher does not exists
        (3, 403, 4, 3),  # is_staff=False, voucher: redeem & account = 3
        (1, 403, 3, 3),  # is_staff=True voucher created & account = 3
        (1, 204, 4, 4),  # is_staff=True voucher redeem & account = 3
    ])
    def test_0(self, client, user_id, status_code, voucher_id, scan_id):
        auth_headers = dict()
        if user_id:
            token = get_jwt_token(user_id)
            auth_headers = {
                'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
            }
        url = reverse('voucher-scan-detail', kwargs={'voucher_pk': voucher_id, 'pk': scan_id})
        r = client.delete(url, **auth_headers)
        assert status_code == r.status_code


@pytest.mark.django_db
class TestVoucherFeedback:

    @pytest.mark.parametrize("user_id, voucher_id, status_code, data", [
        (3, 1, 404, {'comment': 'blah', 'rating': 1, 'phone': '+1458989'}),
        (1, 1, 400, {'comment': 'blah', 'rating': 100, 'phone': '+1458989'}),
        (1, 1, 400, {'comment': '', 'rating': 1, 'phone': '+1458989'}),
        (1, 1, 201, {'comment': """Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
                sunt in culpa qui officia deserunt mollit anim id est laborum.""", 'phone': '+14957777777', 'rating': 1})
    ])
    def test_set_feedback(self, client, user_id, voucher_id, status_code, data):
        token = get_jwt_token(user_id)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
        }
        url = reverse('voucher-feedback', kwargs={'pk': voucher_id})
        r = client.post(url, json.dumps(data), content_type='application/json', **auth_headers)
        assert r.status_code == status_code
