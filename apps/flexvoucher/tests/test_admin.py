from django.test import TestCase
from django.test.client import Client
from django.urls import reverse


class TestAdminModels(TestCase):
    fixtures = ('groups.json', 'user.json', 'account.json',
                'voucher.json', 'invoice_data.json', 'invoice_scan.json')

    def setUp(self):
        self.c = Client()
        self.c.login(username='superuser@example.com', password='q123q123Az')

    def test_index(self):
        response = self.c.get('/admin/')
        self.assertEqual(response.status_code, 200)

    def test_admin_list(self):
        from django.contrib import admin
        models = [model for model in admin.site._registry]
        for model in models:
            url = reverse('admin:%s_%s_changelist' % (model._meta.app_label, model._meta.model_name))
            response = self.c.get(url)
            self.assertEqual(response.status_code, 200)

    def test_admin_obj(self):
        from django.contrib import admin
        models = [model for model in admin.site._registry]
        for model in models:
            query = model.objects.first()
            if not query:
                continue
            url = reverse('admin:%s_%s_change' % (model._meta.app_label, model._meta.model_name), args=(query.pk,))
            response = self.c.get(url)
            self.assertEqual(response.status_code, 200)
