import pytest

from flexvoucher.models import Account, ConsistencyTaskResults, Voucher
from flexvoucher.tasks import (check_status_voucher, prepare_generate_statements, prepare_money_transfer_file,
                               statement_generation, consistency_money_points_balance, consistency_vouchers_status,
                               consistency_execute_chain_tasks, generate_DLP_statement)


@pytest.mark.django_db
class TestCeleryTask:

    def test_0(self, app_celery):
        # TODO: check balance
        check_status_voucher.apply(args=(1,)).get(timeout=60)
        assert True

    def test_prepare_file(self, app_celery):
        prepare_money_transfer_file.apply().get(timeout=60)
        assert Voucher.objects.get(pk=5).voucher_status_id == 9
        assert Voucher.objects.get(pk=5).moneytransfers_set.count() == 1

    def test_prepare_statements(self, app_celery):
        prepare_generate_statements.apply().get(timeout=60)
        assert True

    def test_generate_statement(self, app_celery):
        account_id_list = Account.objects.filter(owner__is_active=True).values_list('id', flat=True)
        statement_id_list = list()
        for account_id in account_id_list:
            pk = statement_generation.apply(args=(account_id,
                                                  '2018-10-01T00:00:00',
                                                  '2018-10-22T23:59:59')).get(timeout=60)
            statement_id_list.append(pk)

    @pytest.mark.parametrize("status, period", [
        (1, ('2018-01-01T00:00:00', '2018-12-31T23:59:59')),
        (1, None)
    ])
    def test_generate_DLP_statement(self, app_celery, status, period):
        generate_DLP_statement.apply(args=(period,))
        assert True


@pytest.mark.django_db
class TestCeleryConsistencyTask:

    def test_0_execute_chain(self, app_celery):
        consistency_execute_chain_tasks.apply().get(timeout=60)
        assert True

    def test_1_execute_chain(self, app_celery):
        consistency_execute_chain_tasks.apply(args=([1],)).get(timeout=60)
        assert True

    def test_1_check_points_money_balance(self, app_celery):
        obj = ConsistencyTaskResults.objects.create()
        consistency_money_points_balance.apply(args=(obj.pk,), kwargs={'is_end': True}).get(timeout=60)
        assert True

    def test_2_check_vouchers_status(self, app_celery):
        obj = ConsistencyTaskResults.objects.create()
        consistency_vouchers_status.apply(args=(obj.pk,), kwargs={'is_end': True}).get(timeout=60)
        assert True
