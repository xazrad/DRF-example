import json

import pytest

from django.urls import reverse

from flexvoucher import models
from user.dbl_api import dbl_client
from user.models import User
from .test_voucher import get_jwt_token


@pytest.mark.django_db
@pytest.mark.parametrize("user_id, source_users, auto_redeem_voucher, immediately_money_tranfer", [
    (3, 'dbl', 'true', 'false'),
    (3, 'dbl', 'true', 'true'),
    (3, 'local', 'true', 'false'),
    (3, 'local', 'true', 'true'),
])
def test_create_posted_voucher_scene(client, monkeypatch, mocker, user_id, source_users, auto_redeem_voucher, immediately_money_tranfer):

    def mock_get_param(name):
        if name == 'source_users':
            return source_users
        if name == 'auto_redeem_voucher':
            return auto_redeem_voucher
        if name == 'immediately_money_tranfer':
            return immediately_money_tranfer
        raise Exception('Unknown param: %s.', name)
    monkeypatch.setattr(models.ApplicationParameter, 'get_param', mock_get_param)

    def mock_balance(*args, **kwargs):
        return {'balance': 1000000}
    monkeypatch.setattr(dbl_client, 'balance', mock_balance)
    mocker.spy(dbl_client, 'balance')

    def mock_request_point(*args, **kwargs):
        return '0000'
    monkeypatch.setattr(dbl_client, 'request_point', mock_request_point)
    mocker.spy(dbl_client, 'request_point')

    url = reverse('voucher-list')
    data = {'points_amount': '120', 'money_amount': '0.5', 'invoice_price': 1000}
    token = get_jwt_token(user_id)
    auth_headers = {
        'HTTP_AUTHORIZATION': 'Bearer {}'.format(token),
    }
    # create voucher
    r = client.post(url, json.dumps(data), content_type='application/json', **auth_headers)
    assert r.status_code == 201
    # check called request balance
    is_called = False if source_users == 'local' else 1
    assert dbl_client.balance.called == is_called
    # check celled request points called
    assert dbl_client.request_point.called == is_called
    # check balance user
    user = User.objects.get(pk=user_id)
    assert user.account.points_balance == 0.0 if source_users == 'local' else 120
    assert user.account.money_balance == 1.0
    # check voucher
    voucher_obj = models.Voucher.objects.get(pk=r.json()['id'])
    assert voucher_obj.voucher_status_id == 1  # voucher status created
    # check history
    assert voucher_obj.voucher_history.count() == 1
    # check transaction
    assert voucher_obj.voucher_history.first().transaction.transaction_type_id == 2  # Redeeming points by Voucher
    assert voucher_obj.voucher_history.first().transaction.amount == 120
    assert models.Transaction.objects.filter(account=user.account).count() == 1 if source_users == 'local' else 2
    if source_users == 'dbl':
        # load point from dbl
        assert models.PointsLoads.objects.filter(account=user.account).count() == 1
        point_load_obj = models.PointsLoads.objects.filter(account=user.account).first()
        assert point_load_obj.pointsload_type_id == 1  # Points are temporary rewarded in order to support voucher creation
        assert point_load_obj.points_status_id == 2  # Approved
        assert point_load_obj.points_amount == 120
        assert point_load_obj.pointsloadhistory_set.count()
        assert point_load_obj.pointsloadhistory_set.first().transaction.transaction_type_id == 1  # Rewarding points
    # posted voucher
    data = {'voucher_status': {'id': 4}}
    url = reverse('voucher-detail', kwargs={'pk': voucher_obj.pk})
    r = client.patch(url, json.dumps(data), content_type='application/json', **auth_headers)
    assert r.status_code == 200
    voucher_obj.refresh_from_db()
    user.refresh_from_db()
    if immediately_money_tranfer == 'false':
        assert voucher_obj.voucher_status_id == 4 if auto_redeem_voucher == 'false' else 5  # voucher created or redeemed
        # check history
        assert voucher_obj.voucher_history.count() == 2 if auto_redeem_voucher == 'false' else 3
        assert user.account.points_balance == 0.0 if source_users == 'local' else 120
        assert user.account.money_balance == 1.5
        assert voucher_obj.moneytransfers_set.count() == 0
        assert voucher_obj.voucher_history.order_by('-timestamp')[0].transaction.transaction_type_id == 5
    else:
        assert voucher_obj.voucher_status_id == 9  # Inpayment
        # check history
        assert voucher_obj.voucher_history.count() == 4  # created posted redeem inpayment
        assert user.account.money_balance == 1.0
        assert voucher_obj.moneytransfers_set.count() == 1
        assert voucher_obj.voucher_history.order_by('-timestamp')[0].transaction.transaction_type_id == 6
