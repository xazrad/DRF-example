from rest_framework import permissions

from .models import Voucher


class VoucherAccessPermission(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        is_staff = request.user.is_staff
        if view.action == 'set_feedback':
            return obj.account_id == request.user.account.pk
        if view.action == 'retrieve':
            return True if is_staff else obj.account_id == request.user.account.pk
        if not is_staff and obj.voucher_status_id != 1:
            return False
        if is_staff and obj.voucher_status_id in [4, 5]:
            return True
        return obj.account_id == request.user.account.pk


class VoucherInvoiceScanAccessPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        try:
            voucher = Voucher.objects.get(pk=view.kwargs['voucher_pk'])
        except Voucher.DoesNotExist:
            return False
        is_staff = request.user.is_superuser or request.user.is_admin
        if not is_staff and voucher.voucher_status_id != 1:
            return False
        if is_staff and voucher.voucher_status_id in [4, 5]:
            return True
        return voucher.account_id == request.user.account.pk
