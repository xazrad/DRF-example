import django.dispatch


def post_save_user(sender, instance, **kwargs):
    """
    Signal connected User post save signal
    for create one-to-one account model
    """

    from .models import Account

    if hasattr(instance, 'account'):
        # we cannot change is_status back
        if instance.account.is_test:
            return
        instance.account.is_test = instance.is_staff
        instance.account.save(dbl_update=False)
    else:
        Account.objects.create(owner=instance, is_test=instance.is_staff)


def post_save_voucher(sender, instance, **kwargs):
    """
    Signal connected Voucher post save signal
    for create one-to-one account model
    """

    from .models import InvoiceData

    if hasattr(instance, 'invoicedata'):
        return
    InvoiceData.objects.create(voucher=instance)


loglevel_reload = django.dispatch.Signal(providing_args=["loglevel"])
