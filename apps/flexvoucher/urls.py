"""flexvoucher URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework_nested import routers

from django.conf import settings
from django.contrib import admin
from django.views.static import serve
from django.urls import include, path, re_path

from . import views


schema_view = get_schema_view(
    openapi.Info(
        title="API v1 FlexVoucher",
        default_version='v1',
        description="API",
    ),
    public=True,
)

router = routers.DefaultRouter()
router.register(r'v1/voucher', views.VoucherViewSet, base_name='voucher')

scans_router = routers.NestedSimpleRouter(router, r'v1/voucher', lookup='voucher')
scans_router.register(r'scan', views.InvoiceScanViewSet, base_name='voucher-scan')

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('admin/', admin.site.urls),
    path('admin/statuscheck/', include('celerybeat_status.urls')),
    re_path(r'^doc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('v1/category/', views.CategoryView.as_view(), name='category_list'),
    path('v1/item/', views.ItemSearch.as_view(), name='item_list'),
    path('v1/item/<str:item_id>/', views.ItemLookUp.as_view(), name='item_detail'),
    path('v1/user/', include('user.urls', namespace='user')),
    path('v1/token/', include('auth_token.urls', namespace='token')),
    path('', include('report.urls', namespace='report')),
]

urlpatterns += router.urls
urlpatterns += scans_router.urls

if settings.DEBUG:
    urlpatterns.append(
        re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    )
