from rest_framework import serializers

from flexvoucher.models import SearchIndexAmazon


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = SearchIndexAmazon
        exclude = ('id', )


class ItemSearchRequestErrorsSerializer(serializers.Serializer):
    Code = serializers.CharField(source='Error.Code.text')
    Message = serializers.CharField(source='Error.Message.text')


class ItemSearchItemSearchRequestSerializer(serializers.Serializer):
    ItemPage = serializers.IntegerField(source='ItemPage.text')
    # ResponseGroup = serializers.CharField(source='ResponseGroup.text')
    # SearchIndex = serializers.CharField(source='SearchIndex.text')


class ItemRequestSerializer(serializers.Serializer):
    IsValid = serializers.BooleanField(source='IsValid.text')
    Errors = serializers.ListField(child=ItemSearchRequestErrorsSerializer(), required=False)
    ItemSearchRequest = ItemSearchItemSearchRequestSerializer(required=False)


class ItemAttribListSerializer(serializers.ListField):
    child = serializers.CharField()


class ItemAttribItemDimensionsSerializer(serializers.Serializer):
    Weight = serializers.CharField(required=False)
    Width = serializers.CharField(required=False)
    Length = serializers.CharField(required=False)
    Height = serializers.CharField(required=False)


class ItemSearchItemAttributesSerializer(serializers.Serializer):
    """
    Item Atrtibs
    https://docs.aws.amazon.com/AWSECommerceService/latest/DG/RG_ItemAttributes.html
    """
    Actor = serializers.CharField(source='Actor.text', required=False)
    Artist = serializers.CharField(source='Artist.text', required=False)
    ASIN = serializers.CharField(source='ASIN.text', required=False)
    AspectRatio = serializers.CharField(source='AspectRatio.text', required=False)
    AudienceRating = serializers.CharField(source='AudienceRating.text', required=False)
    AudioFormat = serializers.CharField(source='AudioFormat.text', required=False)
    Author = serializers.CharField(source='Author.text', required=False)
    CorrectedQuery = serializers.CharField(source='CorrectedQuery.text', required=False)
    Binding = serializers.CharField(source='Binding.text', required=False)
    Brand = serializers.CharField(source='Brand.text', required=False)
    Category = serializers.CharField(source='Category.text', required=False)
    CEROAgeRating = serializers.CharField(source='CEROAgeRating.text', required=False)
    ClothingSize = serializers.CharField(source='ClothingSize.text', required=False)
    Color = serializers.CharField(source='Color.text', required=False)
    # Creator
    Department = serializers.CharField(source='Department.text', required=False)
    Director = serializers.CharField(source='Director.text', required=False)
    EAN = serializers.CharField(source='EAN.text', required=False)
    # EANList
    Edition = serializers.CharField(source='Edition.text', required=False)
    EISBN = serializers.CharField(source='EISBN.text', required=False)
    EpisodeSequence = serializers.CharField(source='EpisodeSequence.text', required=False)
    # ESRBAgeRating
    Feature = ItemAttribListSerializer(required=False)
    Format = serializers.CharField(source='Format.text', required=False)
    Genre = serializers.CharField(source='Genre.text', required=False)
    HardwarePlatform = serializers.CharField(source='HardwarePlatform.text', required=False)
    HazardousMaterialType = serializers.CharField(source='HazardousMaterialType.text', required=False)
    IsAdultProduct = serializers.CharField(source='IsAdultProduct.text', required=False)
    IsAutographed = serializers.CharField(source='IsAutographed.text', required=False)
    IsMemorabilia = serializers.CharField(source='IsMemorabilia.text', required=False)
    IssuesPerYear = serializers.CharField(source='IssuesPerYear.text', required=False)
    ItemDimensions = ItemAttribItemDimensionsSerializer(required=False)
    ItemPartNumber = serializers.CharField(source='ItemPartNumber.text', required=False)
    Label = serializers.CharField(source='Label.text', required=False)
    # Languages
    LegalDisclaimer = serializers.CharField(source='LegalDisclaimer.text', required=False)
    ListPrice = serializers.IntegerField(source='ListPrice.Amount.text', required=False)
    Manufacturer = serializers.CharField(source='Manufacturer.text', required=False)
    Message = serializers.CharField(source='Message.text', required=False)
    ManufacturerMaximumAge = serializers.CharField(source='ManufacturerMaximumAge.text', required=False)
    ManufacturerMinimumAge = serializers.CharField(source='ManufacturerMinimumAge.text', required=False)
    ManufacturerPartsWarrantyDescription = serializers.CharField(source='ManufacturerPartsWarrantyDescription.text', required=False)
    MediaType = serializers.CharField(source='MediaType.text', required=False)
    Model = serializers.CharField(source='Model.text', required=False)
    MPN = serializers.CharField(source='MPN.text', required=False)
    NumberOfDiscs = serializers.CharField(source='NumberOfDiscs.text', required=False)
    NumberOfIssues = serializers.CharField(source='NumberOfIssues.text', required=False)
    NumberOfItems = serializers.CharField(source='NumberOfItems.text', required=False)
    NumberOfPages = serializers.CharField(source='NumberOfPages.text', required=False)
    NumberOfTracks = serializers.CharField(source='NumberOfTracks.text', required=False)
    OperatingSystem = serializers.CharField(source='OperatingSystem.text', required=False)
    PackageDimensions = ItemAttribItemDimensionsSerializer(required=False)
    PackageQuantity = serializers.IntegerField(source='PackageQuantity.text', required=False)
    PartNumber = serializers.CharField(source='PartNumber.text', required=False)
    Platform = serializers.CharField(source='Platform.text', required=False)
    ProductGroup = serializers.CharField(source='ProductGroup.text', required=False)
    ProductTypeSubcategory = serializers.CharField(source='ProductTypeSubcategory.text', required=False)
    PublicationDate = serializers.CharField(source='PublicationDate.text', required=False)
    Publisher = serializers.CharField(source='Publisher.text', required=False)
    RegionCode = serializers.CharField(source='RegionCode.text', required=False)
    ReleaseDate = serializers.CharField(source='ReleaseDate.text', required=False)
    RunningTime = serializers.CharField(source='RunningTime.text', required=False)
    # SeikodoProductCode
    Size = serializers.CharField(source='Size.text', required=False)
    SKU = serializers.CharField(source='SKU.text', required=False)
    Studio = serializers.CharField(source='Studio.text', required=False)
    SubscriptionLength = serializers.CharField(source='SubscriptionLength.text', required=False)
    Title = serializers.CharField(source='Title.text', required=False)
    TradeInValue = serializers.CharField(source='TradeInValue.text', required=False)
    UPC = serializers.CharField(source='UPC.text', required=False)


class ItemImageSerializer(serializers.Serializer):
    URL = serializers.URLField(source='URL.text')
    Height = serializers.IntegerField(source='Height.text')
    Width = serializers.IntegerField(source='Width.text')


class ItemAvailabilityAttributesSerializer(serializers.Serializer):
    AvailabilityType = serializers.CharField(required=False)
    MinimumHours = serializers.IntegerField(required=False)
    MaximumHours = serializers.IntegerField(required=False)


class ItemOfferListingSerializer(serializers.Serializer):
    Price = serializers.IntegerField(source='Price.Amount.text')
    AvailabilityAttributes = ItemAvailabilityAttributesSerializer()
    Availability = serializers.CharField(required=False)
    IsEligibleForSuperSaverShipping = serializers.IntegerField(required=False)
    IsEligibleForPrime = serializers.IntegerField(required=False)


class ItemOfferAttributesSerializer(serializers.Serializer):
    Condition = serializers.CharField()


class ItemOfferSerializer(serializers.Serializer):
    OfferAttributes = ItemOfferAttributesSerializer()
    OfferListing = ItemOfferListingSerializer()


class ItemOffersSerializer(serializers.Serializer):
    TotalOffers = serializers.IntegerField()
    TotalOfferPages = serializers.IntegerField()
    Offer = ItemOfferSerializer(required=False)


class ImageSetSerializer(serializers.Serializer):
    HiResImage = ItemImageSerializer(required=False)
    LargeImage = ItemImageSerializer(required=False)
    MediumImage = ItemImageSerializer(required=False)
    SmallImage = ItemImageSerializer(required=False)
    SwatchImage = ItemImageSerializer(required=False)
    ThumbnailImage = ItemImageSerializer(required=False)
    TinyImage = ItemImageSerializer(required=False)
    Category = serializers.SerializerMethodField(required=False)

    def get_Category(self, obj):
        return obj.attrib.get('Category')


class ItemItemSerializer(serializers.Serializer):
    ASIN = serializers.CharField(source='ASIN.text')
    ParentASIN = serializers.CharField(source='ParentASIN.text', required=False)
    ImageSets = serializers.ListSerializer(child=ImageSetSerializer(), source='ImageSets.ImageSet', required=False)
    ItemAttributes = ItemSearchItemAttributesSerializer()
    SalesRank = serializers.IntegerField(source='SalesRank.text', required=False)
    SmallImage = ItemImageSerializer(required=False)
    MediumImage = ItemImageSerializer(required=False)
    LargeImage = ItemImageSerializer(required=False)
    Offers = ItemOffersSerializer(required=False)


class ItemSearchResponseSerializer(serializers.Serializer):
    TotalPages = serializers.IntegerField(source='Items.TotalPages.text', required=False)
    TotalResults = serializers.IntegerField(source='Items.TotalResults.text', required=False)
    TotalPages = serializers.IntegerField(source='Items.TotalPages.text', required=False)
    Request = ItemRequestSerializer(source='Items.Request')
    Items = serializers.ListField(child=ItemItemSerializer(), source='Items.Item', required=False)


class ItemLookUpSerializer(serializers.Serializer):
    Request = ItemRequestSerializer(source='Items.Request')
    Items = serializers.ListField(child=ItemItemSerializer(), source='Items.Item', required=False)
