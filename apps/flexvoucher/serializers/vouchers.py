from decimal import Decimal, ROUND_HALF_UP

from raven.contrib.django.raven_compat.models import client
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import ValidationError

from django.conf import settings
from django.urls import reverse

from flexvoucher.models import (
    Account, ApplicationParameter, Feedback, InvoiceData, InvoiceScan, MoneyTransfers, Voucher,
    VoucherHistory, VoucherMeta, VoucherStatus, VoucherType
)
from user.dbl_api import dbl_client
from user.models import User
from user.utils import send_email


class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')


class AccountSerializer(ModelSerializer):
    owner = UserSerializer()

    class Meta:
        model = Account
        fields = ('id', 'owner')


class VoucherTypeSerializer(ModelSerializer):

    class Meta:
        model = VoucherType
        fields = '__all__'


class VoucherStatusSerializer(ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = VoucherStatus
        fields = ('id', 'enabled', 'description')
        read_only_fields = ('enabled', 'description')


class VoucherHistorySerializer(ModelSerializer):
    voucher_status = VoucherStatusSerializer()

    class Meta:
        model = VoucherHistory
        fields = ('id', 'timestamp', 'voucher_status')
        read_only_fields = ('id', 'timestamp', 'voucher_status')


class InvoiceScanMultipleSerializer(serializers.Serializer):
    image = serializers.ListField(child=serializers.FileField(allow_empty_file=False))

    def create(self, validated_data):
        invoice_data = validated_data.pop('invoice_data')
        data = list()
        for image in validated_data['image']:
            scan_obj = InvoiceScan(invoice_data=invoice_data, image=image)
            scan_obj.save()
            data.append(scan_obj)
        return data

    def to_representation(self, instance):
        data = list()
        request = self.context.get('request', None)
        for obj in instance:
            row = dict(id=obj.id, image=request.build_absolute_uri(obj.image.url))
            data.append(row)
        return data

    @property
    def data(self):
        return super(serializers.Serializer, self).data


class InvoiceScanSerializer(ModelSerializer):

    class Meta:
        model = InvoiceScan
        exclude = ('invoice_data', 'timestamp', 'deleted',)
        read_only_fields = ('timestamp', 'deleted')


class InvoiceDataSerializer(ModelSerializer):
    invoice_scans = InvoiceScanSerializer(many=True, read_only=True)

    def validate_price(self, value):
        if value != self.instance.voucher.invoice_price:
            raise serializers.ValidationError('Price is not equals vouchers invoice_price')
        return value

    def update(self, instance, validated_data):
        instance.price = validated_data.get('price', instance.price)
        instance.date = validated_data.get('date', instance.date)
        instance.shop_name = validated_data.get('shop_name', instance.shop_name)
        instance.shop_address = validated_data.get('shop_address', instance.shop_address)
        instance.shop_tax_id = validated_data.get('shop_tax_id', instance.shop_tax_id)
        instance.comment = validated_data.get('comment', instance.comment)
        instance.save()
        return instance

    class Meta:
        model = InvoiceData
        exclude = ('voucher', 'id',)


class VoucherSerializer(ModelSerializer):
    voucher_type = VoucherTypeSerializer(read_only=True)
    account = AccountSerializer(read_only=True)
    voucher_status = VoucherStatusSerializer()
    invoice_data = InvoiceDataSerializer(source='invoicedata', read_only=True)
    voucher_history = VoucherHistorySerializer(many=True, read_only=True)
    meta = serializers.DictField(write_only=True, required=False)

    def __init__(self, *args, **kwargs):
        super(VoucherSerializer, self).__init__(*args, **kwargs)
        if getattr(self.context['view'], 'action', None) and self.context['view'].action == 'list':
            self.fields.pop('invoice_data')
            self.fields.pop('voucher_history')

        if getattr(self.context['view'], 'action', None) and self.context['view'].action == 'update':
            self.fields.pop('points_amount')

    def validate_invoice_price(self, value):
        try:
            money_amount = round(Decimal(self.initial_data.get('money_amount')), 2)
        except Exception:
            return value
        if value < money_amount:
            raise serializers.ValidationError('Value must be greater than or equal to money_amount.')
        return value

    def validate_points_amount(self, value):
        user = self.context['request'].user
        # check balance
        if ApplicationParameter.get_param('source_users') == 'local' or user.account.is_test:
            user.account.refresh_from_db()
            points_balance = user.account.points_balance
        else:
            points_balance = int(dbl_client.balance(user.username)['balance'])
        if value > points_balance:
            raise ValidationError("Not enough user points.")
        return value

    def validate_money_amount(self, value):
        try:
            points_amount = int(self.initial_data.get('points_amount'))
        except Exception:
            return value
        points_amount_calculated = int((value * settings.C_RATION).quantize(Decimal('0'),
                                                                            rounding=ROUND_HALF_UP))
        if points_amount != points_amount_calculated:
            raise serializers.ValidationError('Incorrect calculated amount.')
        return value

    def validate_voucher_status(self, value):
        # creating new voucher
        if not self.instance:
            return value
        if self.instance.voucher_status_id == value['id']:
            raise ValidationError('The voucher has same status.')
        if not VoucherStatus.objects.filter(id=value['id']).exists():
            raise ValidationError('Invalid voucher status.')
        # user can change "created voucher to posted or cancelled
        if value['id'] not in [2, 4]:
            raise ValidationError('Forbidden to change status.')
        # check scan images
        if not self.instance.invoicedata.invoice_scans.exists() and value['id'] == 4:
            raise ValidationError('Voucher does not contain invoice`s information.')
        return value

    def create(self, validated_data):
        user = self.context['request'].user
        kwargs_voucher = {
            'voucher_type_id': 1,  # Standard Voucher
            'voucher_status_id': 1,  # Created
            'points_amount': validated_data['points_amount'],
            'money_amount': validated_data['money_amount'],
            'account': user.account,
            'invoice_price': validated_data['invoice_price']
        }

        if ApplicationParameter.get_param('source_users') == 'dbl' and not user.account.is_test:
            voucher_reference = dbl_client.request_point(user.username, validated_data['points_amount'])
            if voucher_reference is None:
                # user does not have enough points
                raise ValidationError("Not enough user points.")
            kwargs_voucher['voucher_reference'] = voucher_reference
        voucher = Voucher(**kwargs_voucher)
        voucher.save()
        if validated_data.get('meta'):
            list_objs = [VoucherMeta(meta_key=k,
                                     meta_value=v,
                                     voucher=voucher) for k, v in validated_data['meta'].items()]
            # because request points are performed
            try:
                VoucherMeta.objects.bulk_create(list_objs)
            except Exception:
                # send error to sentry
                client.captureException()
        return voucher

    def update(self, instance, validated_data):
        if validated_data.get('voucher_status') is None:
            return instance
        # user can only cancelled & posted voucher
        voucher_status_id = validated_data['voucher_status']['id']
        if voucher_status_id not in [2, 4]:
            return instance
        instance.voucher_status_id = voucher_status_id
        instance.save()
        # posted voucher to redeemed
        if instance.voucher_status_id == 4 and ApplicationParameter.get_param('auto_redeem_voucher') == 'true':
            instance.voucher_status_id = 5
            instance.save()
        # check immediately_money_tranfer
        if instance.voucher_status_id == 5 and ApplicationParameter.get_param('immediately_money_tranfer') == 'true':
            MoneyTransfers.objects.create(
                account=instance.account,
                voucher=instance,
                money_type_id=2,
                money_status_id=1,
                money_amount=instance.money_amount
            )
        return instance

    class Meta:
        model = Voucher
        fields = '__all__'
        read_only_fields = ('voucher_uic', 'voucher_reference', 'date_out', 'date_in')


class FeedbackSerializer(serializers.Serializer):
    phone = serializers.CharField(max_length=12, required=False)
    rating = serializers.ChoiceField(choices=[1, 2, 3, 4, 5])
    comment = serializers.CharField(max_length=2048)

    def create(self, validated_data):
        obj = Feedback(
            voucher=validated_data.get('voucher'),
            account=validated_data['account'],
            phone=validated_data.get('phone'),
            comment=validated_data['comment'],
            rating=validated_data['rating'],
        )
        obj.save()
        # send emails
        ctx = {
            'voucher_uic': '' if validated_data.get('voucher') is None else validated_data['voucher'].voucher_uic,
            'user': validated_data['account'].owner,
            'phone': validated_data.get('phone', ''),
            'comment': validated_data['comment'],
            'rating': validated_data['rating'],
            'link': reverse('admin:flexvoucher_feedback_change', args=(obj.pk,))
        }
        send_email(to=settings.MANAGERS, ctx=ctx,
                   subject_template_name='user/subject_feedback.txt',
                   template_name='user/email_feedback.html')
        return obj
