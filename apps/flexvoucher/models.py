import os

from datetime import timedelta
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
from user.dbl_api import dbl_client
from .tasks import check_status_voucher
from .utils import generate_voucher_uic


DEBIT, CREDIT = 'debit', 'credit'

POINTS, MONEY = 'points', 'money'


class Account(models.Model):
    """
    Model for User account
    """
    owner = models.OneToOneField(get_user_model(), verbose_name='User', on_delete=models.CASCADE)
    points_balance = models.DecimalField('Point balance', max_digits=10, decimal_places=2, default=0)
    money_balance = models.DecimalField('Money balance', max_digits=8, decimal_places=2, default=0)
    iban = models.CharField('IBAN', max_length=34, null=True, blank=True)
    tax_id = models.CharField('TAX ID', max_length=14, null=True, blank=True)
    vat_id = models.CharField('VAT ID', max_length=14, null=True, blank=True)
    created_at = models.DateTimeField('Created', default=timezone.now)
    updated_at = models.DateTimeField('Last change', auto_now=True)
    is_test = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % self.owner

    def save(self, *args, **kwargs):
        """
        Signal connected Account pre save signal
        for update profile DBL API
        """
        from flexvoucher.models import ApplicationParameter
        from user.dbl_api import dbl_client

        dbl_update = kwargs.pop('dbl_update', True)
        # New object. See Signal post_save_user
        if self.id is None or ApplicationParameter.get_param('source_users') == 'local' or self.is_test:
            return super().save(*args, **kwargs)

        if dbl_update:
            dbl_client.profile_update(
                username=self.owner.username,
                tax_id=self.tax_id,
                vat=self.vat_id,
                iban=self.iban
            )
        return super().save(*args, **kwargs)


class SearchIndexAmazon(models.Model):
    """
    Categories (Search indecies) for search
    """

    department = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    sort_values = ArrayField(base_field=models.CharField(max_length=64), null=True)
    search_parameters = ArrayField(base_field=models.CharField(max_length=64))

    class Meta:
        db_table = 'flexvoucher_search_index_amazon'

    def __str__(self):
        return 'Category %s' % self.department


class ApplicationParameter(models.Model):
    '''
    All params:
        - source_users - local (default)/ dbl
        - auto_redeem_voucher - false (default)/ true
        - immediately_money_tranfer - false (default)/ true
    '''
    name = models.CharField(max_length=48)
    value = models.CharField(max_length=48)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

    @classmethod
    def get_param(cls, name):
        """
        :param name: source_users ans etc
        :return:
        """
        return cls.objects.filter(name=name).values_list('value', flat=True)[0]


class TransactionType(models.Model):
    POINTS_OR_MONEY = (
        (POINTS, 'Points'),
        (MONEY, 'Money'),
    )

    CREDIT_OR_DEBIT = (
        (CREDIT, 'Credit'),
        (DEBIT, 'Debit'),
    )

    points_or_money = models.CharField(max_length=12, choices=POINTS_OR_MONEY)
    credit_or_debit = models.CharField(max_length=12, choices=CREDIT_OR_DEBIT)
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description


class Transaction(models.Model):
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.PROTECT)
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    points_balance = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    money_balance = models.DecimalField(max_digits=8, decimal_places=2, null=True)

    class Meta:
        ordering = ['timestamp']

    def __str__(self):
        return 'Transaction %s' % self.id

    def save(self, **kwargs):
        if not self.transaction_type_id or not self.transaction_type:
            return
        if self.account.is_test:
            return
        if self.id is not None:
            return
        super().save(force_insert=True)
        # debit/credit balance user account
        direction = 1 if self.transaction_type.credit_or_debit == CREDIT else -1
        if self.transaction_type.points_or_money == POINTS:
            kwargs_update = {
                'points_balance': models.F('points_balance') + direction * self.amount
            }
        if self.transaction_type.points_or_money == MONEY:
            kwargs_update = {
                'money_balance': models.F('money_balance') + direction * self.amount
            }
        # update user account
        Account.objects.filter(pk=self.account.pk).update(**kwargs_update)
        self.account.refresh_from_db(fields=['points_balance', 'money_balance'])
        # update info about balance
        self.points_balance = self.account.points_balance
        self.money_balance = self.account.money_balance
        super().save(force_update=True, update_fields=['points_balance', 'money_balance'])


class VoucherType(models.Model):
    validity_points_in_hours = models.PositiveSmallIntegerField(null=True)
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description


class VoucherStatus(models.Model):
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.PROTECT, null=True)
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name_plural = 'Voucher statuses'


class Voucher(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    voucher_type = models.ForeignKey(VoucherType, on_delete=models.PROTECT)
    voucher_status = models.ForeignKey(VoucherStatus, on_delete=models.PROTECT)
    points_amount = models.IntegerField()
    money_amount = models.DecimalField(max_digits=8, decimal_places=2)
    invoice_price = models.DecimalField(max_digits=8, decimal_places=2)
    voucher_reference = models.CharField(max_length=60, null=True)
    voucher_uic = models.CharField('Voucher ID', max_length=8, unique=True)
    date_out = models.DateTimeField(null=True)
    date_in = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.voucher_uic

    def save(self, **kwargs):
        is_return_dbl = kwargs.pop('is_return_dbl', False)
        is_new = False
        source_users = ApplicationParameter.get_param('source_users')
        # Create New Voucher
        if self.id is None:
            is_new = True
            # only "Created" status for New Voucher
            if self.voucher_status_id != 1:
                return
            # check balance in serializer.voucher.py::VoucherSerializer
            if self.voucher_type.validity_points_in_hours:
                validity_delta = self.voucher_type.validity_points_in_hours
                # set date_out date
                self.date_out = timezone.now() + timedelta(hours=validity_delta)
            else:
                self.date_out = None
            self.voucher_uic = generate_voucher_uic()
        super().save(**kwargs)
        self.refresh_from_db()
        # return point in DBL. cancelled, expired, annulled, deleted statuses
        if not is_new and source_users == 'dbl' and not self.account.is_test:
            if self.voucher_status_id in [2, 3, 6]:
                dbl_client.return_points(self.voucher_reference, self.account.owner.username,
                                         points_amount=self.points_amount)
            if self.voucher_status_id == 8 and is_return_dbl:
                dbl_client.return_points(self.voucher_reference, self.account.owner.username,
                                         points_amount=self.points_amount)
        # create task for expired status voucher
        if is_new and self.date_out:
            check_status_voucher.apply_async(args=(self.id,), eta=self.date_out)

        # create additional transaction for credit points
        if source_users == 'dbl' and self.voucher_status.transaction_type \
                and self.voucher_status.transaction_type.credit_or_debit == DEBIT \
                and self.voucher_status.transaction_type.points_or_money == POINTS:
            # save is disabled for is_tech_user
            PointsLoads.objects.create(
                account=self.account,
                pointsload_type_id=1,  # credit
                points_status_id=2,  # approved
                points_amount=self.points_amount
            )
        # create a transaction
        transaction_id = None
        if self.voucher_status.transaction_type_id:
            amount = self.points_amount if self.voucher_status.transaction_type.points_or_money == 'points' \
                else self.money_amount
            transaction_obj = Transaction(
                transaction_type_id=self.voucher_status.transaction_type_id,
                account=self.account,
                amount=amount,
            )
            # save is disabled for is_tech_user
            transaction_obj.save()
            transaction_id = transaction_obj.pk
        # create a history record
        VoucherHistory.objects.create(voucher_id=self.id,
                                      voucher_status_id=self.voucher_status_id,
                                      transaction_id=transaction_id)
        # create additional transaction for debit points
        if source_users == 'dbl' and self.voucher_status.transaction_type \
                and self.voucher_status.transaction_type.credit_or_debit == CREDIT \
                and self.voucher_status.transaction_type.points_or_money == POINTS:
            # save is disabled for is_tech_user
            PointsLoads.objects.create(
                account=self.account,
                pointsload_type_id=2,  # debit
                points_status_id=2,  # approved
                points_amount=self.points_amount
            )
        # action is_tech_user
        if self.account.is_test and is_new:
            self.account.points_balance = models.F('points_balance') - self.points_amount
            self.account.save()
        # cancelled or expired or annulled or deleted
        if self.account.is_test and self.voucher_status_id in [2, 3, 6, 8]:
            self.account.points_balance = models.F('points_balance') + self.points_amount
            self.account.save()

    class Meta:
        ordering = ('-date_in', )


class VoucherHistory(models.Model):
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE, related_name='voucher_history')
    voucher_status = models.ForeignKey(VoucherStatus, on_delete=models.PROTECT)
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Voucher history %s' % self.id

    class Meta:
        verbose_name_plural = 'Voucher history'
        ordering = ['timestamp']


class VoucherMeta(models.Model):
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE)
    meta_key = models.CharField('Key', max_length=128)
    meta_value = models.CharField('Value', max_length=128, null=True)

    class Meta:
        verbose_name_plural = 'System info'
        verbose_name = 'System info'


class InvoiceData(models.Model):
    voucher = models.OneToOneField(Voucher, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    date = models.DateField(null=True, blank=True)
    shop_name = models.CharField(max_length=64, null=True, blank=True)
    shop_address = models.CharField(max_length=256, null=True, blank=True)
    shop_tax_id = models.CharField(max_length=24, null=True, blank=True)
    comment = models.CharField(max_length=256, null=True, blank=True)

    def __str__(self):
        return 'Invoice data %s. %s' % (self.id, self.voucher)

    class Meta:
        verbose_name_plural = 'Invoice data'


class InvoiceScan(models.Model):

    def user_directory_path(instance, filename):
        path = 'scans/%s/%s.%s' % (timezone.now().strftime('%Y-%m-%d'),
                                   uuid4().hex,
                                   filename.split('.')[-1])
        return path

    invoice_data = models.ForeignKey(InvoiceData, on_delete=models.CASCADE, related_name='invoice_scans')
    image = models.FileField(upload_to=user_directory_path)
    timestamp = models.DateTimeField(default=timezone.now)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return 'Invoice scan %s' % self.id

    def delete(self, using=None, keep_parents=False):
        if self.image:
            if os.path.isfile(self.image.path):
                os.remove(self.image.path)
        return super().delete(using=using, keep_parents=keep_parents)


class PointsLoadType(models.Model):
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description


class PointsLoadStatus(models.Model):
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name_plural = 'Point loads statuses'


class PointsLoads(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    pointsload_type = models.ForeignKey(PointsLoadType, on_delete=models.PROTECT)
    points_status = models.ForeignKey(PointsLoadStatus, on_delete=models.PROTECT)
    points_amount = models.PositiveIntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'PointsLoads %s' % self.id

    class Meta:
        verbose_name_plural = 'Point loads'

    def save(self, **kwargs):
        if self.account.is_test:
            return
        super().save(**kwargs)
        transaction_obj = None
        # approved
        if self.points_status_id == 2:
            # create a transaction
            transaction_type_id = PointsLoadTransactionType.objects.get(pointsload_type_id=self.pointsload_type_id,
                                                                        pointsload_status_id=2).transaction_type_id
            transaction_obj = Transaction(
                transaction_type_id=transaction_type_id,
                account=self.account,
                amount=self.points_amount
            )
            transaction_obj.save()
        # create a history
        PointsLoadHistory.objects.create(transaction=transaction_obj,
                                         points_load_id=self.id,
                                         points_status_id=self.points_status_id)


class PointsLoadTransactionType(models.Model):
    pointsload_type = models.ForeignKey(PointsLoadType, on_delete=models.CASCADE)
    pointsload_status = models.ForeignKey(PointsLoadStatus, on_delete=models.CASCADE)
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.PROTECT)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description


class PointsLoadHistory(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT, null=True)
    points_load = models.ForeignKey(PointsLoads, on_delete=models.CASCADE)
    points_status = models.ForeignKey(PointsLoadStatus, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'PointLoad history %s' % self.id

    class Meta:
        verbose_name_plural = 'PointsLoad history'


class MoneyTransferType(models.Model):
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description


class MoneyTransferStatus(models.Model):
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name_plural = 'MoneyTransfer statuses'


class MoneyTransferFile(models.Model):
    file = models.FileField(upload_to='files/%Y/%m/%d/', null=True)
    timestamp = models.DateTimeField('Generated at', auto_now_add=True)
    is_blocked = models.BooleanField('Blocked', default=False)
    who_downloaded = models.ManyToManyField(get_user_model(), through='MoneyTransferFileDownload')

    def __str__(self):
        return 'File # %s %s' % (self.id, timezone.template_localtime(self.timestamp).strftime('%Y-%m-%d %H:%M:%S'))

    class Meta:
        verbose_name = 'Money transfer file'
        verbose_name_plural = 'Money transfer files'
        ordering = ['-timestamp']


class MoneyTransferFileDownload(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    moneytransferfile = models.ForeignKey(MoneyTransferFile, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Download history'
        verbose_name_plural = 'Download history'


class MoneyTransfers(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    money_type = models.ForeignKey(MoneyTransferType, verbose_name='Type', on_delete=models.PROTECT)
    money_status = models.ForeignKey(MoneyTransferStatus, verbose_name='Status', on_delete=models.PROTECT)
    money_amount = models.DecimalField(verbose_name='Euro', max_digits=8, decimal_places=2)
    money_tranfer_file = models.ForeignKey(MoneyTransferFile, on_delete=models.PROTECT, null=True)
    voucher = models.ForeignKey(Voucher, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'MoneyTransfer %s' % self.id

    class Meta:
        verbose_name_plural = 'Money transfer'

    def save(self, **kwargs):
        super().save(**kwargs)
        voucher_status_id = None
        # change status voucher
        # Transfered to Bank
        if self.money_status_id == 1:
            voucher_status_id = 9  # Inpayment
        # Setllement confirmed by Bank
        if self.money_status_id == 2:
            voucher_status_id = 10  # Paid
        # Transfer to bank problem
        if self.money_status_id == 3:
            voucher_status_id = 5  # Redeemed
        self.voucher.voucher_status_id = voucher_status_id
        self.voucher.save(force_update=True, update_fields=['voucher_status_id'])
        # create a history record
        MoneyTransferHistory.objects.create(money_transfer_id=self.id, money_status_id=self.money_status_id)


class MoneyTransferHistory(models.Model):
    money_transfer = models.ForeignKey(MoneyTransfers, on_delete=models.PROTECT)
    money_status = models.ForeignKey(MoneyTransferStatus, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'MoneyTransfer history %s' % self.id

    class Meta:
        verbose_name_plural = 'MoneyTransfer history'


class Statement(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    init_points_amount = models.DecimalField(max_digits=10, decimal_places=2)
    init_money_amount = models.DecimalField(max_digits=8, decimal_places=2)
    end_points_amount = models.DecimalField(max_digits=10, decimal_places=2)
    end_money_amount = models.DecimalField(max_digits=8, decimal_places=2)
    total_points_credit = models.DecimalField(max_digits=14, decimal_places=2)
    total_points_debit = models.DecimalField(max_digits=14, decimal_places=2)
    total_money_credit = models.DecimalField(max_digits=12, decimal_places=2)
    total_money_debit = models.DecimalField(max_digits=12, decimal_places=2)
    first_trx = models.ForeignKey(Transaction, on_delete=models.CASCADE, related_name='first_trx', null=True)
    last_trx = models.ForeignKey(Transaction, on_delete=models.CASCADE, related_name='last_trx', null=True)
    is_sent = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    period = models.DateField()  # last day of month

    def __str__(self):
        return '%s %s' % (self.account, self.period.isoformat())

    class Meta:
        ordering = ['-timestamp']
        unique_together = ('account', 'period')


class Feedback(models.Model):
    voucher = models.ForeignKey(Voucher, on_delete=models.SET_NULL, null=True)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, null=True)
    phone = models.CharField(max_length=12, null=True)
    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    comment = models.CharField(max_length=2048)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.id)

    class Meta:
        ordering = ['-timestamp']
        verbose_name_plural = 'Feedback'


class ConsistencyTask(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField(null=True)
    is_active = models.BooleanField(default=False)
    order_id = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.name)


class ConsistencyTaskResults(models.Model):
    tasks = models.ManyToManyField(ConsistencyTask, through='ConsistencyTaskResultsTask')
    start_at = models.DateTimeField(auto_now_add=True)
    end_at = models.DateTimeField(null=True)

    def __str__(self):
        return '{}'.format(self.id)

    class Meta:
        verbose_name = 'Consistency task results'
        verbose_name_plural = 'Consistency task results'
        ordering = ['-start_at']


class ConsistencyTaskResultsTask(models.Model):
    consistencytaskresult = models.ForeignKey(ConsistencyTaskResults, on_delete=models.CASCADE)
    consistencytask = models.ForeignKey(ConsistencyTask, on_delete=models.CASCADE)
    is_successful = models.BooleanField(default=True)
    description = models.TextField(null=True)


class DLPFiles(models.Model):

    def directory_path(instance, filename):
        return f'files/statements/commission/{instance.period.year}/' \
            f'statement-{instance.period.year}-{instance.period.month:02}-{instance.period.day:02}.pdf'

    number = models.CharField(max_length=7)
    file = models.FileField(upload_to=directory_path)
    period = models.DateField()  # last day of month
    date_from = models.DateTimeField()
    date_to = models.DateTimeField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.id)

    class Meta:
        ordering = ['-timestamp']
        verbose_name = 'DLP file'
        verbose_name_plural = 'DLP files'
