from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.app_settings import swagger_settings
from drf_yasg.inspectors import SwaggerAutoSchema
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from flexvoucher.models import InvoiceScan, InvoiceData, Voucher
from flexvoucher.permissions import VoucherAccessPermission, VoucherInvoiceScanAccessPermission
from flexvoucher.serializers import (FeedbackSerializer, InvoiceDataSerializer, InvoiceScanMultipleSerializer,
                                     VoucherSerializer)


class NoFileInspector(SwaggerAutoSchema):
    field_inspectors = swagger_settings.DEFAULT_FIELD_INSPECTORS[:4] + swagger_settings.DEFAULT_FIELD_INSPECTORS[5:]


class VoucherViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.CreateModelMixin,
                     mixins.UpdateModelMixin,
                     viewsets.GenericViewSet):
    """
    list:

    Returns Voucher's list

    `Authentication: JWT`

    `Permissions`

    * user (only for themself Vouchers)

    * super_user

    * admins

    retrieve:

    Returns Voucher's detail

    `Authentication: JWT`

    `Permissions`

    * super_user

    * admins

    * user (for self)

    create:

    Create a new Voucher.

    `Authentication: JWT`

    For create use `points_amount` parameter only!

    update:

    Change status Voucher.

    `Authentication: JWT`

    Params example:
    `{"voucher_status": {"id": 1}}`
    """
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, VoucherAccessPermission)
    queryset = Voucher.objects.all()
    serializer_class = VoucherSerializer
    pagination_class = PageNumberPagination
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('voucher_status',)

    def get_queryset(self):
        query = super(VoucherViewSet, self).get_queryset()
        if self.request.user.is_staff:
            return query
        return query.filter(account=self.request.user.account).all()

    def get_serializer(self, *args, **kwargs):
        if self.action == 'create' and kwargs:
            # Created
            kwargs['data']['voucher_status'] = {'id': 1}
        return super(VoucherViewSet, self).get_serializer(*args, **kwargs)

    @swagger_auto_schema(
        operation_description="Set invoice data.",
        request_body=InvoiceDataSerializer,
    )
    @action(methods=['put'], url_path='invoice-data', detail=True, url_name='invoice_data')
    def set_invoice_data(self, request, pk):
        voucher = self.get_object()
        serializer = InvoiceDataSerializer(instance=voucher.invoicedata, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_description="Send feedback.",
        request_body=FeedbackSerializer,
    )
    @action(methods=['post'], url_path='feedback', detail=True, url_name='feedback')
    def set_feedback(self, request, pk):
        voucher = self.get_object()
        account = self.request.user.account
        serializer = FeedbackSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(voucher=voucher, account=account)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class InvoiceScanViewSet(mixins.CreateModelMixin,
                         mixins.DestroyModelMixin,
                         viewsets.GenericViewSet):
    """
    create:

    Post a new Scan Image.

    `Authentication: JWT`

    destroy:

    """
    swagger_schema = NoFileInspector
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, VoucherInvoiceScanAccessPermission)
    serializer_class = InvoiceScanMultipleSerializer
    parser_classes = (MultiPartParser,)

    def perform_create(self, serializer):
        invoicedata = InvoiceData.objects.get(voucher_id=self.kwargs['voucher_pk'])
        serializer.save(invoice_data=invoicedata)

    def get_queryset(self):
        return InvoiceScan.objects.filter(invoice_data__voucher_id=self.kwargs['voucher_pk'], deleted=False)
