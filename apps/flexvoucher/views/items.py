import bottlenose
from lxml import objectify
from rest_framework import generics, status
from rest_framework.views import Response

from django.conf import settings

from flexvoucher.models import SearchIndexAmazon
from flexvoucher.serializers.items import CategorySerializer, ItemLookUpSerializer, ItemSearchResponseSerializer


amazon = bottlenose.Amazon(settings.AMAZON_ACCESS_KEY, settings.AMAZON_SECRET_KEY, settings.AMAZON_ASSOC_TAG, Region="DE")


class CategoryView(generics.ListAPIView):
    """
    get:
    Return list `SearchIndex` Amazon API.

    [Categories for DE](https://docs.aws.amazon.com/AWSECommerceService/latest/DG/LocaleDE.html)
    """
    queryset = SearchIndexAmazon.objects.all()
    serializer_class = CategorySerializer


class ItemSearch(generics.ListAPIView):
    """
    get:
    Search Items (goods) Amazon API.

    Use `query-string` for sort & filter.

    User the appropriate sorting & filtering options for the selected `category`.

    Required parameters:

    * `SearchIndex`

    * `Keywords`

    * `ItemPage` (starts from 1)

    [Doc](https://docs.aws.amazon.com/AWSECommerceService/latest/DG/LocaleDE.html)

    [Scratchpad](https://webservices.amazon.com/scratchpad/index.html)

    [ResponseGroups](https://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_ResponseGroupsList.html):

    * Images

    * Small
    """
    serializer_class = ItemSearchResponseSerializer

    def list(self, request, *args, **kwargs):
        q = request.query_params.dict()
        q['ResponseGroup'] = 'Images,Small'
        if not all(('ItemPage' in q.keys(), 'Keywords' in q.keys(), 'SearchIndex' in q.keys())):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        resp_amazon = amazon.ItemSearch(**q)
        root = objectify.fromstring(resp_amazon)
        serializer = self.get_serializer(root)
        return Response(serializer.data)


class ItemLookUp(generics.RetrieveAPIView):
    """
    Get item by `ASIN` Amazon.

    [Doc](https://docs.aws.amazon.com/AWSECommerceService/latest/DG/LocaleDE.html)

    [Scratchpad](https://webservices.amazon.com/scratchpad/index.html)

    [ResponseGroups](https://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_ResponseGroupsList.html):

    * Medium
    """
    serializer_class = ItemLookUpSerializer

    def get_object(self):
        item_id = self.kwargs['item_id']
        response_group = 'Medium, Offers'
        resp_amazon = amazon.ItemLookup(ItemId=item_id, ResponseGroup=response_group)
        root = objectify.fromstring(resp_amazon)
        return root
