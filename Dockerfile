FROM python:3.6-alpine

# Copy in your requirements file
ADD requirements.txt /requirements.txt

# Install build deps, then run `pip install`, then remove unneeded build deps all in a single step. Correct the path to your production requirements file, if needed.
RUN set -ex \
    && apk add --no-cache --virtual .build-deps \
            gcc \
            make \
            libc-dev \
            musl-dev \
            linux-headers \
            pcre-dev \
            postgresql-dev \
            libxml2-dev \
            libxslt-dev \
            libffi-dev \
            libgcc \
            curl \
            jpeg-dev \
            zlib-dev \
            freetype-dev \
            lcms2-dev \
            openjpeg-dev \
            tiff-dev \
            tk-dev \
            tcl-dev \
    && apk add --update --no-cache \
        libstdc++ \
        libx11 \
        glib \
        libxrender \
        libxext \
        libintl \
        libcrypto1.0 \
        libssl1.0 \
        ttf-dejavu \
        ttf-droid \
        ttf-freefont \
        ttf-liberation \
        ttf-ubuntu-font-family \
    && python3.6 -m venv /venv \
    && /venv/bin/pip install -U pip==18.1 \
    && LIBRARY_PATH=/lib:/usr/lib /bin/sh -c "/venv/bin/pip install --no-cache-dir -r /requirements.txt" \
    && runDeps="$( \
            scanelf --needed --nobanner --recursive /venv \
                    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                    | sort -u \
                    | xargs -r apk info --installed \
                    | sort -u \
    )" \
    && apk add --virtual .python-rundeps $runDeps \
    && apk del .build-deps \
    && mkdir /code/ \
    && mkdir /static/ \
    && mkdir /media_app/

VOLUME ["/static/", "/media_app/"]
WORKDIR /code/
COPY ./etc/wkhtmltopdf /usr/bin/
COPY . /code/

# uWSGI will listen on this port
EXPOSE 8000

ENV DATABASE_URL=none

ENV BROKER_URL=none

# Add any custom, static environment variables needed by Django or your settings file here:
ENV DJANGO_SETTINGS_MODULE=settings.staging

# uWSGI configuration (customize as needed):
ENV UWSGI_VIRTUALENV=/venv UWSGI_WSGI_FILE=/code/wsgi.py UWSGI_SOCKET=:8000 UWSGI_MASTER=1 UWSGI_WORKERS=4 UWSGI_THREADS=8

ENV UWSGI_LAZY_APPS=1 UWSGI_WSGI_ENV_BEHAVIOR=holy

#ENTRYPOINT ["/code/docker-entrypoint.sh"]
# Start uWSGI
#CMD ["/venv/bin/uwsgi"]
