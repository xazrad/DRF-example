import csv

import pytest

from django.core.management import call_command

from flexvoucher import celery_app


@pytest.fixture(scope='class')
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command('loaddata', 'groups.json')
        call_command('loaddata', 'user.json')
        call_command('loaddata', 'account.json')
        call_command('loaddata', 'voucher.json')
        call_command('loaddata', 'voucher_history.json')
        call_command('loaddata', 'invoice_data.json')
        call_command('loaddata', 'invoice_scan.json')
        call_command('loaddata', 'feedback.json')


@pytest.fixture
def example_csv(tmpdir):
    p = tmpdir.join("data.txt")
    try:
        myfile = open(p, 'w')
        fieldnames = ['email', 'first_name', 'last_name', 'points']
        writer = csv.DictWriter(myfile, fieldnames=fieldnames, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writeheader()
        writer.writerow({'email': 'example@example.ru',
                         'last_name': 'Spam',
                         'first_name': 'Hello',
                         'points': 0})
        writer.writerow({'email': 'example@example2.ru',
                         'last_name': 'Spam2',
                         'first_name': 'Hello2',
                         'points': 0})
    finally:
        myfile.close()

    with open(p, 'rb') as fp:
        yield fp


@pytest.fixture
def example_csv_incorrect(tmpdir):
    p = tmpdir.join("data.txt")
    try:
        myfile = open(p, 'w')
        myfile.write('ljkwghrephgkerjhgerkjghkerjqhg')
    finally:
        myfile.close()

    with open(p, 'rb') as fp:
        yield fp


@pytest.fixture
def example_scan():
    p = 'apps/flexvoucher/tests/photo_scan.jpg'
    with open(p, 'rb') as fp:
        yield fp


@pytest.fixture
def example_scan2():
    p = 'apps/flexvoucher/tests/photo_scan2.jpg'
    with open(p, 'rb') as fp:
        yield fp


@pytest.fixture(scope='module')
def app_celery():
    celery_app.conf.update(CELERY_ALWAYS_EAGER=True)
    # celery_app.conf.update(TESTING=True)
    return celery_app
